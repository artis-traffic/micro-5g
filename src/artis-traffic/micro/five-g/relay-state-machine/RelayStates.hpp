/**
 * @file artis-traffic/micro/five_g/relay-state-machine/RelayStates.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_FIVE_G_RELAY_STATES_HPP
#define ARTIS_TRAFFIC_FIVE_G_RELAY_STATES_HPP

#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/micro/five-g/Data.hpp>
#include <artis-traffic/micro/five-g/antenna-state-machine/AntennaStates.hpp>

#include <map>
#include <vector>

namespace artis::traffic::micro::five_g {

//struct RelayMessageState : public AntennaMessageState {
//    std::map<uint, std::pair<artis::traffic::core::Time, Cell> > _vehicles;
//    std::vector<MeasureReportMessage> _measure_messages;
//    std::deque<MeasureReportMessagePair> _send_measure_messages;
//    std::map<std::string, std::vector<uint> > _cells_vehicles;
//    double _timeout_duration;
//    uint _lost_messages_count;
//};
//
//struct RelayActiveState : public AntennaActiveState {
//    std::map<std::string, uint > _cells_active_vehicles;
//    std::vector<std::string> _active_cells_id;
//    std::vector<std::string> _inactive_cells_id;
//    std::map<std::string, uint > _cells_transmitting_vehicles;
//};
//
//struct RelayCAMState : public AntennaCAMState {
//    std::map<std::string, std::deque<CAMSignalMessagePair> > _signal_messages;
//    std::vector<CAMMessage> _cam_messages;
//    std::map<uint, uint> _cam_vehicle_count;
//};

}

#endif //ARTIS_TRAFFIC_FIVE_G_RELAY_STATES_HPP
