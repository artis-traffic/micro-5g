#ifndef ARTIS_TRAFFIC_MICRO_5G_DATA_HPP
#define ARTIS_TRAFFIC_MICRO_5G_DATA_HPP

#include <string>
#include <vector>

namespace artis::traffic::micro::five_g {

    struct Coordinates {
        double x;
        double y;

        bool operator==(const Coordinates &other) const {
            return x == other.x and y == other.y;
        }

        std::string to_string() const {
            return "[" + std::to_string(x) + "," + std::to_string(y) + "]";
        }
    };

    struct Cell {
        std::string cell_id;
        int frequency;
        int orientation;
        double rsrp;
        double rsrq;
        double sinr;

        bool operator==(const Cell &other) const {
            return cell_id == other.cell_id and orientation == other.orientation and
            frequency == other.frequency and rsrp == other.rsrp
            and rsrq == other.rsrq and sinr == other.sinr;
        }

        Cell& operator=(const Cell &other) = default;

        bool operator <(const Cell& c1) const
        {
            return cell_id < c1.cell_id;
        }

        std::string to_string() const {
            std::string str;
            if (frequency == 26000) {
                str += cell_id + "," + std::to_string(frequency) +
                       "," + std::to_string(rsrp) + "," + std::to_string(rsrq)
                       + "," + std::to_string(sinr);
            }
            else {
                str += cell_id + "," + std::to_string(frequency) + "," + std::to_string(orientation) +
                       "," + std::to_string(rsrp) + "," + std::to_string(rsrq)
                       + "," + std::to_string(sinr);
            }
            return "cell<" + str + ">";
        }

        int message_size() const {
          return 128;
      }
    };


    struct PositionData {
        uint vehicle_index;
        Coordinates coordinates;
        bool is_event;

        bool operator==(const PositionData &other) const {
            return vehicle_index == other.vehicle_index and coordinates == other.coordinates
            and is_event == other.is_event;
        }

        std::string to_string() const {
            return "message_data<" + std::to_string(vehicle_index) + " , " +
            coordinates.to_string() + " , " + std::to_string(is_event);
        }
    };

    struct Data {
        std::string measure_id;
        uint vehicle_index;
        Coordinates position;
        Cell serving_cell;

        bool operator==(const Data &other) const {
            return vehicle_index == other.vehicle_index and position == other.position
                   and serving_cell == other.serving_cell;
        }

        std::string to_string() const {
            std::string str = "data<" + measure_id + " , " + std::to_string(vehicle_index)
                              + " , " + position.to_string() + " , serving_cell<" + serving_cell.to_string() + ">, neighbor_cells:[";
            str += ">";
            return str;
        }

        int message_size() const {
            return 0;
        }
    };

//    struct MeasureReportData : Data {
//        std::vector<Cell> neighbor_cells;
//
//        bool operator==(const MeasureReportData &other) const {
//            return vehicle_index == other.vehicle_index and position == other.position
//            and serving_cell == other.serving_cell and neighbor_cells == other.neighbor_cells;
//        }
//
//        std::string to_string() const {
//            std::string str = "measure_report_data<" + measure_id + " , " + std::to_string(vehicle_index)
//                   + " , " + position.to_string() + " , serving_cell<" + serving_cell.to_string() + ">, neighbor_cells:[";
//            for (uint i = 0; i < neighbor_cells.size(); i++) {
//                if (i < neighbor_cells.size() - 1) {
//                    str += neighbor_cells[i].to_string() + ",";
//                }
//                else {
//                    str += neighbor_cells[i].to_string() + "]";
//                }
//            }
//            str += ">";
//            return str;
//        }
//
//        int message_size() const {
//            int message_size = 32 + serving_cell.message_size();
//            for (uint i = 0; i < neighbor_cells.size(); i++) {
//                message_size += neighbor_cells[i].message_size();
//            }
//            return message_size;
//        }
//    };

//    struct CAMData : Data {
//
//        bool operator==(const MeasureReportData &other) const {
//            return vehicle_index == other.vehicle_index and position == other.position
//                   and serving_cell == other.serving_cell;
//        }
//
//        std::string to_string() const {
//            std::string str = "cam_data<" + measure_id + " , " + std::to_string(vehicle_index)
//                              + " , " + position.to_string() + " , serving_cell<" + serving_cell.to_string();
//            str += ">";
//            return str;
//        }
//
//        bool operator==(const CAMData &other) const {
//            return vehicle_index == other.vehicle_index and position == other.position
//                   and serving_cell == other.serving_cell;
//        }
//
//        int message_size() const {
//            int message_size = 8 + 8 + 16 + 16 + 8 + 8 + 8 + 32 + 16 + 32 + 16 + 72 + 16;
//            return message_size;
//        }
//    };

//    struct DENMData : Data {
//        bool entering_vehicle;
//
//        std::string to_string() const {
//            std::string str = "denm_data<" + measure_id + " , " + std::to_string(vehicle_index)
//                              + " , " + position.to_string() + " , serving_cell<" + serving_cell.to_string();
//            str += ">";
//            return str;
//        }
//
//        bool operator==(const DENMData &other) const {
//            return vehicle_index == other.vehicle_index and position == other.position
//                   and serving_cell == other.serving_cell and entering_vehicle == other.entering_vehicle;
//        }
//
//        int message_size() const {
//            int message_size = 8 + 8 + 16 + 16 + 16 + 8 + 16 + 8 + 8 + 8 + 16 + 16 + 32 + 32 + 16 + 32 + 16
//                    + 72 + 8 + 32 + 16 + 32 + 16 + 72 + 5880;
//            return message_size;
//        }
//    };

    struct Message {
        std::string message_id;
        uint in_port_index;
        artis::traffic::core::Time next_time;
        bool cell_change;
        bool is_event;
        double delay;
        double debit;
        double distance;
        uint vehicle_message_count;
        double end_delay;
        Data data;

        Message() = default;

//        Message(std::string message_id,
//        uint in_port_index, artis::traffic::core::Time next_time, bool cell_change, bool is_event,
//        double delay, double debit, double distance, uint vehicle_message_count, double end_delay,
//        Data data) {
//            this->message_id = message_id;
//            this->in_port_index = in_port_index;
//            this->next_time = next_time;
//            this->cell_change = cell_change;
//            this->is_event = is_event;
//            this->delay = delay;
//            this->debit = debit;
//            this->distance = distance;
//            this->vehicle_message_count = vehicle_message_count;
//            this->end_delay = end_delay;
//            this->data = data;
//        }
//
//        Message(const Message& message) {
//            message_id = message.message_id;
//            in_port_index = message.in_port_index;
//            next_time = message.next_time;
//            cell_change = message.cell_change;
//            is_event = message.is_event;
//            delay = message.delay;
//            debit = message.debit;
//            distance = message.distance;
//            vehicle_message_count = message.vehicle_message_count;
//            end_delay = message.end_delay;
//            data = message.data;
//        }

        bool operator==(const Message &other) const {
            return in_port_index == other.in_port_index and next_time == other.next_time
                   and cell_change == other.cell_change and is_event == other.is_event and data == other.data;
        }

        std::string to_string() const {
            return "message<" + message_id + " " + std::to_string(in_port_index) + " , "
                    + std::to_string(next_time) + ">";
        }

        int message_size() const { return 0; };
    };

    struct MeasureReportMessage : Message {
        //MeasureReportData data;
        std::vector<Cell> neighbor_cells;

        bool operator==(const MeasureReportMessage &other) const {
            return in_port_index == other.in_port_index and data == other.data and next_time == other.next_time
                   and cell_change == other.cell_change and is_event == other.is_event and neighbor_cells == other.neighbor_cells;
        }

        std::string to_string() const {
            return "message<" + message_id + " " + std::to_string(in_port_index) + " , "
                   + data.serving_cell.cell_id + " , " + std::to_string(next_time) + ">";
        }

        int message_size() const {
            int message_size = 32 + data.serving_cell.message_size();
            for (uint i = 0; i < neighbor_cells.size(); i++) {
                message_size += neighbor_cells[i].message_size();
            }
            return 32 + message_size;
        }
    };

    struct CAMSignalMessage : Message {
        //CAMData data;

        bool operator==(const CAMSignalMessage &other) const {
            return in_port_index == other.in_port_index and data == other.data and next_time == other.next_time
                   and cell_change == other.cell_change and is_event == other.is_event;
        }

        std::string to_string() const {
            return "message<" + message_id + " " + std::to_string(in_port_index) + " , "
                   + data.serving_cell.cell_id + " , " + std::to_string(next_time) + ">";
        }

        int message_size() const {
            return 8 + 8 + 16 + 16 + 8 + 8 + 8 + 32 + 16 + 32 + 16 + 72 + 16;
        }
    };

    struct CAMMessage : Message {
        //CAMData data;
        uint vehicle_count;

        bool operator==(const CAMMessage &other) const {
            return in_port_index == other.in_port_index and data == other.data and next_time == other.next_time
                   and cell_change == other.cell_change and is_event == other.is_event and vehicle_count == other.vehicle_count;
        }

        std::string to_string() const {
            return "message<" + message_id + " " + std::to_string(in_port_index) + " , "
                   + data.serving_cell.cell_id + " , " + std::to_string(next_time) + ">";
        }

        int message_size() const {
            return 8 + 8 + 16 + 16 + 8 + 8 + 8 + 32 + 16 + 32 + 16 + 72 + 16;
        }

        CAMMessage() = default;

        CAMMessage(const CAMSignalMessage& signal_message) {
            message_id = signal_message.message_id;
            in_port_index = signal_message.in_port_index;
            next_time = signal_message.next_time;
            cell_change = signal_message.cell_change;
            is_event = signal_message.is_event;
            delay = signal_message.delay;
            debit = signal_message.debit;
            distance = signal_message.distance;
            vehicle_message_count = signal_message.vehicle_message_count;
            end_delay = signal_message.end_delay;
            vehicle_count = 0;
            data = signal_message.data;
        }

        CAMMessage(const CAMMessage& signal_message) {
            message_id = signal_message.message_id;
            in_port_index = signal_message.in_port_index;
            next_time = signal_message.next_time;
            cell_change = signal_message.cell_change;
            is_event = signal_message.is_event;
            delay = signal_message.delay;
            debit = signal_message.debit;
            distance = signal_message.distance;
            vehicle_message_count = signal_message.vehicle_message_count;
            end_delay = signal_message.end_delay;
            vehicle_count = signal_message.vehicle_count;
            data = signal_message.data;
        }
    };

    struct DENMSignalMessage : Message {
        //DENMData data;
        bool entering_vehicle;

        bool operator==(const DENMSignalMessage &other) const {
            return in_port_index == other.in_port_index and data == other.data and next_time == other.next_time
                   and cell_change == other.cell_change and is_event == other.is_event and
                   entering_vehicle == other.entering_vehicle;
        }

        std::string to_string() const {
            return "message<" + message_id + " " + std::to_string(in_port_index) + " , "
                   + data.serving_cell.cell_id + " , " + std::to_string(next_time) + ">";
        }

        int message_size() const {
            int message_size = 8 + 8 + 16 + 16 + 16 + 8 + 16 + 8 + 8 + 8 + 16 + 16 + 32 + 32 + 16 + 32 + 16
                               + 72 + 8 + 32 + 16 + 32 + 16 + 72 + 5880;
            return message_size;
        }
    };
    
    struct DENMMessage : Message {
        //DENMData data;
        bool entering_vehicle;
        uint vehicle_count;

        bool operator==(const DENMMessage &other) const {
            return in_port_index == other.in_port_index and data == other.data and next_time == other.next_time
                   and cell_change == other.cell_change and is_event == other.is_event and
                   entering_vehicle == other.entering_vehicle and vehicle_count == other.vehicle_count;
        }

        std::string to_string() const {
            return "message<" + message_id + " " + std::to_string(in_port_index) + " , "
                   + data.serving_cell.cell_id + " , " + std::to_string(next_time) + ">";
        }

        int message_size() const {
            int message_size = 8 + 8 + 16 + 16 + 16 + 8 + 16 + 8 + 8 + 8 + 16 + 16 + 32 + 32 + 16 + 32 + 16
                               + 72 + 8 + 32 + 16 + 32 + 16 + 72 + 5880;
            return message_size;
        }

        DENMMessage () = default;

        DENMMessage(const DENMSignalMessage& signal_message) {
            message_id = signal_message.message_id;
            in_port_index = signal_message.in_port_index;
            next_time = signal_message.next_time;
            cell_change = signal_message.cell_change;
            is_event = signal_message.is_event;
            delay = signal_message.delay;
            debit = signal_message.debit;
            distance = signal_message.distance;
            vehicle_message_count = signal_message.vehicle_message_count;
            data.vehicle_index = signal_message.data.vehicle_index;
            data.serving_cell = signal_message.data.serving_cell;
            data.position = signal_message.data.position;
            entering_vehicle = signal_message.entering_vehicle;
            vehicle_count = 0;
        }

        DENMMessage(std::string message_id, uint in_port_index, artis::traffic::core::Time next_time,
        bool cell_change, bool is_event, double delay, double debit, double distance,
        uint vehicle_message_count, double end_delay, Data data, bool entering_vehicle, uint vehicle_count) {
            this->message_id = message_id;
            this->in_port_index = in_port_index;
            this->next_time = next_time;
            this->cell_change = cell_change;
            this->is_event = is_event;
            this->delay = delay;
            this->debit = debit;
            this->distance = distance;
            this->vehicle_message_count = vehicle_message_count;
            this->data.vehicle_index = data.vehicle_index;
            this->data.serving_cell = data.serving_cell;
            this->data.position = data.position;
            this->data = data;
            this->entering_vehicle = entering_vehicle;
            this->vehicle_count = vehicle_count;
        }
    };

    struct MeasureReportMessagePair {
        MeasureReportMessage message;
        uint port_index;

        bool operator==(const MeasureReportMessagePair &other) const {
            return message == other.message and port_index == other.port_index;
        }

        std::string to_string() const {
            return "message<" + message.message_id + " " + std::to_string(port_index) + ">";
        }
    };

    struct CAMSignalMessagePair {
        CAMSignalMessage message;
        uint port_index;

        bool operator==(const CAMSignalMessagePair &other) const {
            return message == other.message and port_index == other.port_index;
        }

        std::string to_string() const {
            return "message<" + message.message_id + " " + std::to_string(port_index) + ">";
        }
    };

    struct CAMMessagePair {
        CAMMessage message;
        uint port_index;

        bool operator==(const CAMMessagePair &other) const {
            return message == other.message and port_index == other.port_index;
        }

        std::string to_string() const {
            return "message<" + message.message_id + " " + std::to_string(port_index) + ">";
        }
    };

    struct DENMMessagePair {
        DENMMessage message;
        uint port_index;

        bool operator==(const DENMMessagePair &other) const {
            return message == other.message and port_index == other.port_index;
        }

        std::string to_string() const {
            return "message<" + message.message_id + " " + std::to_string(port_index) + ">";
        }
    };

    struct DENMSignalMessagePair {
        DENMSignalMessage message;
        uint port_index;

        bool operator==(const DENMSignalMessagePair &other) const {
            return message == other.message and port_index == other.port_index;
        }

        std::string to_string() const {
            return "message<" + message.message_id + " " + std::to_string(port_index) + ">";
        }
    };
}

#endif //ARTIS_TRAFFIC_MICRO_5G_DATA_HPP
