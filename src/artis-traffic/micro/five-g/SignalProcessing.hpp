/**
 * @file artis-traffic/micro/five-g/signalprocessing.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_SIGNALPROCESSING_HPP
#define ARTIS_TRAFFIC_MICRO_5G_SIGNALPROCESSING_HPP

#include <artis-star-addons/pdevs/StateMachineDynamics.hpp>
#include <artis-star/kernel/pdevs/Dynamics.hpp>
#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/micro/five-g/Data.hpp>

#include <artis-traffic/micro/five-g/signal_processing-state-machine/SignalProcessingIDs.hpp>
#include <artis-traffic/micro/five-g/signal_processing-state-machine/SignalProcessingTypes.hpp>
#include <artis-traffic/micro/five-g/signal_processing-state-machine/SignalProcessingRootStateMachine.hpp>
#include <artis-traffic/micro/five-g/signal_processing-state-machine/SignalProcessingStateMachine.hpp>

#include <vector>
#include <map>

namespace artis::traffic::micro::five_g {

    struct SignalProcessingParameters {
        std::vector<Coordinates> coordinates;
        std::vector<std::vector<Cell>> cells;
    };

    template<class Types, class Parameters>
    class SignalProcessing
            : public artis::addons::pdevs::StateMachineDynamics<artis::common::DoubleTime, SignalProcessing<Types, Parameters>, SignalProcessingParameters,
                    typename Types::root_state_machine_type> {
    public:
        struct inputs {
            enum values {
                IN, ACTIVE = IN + 500, INACTIVE = ACTIVE + 500,
                CAM_SIGNAL = INACTIVE + 500, CAM_MESSAGE = CAM_SIGNAL + 500, 
                DENM_SIGNAL = CAM_MESSAGE + 500, DENM_MESSAGE = DENM_SIGNAL + 500,
                VEHICLE_LIST = DENM_MESSAGE + 500, DENM_BROADCAST = VEHICLE_LIST + 500
            };
        };

        struct outputs {
            enum values {
                OUT, ACTIVE = OUT + 500, INACTIVE = ACTIVE + 500, LINK_TRANSMITTING = INACTIVE + 500,
                CAM_SIGNAL = LINK_TRANSMITTING + 500, CAM_MESSAGE = CAM_SIGNAL + 500,
                DENM_SIGNAL = CAM_MESSAGE + 500, DENM_MESSAGE = DENM_SIGNAL + 500,
                VEHICLE_LIST = DENM_MESSAGE + 500, DENM_BROADCAST = VEHICLE_LIST + 500
            };
        };

        struct vars {
            enum values {
                VEHICLE_INDEXES, VEHICLE_POSITIONS, TRANSMITTING_VEHICLES, AVERAGE_DENM_DELAYS, AVERAGE_DENM_DEBITS,
                AVERAGE_DENM_DISTANCE, CELLS_DENM_COUNT, AVERAGE_CAM_DELAYS, AVERAGE_CAM_DEBITS,
                AVERAGE_CAM_DISTANCE, CELLS_CAM_COUNT, SERVING_CELL_CHANGES, DENM_VEHICLE_COUNT
            };
        };

        SignalProcessing(const std::string &name,
                         const artis::pdevs::Context<common::DoubleTime, SignalProcessing<Types, Parameters>, SignalProcessingParameters> &context)
                : artis::addons::pdevs::StateMachineDynamics<artis::common::DoubleTime, SignalProcessing<Types, Parameters>, Parameters,
                typename Types::root_state_machine_type>(name, context),
                  _coordinates(context.parameters().coordinates), _cells(context.parameters().cells) {
            for (unsigned int i = 0; i < _coordinates.size() + 1; ++i) {
                std::stringstream ss_in;
                ss_in << "in_" << (i + 1);
                this->input_port({inputs::IN + i, ss_in.str()});

                std::stringstream ss_out;
                ss_out << "out_" << (i + 1);
                this->output_port({outputs::OUT + i, ss_out.str()});

                std::stringstream ss_act;
                ss_act << "active_" << (i + 1);
                this->output_port({outputs::ACTIVE + i, ss_act.str()});

                std::stringstream ss_in_act;
                ss_in_act << "active_" << (i + 1);
                this->input_port({inputs::ACTIVE + i, ss_in_act.str()});

                std::stringstream ss_iact;
                ss_iact << "active_" << (i + 1);
                this->output_port({outputs::INACTIVE + i, ss_iact.str()});

                std::stringstream ss_in_iact;
                ss_in_iact << "active_" << (i + 1);
                this->input_port({inputs::INACTIVE + i, ss_in_iact.str()});

                std::stringstream ss_ilt;
                ss_iact << "link_transmitting_" << (i + 1);
                this->output_port({outputs::LINK_TRANSMITTING + i, ss_ilt.str()});

                std::stringstream ss_icsm;
                ss_icsm << "cam_signal_" << (i + 1);
                this->input_port({inputs::CAM_SIGNAL + i, ss_icsm.str()});
                std::stringstream ss_ocsm;
                ss_ocsm << "cam_signal_" << (i + 1);
                this->output_port({outputs::CAM_SIGNAL + i, ss_ocsm.str()});

                std::stringstream ss_icm;
                ss_icm << "cam_message_" << (i + 1);
                this->input_port({inputs::CAM_MESSAGE + i, ss_icm.str()});
                std::stringstream ss_ocm;
                ss_ocm << "cam_message_" << (i + 1);
                this->output_port({outputs::CAM_MESSAGE + i, ss_ocm.str()});

                std::stringstream ss_idsm;
                ss_idsm << "denm_signal_" << (i + 1);
                this->input_port({inputs::DENM_SIGNAL + i, ss_idsm.str()});
                std::stringstream ss_odsm;
                ss_odsm << "denm_signal_" << (i + 1);
                this->output_port({outputs::DENM_SIGNAL + i, ss_odsm.str()});
                
                std::stringstream ss_idenm;
                ss_idenm << "denm_message_" << (i + 1);
                this->input_port({inputs::DENM_MESSAGE + i, ss_idenm.str()});

                std::stringstream ss_odenm;
                ss_odenm << "denm_message_" << (i + 1);
                this->output_port({outputs::DENM_MESSAGE + i, ss_odenm.str()});

                std::stringstream ss_idenmbc;
                ss_idenmbc << "denm_broadcast_" << (i + 1);
                this->input_port({inputs::DENM_BROADCAST + i, ss_idenmbc.str()});

                std::stringstream ss_odenmbc;
                ss_odenmbc << "denm_broadcast_" << (i + 1);
                this->output_port({outputs::DENM_BROADCAST + i, ss_odenmbc.str()});

            }
            this->input_port({inputs::VEHICLE_LIST, "vehicle_list"});
            this->output_port({outputs::VEHICLE_LIST, "vehicle_list"});

            this->observables({{vars::VEHICLE_INDEXES,   "vehicle_indexes"},
                               {vars::VEHICLE_POSITIONS, "vehicle_positions"},
                               {vars::TRANSMITTING_VEHICLES, "transmitting_vehicles"},
                               {vars::AVERAGE_DENM_DELAYS, "average_denm_delays"},
                               {vars::AVERAGE_DENM_DEBITS, "average_denm_debits"},
                               {vars::AVERAGE_DENM_DISTANCE, "average_denm_distance"},
                               {vars::CELLS_DENM_COUNT, "cells_denm_count"},
                               {vars::AVERAGE_CAM_DELAYS, "average_cam_delays"},
                               {vars::AVERAGE_CAM_DEBITS, "average_cam_debits"},
                               {vars::AVERAGE_CAM_DISTANCE, "average_cam_distance"},
                               {vars::CELLS_CAM_COUNT, "cells_cam_count"},
                               {vars::SERVING_CELL_CHANGES, "serving_cell_changes"},
                               {vars::DENM_VEHICLE_COUNT, "denm_vehicle_count"}});
        }

        ~SignalProcessing() override = default;

        void dext(const artis::traffic::core::Time &t,
                  const artis::traffic::core::Time & /* e */,
                  const artis::traffic::core::Bag &bag) override {
            std::for_each(bag.begin(), bag.end(),
                          [this, t](const artis::traffic::core::ExternalEvent &event) {
                              for (unsigned int i = 0; i < _coordinates.size() + 1; i++) {
                                  if (event.on_port(inputs::IN + i)) {
                                      MeasureReportMessage message{};

                                      event.data()(message);

                                      message.in_port_index = i;

//                                      std::cout<<this->get_full_name() << " SIGNAL IN " << i << " vehicle " << message.data.vehicle_index << std::endl;
                                      this->root().transition(t, SignalProcessingStateMachineIDs::TRANSMIT,
                                                              typename Types::state_machine_type::ExternalEvent{
                                                                      Types::event_IDs_type::NEW_MESSAGE,
                                                                      message});
                                  }
                                  if (event.on_port(inputs::ACTIVE + i)) {
                                      std::vector<std::string> active_cells_id{};

                                      event.data()(active_cells_id);

//                                      std::cout<<" dext => SIGNAL " << this->get_full_name()<<"=> t: "<<t<<" "<<cell_id<<std::endl;
                                      this->root().transition(t, SignalProcessingStateMachineIDs::ACTIVE,
                                                              typename Types::state_machine_type::ExternalEvent{
                                                                      Types::event_IDs_type::NEW_ACTIVE,
                                                                      active_cells_id});
                                  }
                                  if (event.on_port(inputs::INACTIVE + i)) {
                                      std::vector<std::string> active_cells_id{};

                                      event.data()(active_cells_id);

//                                      std::cout<<" dext => SIGNAL " << this->get_full_name()<<"=> t: "<<t<<" "<<cell_id<<std::endl;
                                      this->root().transition(t, SignalProcessingStateMachineIDs::ACTIVE,
                                                              typename Types::state_machine_type::ExternalEvent{
                                                                      Types::event_IDs_type::NEW_INACTIVE,
                                                                      active_cells_id});
                                  }
                                  if (event.on_port(inputs::CAM_SIGNAL + i)) {
                                      CAMSignalMessage message{};

                                      event.data()(message);

                                      message.in_port_index = i;

//                                      std::cout<<this->get_full_name() << " SP INCAMSIGNAL " << i << " vehicle " << message.data.vehicle_index << std::endl;
                                      this->root().transition(t, SignalProcessingStateMachineIDs::TRANSMIT,
                                                              typename Types::state_machine_type::ExternalEvent{
                                                                      Types::event_IDs_type::NEW_CAM_SIGNAL,
                                                                      message});
                                  }
                                  if (event.on_port(inputs::CAM_MESSAGE + i)) {
                                      CAMMessage message{};

                                      event.data()(message);

                                      message.in_port_index = i;

//                                      std::cout<<this->get_full_name() << " SP INCAMMESSAGE " << i << " vehicle " << message.data.vehicle_index << std::endl;
                                      if (i == 0) {
                                          this->root().transition(t, SignalProcessingStateMachineIDs::TRANSMIT,
                                                                  typename Types::state_machine_type::ExternalEvent{
                                                                          Types::event_IDs_type::NEW_CAM_MESSAGE_UPLINK,
                                                                          message});
                                      }
                                      else {
                                          this->root().transition(t, SignalProcessingStateMachineIDs::TRANSMIT,
                                                                  typename Types::state_machine_type::ExternalEvent{
                                                                          Types::event_IDs_type::NEW_CAM_MESSAGE_DOWNLINK,
                                                                          message});
                                      }
                                  }
                                  if (event.on_port(inputs::DENM_SIGNAL + i)) {
                                      DENMSignalMessage message{};

                                      event.data()(message);

                                      message.in_port_index = i;

//                                      std::cout<<this->get_full_name() << " SP INDENMSIGNAL " << i << " vehicle " << message.data.vehicle_index << std::endl;
                                      this->root().transition(t, SignalProcessingStateMachineIDs::TRANSMIT,
                                                              typename Types::state_machine_type::ExternalEvent{
                                                                      Types::event_IDs_type::NEW_DENM_SIGNAL,
                                                                      message});
                                  }
                                  if (event.on_port(inputs::DENM_MESSAGE + i)) {
                                      DENMMessage message{};

                                      event.data()(message);

                                      message.in_port_index = i;

//                                      std::cout<<this->get_full_name() << " MESSAGE DENM IN " << i << " vehicle " << message.data.vehicle_index << std::endl;
                                    if (i == 0) {
                                        this->root().transition(t, SignalProcessingStateMachineIDs::TRANSMIT,
                                                                typename Types::state_machine_type::ExternalEvent{
                                                                        Types::event_IDs_type::NEW_DENM_MESSAGE_UPLINK,
                                                                        message});
                                    }
                                    else {
                                        this->root().transition(t, SignalProcessingStateMachineIDs::TRANSMIT,
                                                                typename Types::state_machine_type::ExternalEvent{
                                                                        Types::event_IDs_type::NEW_DENM_MESSAGE_DOWNLINK,
                                                                        message});
                                    }
                                  }
                                  if (event.on_port(inputs::VEHICLE_LIST + i)) {
                                      std::vector<typename Types::vehicle_type> vehicle_list{};

                                      event.data()(vehicle_list);

//                                      std::cout<<this->get_full_name() << " SIGNAL VLIST IN " << std::endl;
                                      this->root().transition(t, SignalProcessingStateMachineIDs::TRANSMIT,
                                                              typename Types::state_machine_type::ExternalEvent{
                                                                      Types::event_IDs_type::GET_VEHICLE_LIST,
                                                                      vehicle_list});
                                  }
                                  if (event.on_port(inputs::DENM_BROADCAST + i)) {
                                      DENMMessage message{};

                                      event.data()(message);

                                      message.in_port_index = i;

//                                      std::cout<<this->get_full_name() << " SP INDENMBROADCAST " << i << " vehicle " << message.data.vehicle_index << std::endl;
                                      this->root().transition(t, SignalProcessingStateMachineIDs::TRANSMIT,
                                                              typename Types::state_machine_type::ExternalEvent{
                                                                      Types::event_IDs_type::BROADCAST_DENM_MESSAGE_DOWNLINK,
                                                                      message});
                                  }
                              }
                          });
        }

        artis::traffic::core::Bag lambda(const artis::traffic::core::Time & t) const override {
            artis::traffic::core::Bag bag;
            const typename Types::root_state_machine_type::external_events_type &events = this->root().outbox();

            std::for_each(events.cbegin(), events.cend(), [t, this, &bag](const auto &e) {
                switch (e.id) {
                    case SignalProcessingEventIDs::TRANSMISSION: {
                        MeasureReportMessage message;
                        e.data(message);

//                        unsigned int out_index = message.in_port_index == 0 ? 1 : 0;


//                        if (message.in_port_index != 0) {
//                            std::cout << " => SIGNAL OUT => " << message.in_port_index << " t: " << t << " "
//                                      << message.message_id << " cell" << message.data.serving_cell.cell_id
//                                      << std::endl;
//                        }
                        bag.push_back(
                                artis::traffic::core::ExternalEvent(outputs::OUT + message.in_port_index, message));
                        break;
                    }
                    case SignalProcessingEventIDs::RECEIVING: {
                        DENMMessage message;
                        e.data(message);

//                        unsigned int out_index = message.in_port_index == 0 ? 1 : 0;


//                        std::cout << " => SIGNAL OUT => " << message.in_port_index << " t: " << t << " " << message.data.measure_id << std::endl;

//                        std::cout<<" lambda => SIGNAL RECEIVING => t: " << t << " " << message.data.serving_cell.cell_id << std::endl;
                        bag.push_back(
                                artis::traffic::core::ExternalEvent(outputs::ACTIVE, message));
                        break;
                    }
                    case SignalProcessingEventIDs::NOT_RECEIVING: {
                        DENMMessage message;
                        e.data(message);

//                        unsigned int out_index = message.in_port_index == 0 ? 1 : 0;


//                        std::cout << " => SIGNAL OUT => " << message.in_port_index << " t: " << t << " " << message.data.measure_id << std::endl;

//                        std::cout<<" lambda => SIGNAL NOT_RECEIVING => t: " << t << " " << message.data.serving_cell.cell_id << std::endl;
                        // TODO: adapt for relays
                        bag.push_back(
                                artis::traffic::core::ExternalEvent(outputs::INACTIVE, message));
                        break;
                    }
                    case SignalProcessingEventIDs::TRANSMITTING: {
                        CAMSignalMessage message;
                        e.data(message);

//                        unsigned int out_index = message.in_port_index == 0 ? 1 : 0;


//                        std::cout << " => SIGNAL OUT => " << message.in_port_index << " t: " << t << " " << message.data.measure_id << std::endl;

//                        std::cout<<" lambda => SIGNAL RECEIVING => t: " << t << " " << message.data.serving_cell.cell_id << std::endl;
                        bag.push_back(
                                artis::traffic::core::ExternalEvent(outputs::LINK_TRANSMITTING, message));
                        break;
                    }
                    case SignalProcessingEventIDs::SEND_CAM_SIGNAL: {
                        CAMSignalMessage message;
                        e.data(message);

//                        std::cout << " => SP SEND CAM SIGNAL => " << message.in_port_index << " t: " << t << " " << message.data.measure_id << " vid "<< message.data.vehicle_index << std::endl;

                        bag.push_back(
                                artis::traffic::core::ExternalEvent(outputs::CAM_SIGNAL + message.in_port_index, message));
                        break;
                    }
                    case SignalProcessingEventIDs::SEND_CAM_MESSAGE: {
                        CAMMessage message;
                        e.data(message);

//                        std::cout << " => SP SEND CAM MESSAGE => " << message.in_port_index << " t: " << t << " " << message.data.measure_id << " vid "<< message.data.vehicle_index << std::endl;

                        bag.push_back(
                                artis::traffic::core::ExternalEvent(outputs::CAM_MESSAGE + message.in_port_index, message));
                        break;
                    }
                    case SignalProcessingEventIDs::SEND_DENM_SIGNAL: {
                        DENMSignalMessage message;
                        e.data(message);

//                        std::cout << " => SP SEND DENM SIGNAL => " << message.in_port_index << " t: " << t << " " << message.data.measure_id << " vid "<< message.data.vehicle_index << std::endl;

                        bag.push_back(
                                artis::traffic::core::ExternalEvent(outputs::DENM_SIGNAL + message.in_port_index, message));
                        break;
                    }
                    case SignalProcessingEventIDs::DENM_TRANSMISSION: {
                        DENMMessage message;
                        e.data(message);

//                        unsigned int out_index = message.in_port_index == 0 ? 1 : 0;


//                        if (message.in_port_index != 0) {
//                            std::cout << " => SIGNAL OUT => " << message.in_port_index << " t: " << t << " "
//                                      << message.message_id << " cell" << message.data.serving_cell.cell_id
//                                      << std::endl;
//                        }
                        bag.push_back(
                                artis::traffic::core::ExternalEvent(outputs::DENM_MESSAGE + message.in_port_index, message));
                        break;
                    }
                    case SignalProcessingEventIDs::ASK_VEHICLE_LIST: {

//                        std::cout << this->get_full_name() << " => SP ASK VLIST => t: " << t << std::endl;

                        bag.push_back(
                                artis::traffic::core::ExternalEvent(outputs::VEHICLE_LIST));
                        break;
                    }
                    case SignalProcessingEventIDs::BROADCAST_DENM_MESSAGE_RELAY: {
                        DENMMessage message;
                        e.data(message);

//                        std::cout << " => SP SEND DENM BROADCAST => " << message.in_port_index << " t: " << t << " " << message.data.measure_id << " vid "<< message.data.vehicle_index << std::endl;

                        if (_coordinates.size() > 2) {
                            for (unsigned int i = 2; i < _coordinates.size(); i++) {
                                if (i != message.in_port_index) {
                                    bag.push_back(
                                            artis::traffic::core::ExternalEvent(outputs::DENM_BROADCAST + i,
                                                                                message));
                                }
                            }
                        }
                        else {
                            bag.push_back(
                                    artis::traffic::core::ExternalEvent(outputs::DENM_BROADCAST + 1,
                                                                        message));
                        }
                        break;
                    }
                    default: {
                    }
                }
            });
            return bag;
        }

        artis::common::event::Value observe(const artis::traffic::core::Time & t,
                                            unsigned int index) const override {
            const auto &state = this->root().template state_<TransmitState<Types>>(Types::state_machine_IDs_type::TRANSMIT);
            switch (index) {
                case vars::TRANSMITTING_VEHICLES: {
                    int count = 0;
                    if (not state._measure_messages.empty()) {
                        count = state._measure_messages.size();
                    }
                    return count;
                }
                case vars::AVERAGE_DENM_DELAYS: {
                    std::map<std::string, std::pair<double, int>> delays;
                    std::vector<DENMMessage> messages = state._sent_denm_messages;

                    for (const auto &cell: _cells[0]) {
                        std::string cell_id = cell.frequency + "/" + cell.orientation;
                        delays[cell_id] = std::make_pair(0, 0);
                    }
                    for (const auto &measure: messages) {
                        std::string cell_id = measure.data.serving_cell.frequency + "/" + measure.data.serving_cell.orientation;
                        std::pair<double, int> &p = delays[cell_id];

                        p.first += measure.delay;
                        p.second++;
                    }

                    std::vector<double> dbl_delays;

                    for (auto &delay: delays) {
                        dbl_delays.push_back(delay.second.first != 0 ? delay.second.first / delay.second.second : 0);
                    }
                    return dbl_delays;
                }
                case vars::AVERAGE_DENM_DEBITS: {
                    // antenna by cell code TODO
                    std::map<std::string, std::pair<double, int>> debits;
                    std::vector<DENMMessage> messages = state._sent_denm_messages;

                    for (const auto &cell: _cells[0]) {
                        std::string cell_id = cell.frequency + "/" + cell.orientation;
                        debits[cell_id] = std::make_pair(0, 0);
                    }
                    for (const auto &measure: messages) {
                        std::string cell_id = measure.data.serving_cell.frequency + "/" + measure.data.serving_cell.orientation;
                        std::pair<double, int> &p = debits[cell_id];

                        p.first += measure.debit;
                        p.second++;
                    }

                    std::vector<double> dbl_debits;

                    for (auto &debit: debits) {
                        dbl_debits.push_back(debit.second.first != 0 ? debit.second.first / debit.second.second : 0);
                    }
                    return dbl_debits;
                }
                case vars::AVERAGE_DENM_DISTANCE: {
                    std::map<std::string, std::pair<double, int>> distances;
                    std::vector<DENMMessage> messages = state._sent_denm_messages;

                    for (const auto &cell: _cells[0]) {
                        std::string cell_id = cell.frequency + "/" + cell.orientation;
                        distances[cell_id] = std::make_pair(0, 0);
                    }
                    for (const auto &measure: messages) {
                        std::string cell_id = measure.data.serving_cell.frequency + "/" + measure.data.serving_cell.orientation;
                        std::pair<double, int> &p = distances[cell_id];

                        p.first += measure.distance;
                        p.second++;
                    }

                    std::vector<double> dbl_distances;

                    for (auto &distance: distances) {
                        dbl_distances.push_back(distance.second.first != 0 ? distance.second.first / distance.second.second : 0);
                    }
                    return dbl_distances;
                }
                case vars::CELLS_DENM_COUNT: {
                    std::map<std::string, std::pair<double, int>> counts;
                    std::vector<DENMMessage> messages = state._sent_denm_messages;

                    for (const auto &cell: _cells[0]) {
                        std::string cell_id = cell.frequency + "/" + cell.orientation;
                        counts[cell_id] = std::make_pair(0, 0);
                    }
                    for (const auto &measure: messages) {
                        std::string cell_id = measure.data.serving_cell.frequency + "/" + measure.data.serving_cell.orientation;
                        std::pair<double, int> &p = counts[cell_id];

                        p.first ++;
                        p.second++;
                    }

                    std::vector<double> dbl_counts;

                    for (auto &count: counts) {
                        dbl_counts.push_back(count.second.first);
                    }
                    return dbl_counts;
                }
                case vars::AVERAGE_CAM_DELAYS: {
                    std::map<std::string, std::pair<double, int>> delays;
                    std::vector<CAMMessage> messages = state._sent_cam_messages;

                    for (const auto &cell: _cells[0]) {
                        std::string cell_id = cell.frequency + "/" + cell.orientation;
                        delays[cell_id] = std::make_pair(0, 0);
                    }
                    for (const auto &measure: messages) {
                        std::string cell_id = measure.data.serving_cell.frequency + "/" + measure.data.serving_cell.orientation;
                        std::pair<double, int> &p = delays[cell_id];

                        p.first += measure.delay;
                        p.second++;
                    }

                    std::vector<double> dbl_delays;

                    for (auto &delay: delays) {
                        dbl_delays.push_back(delay.second.first != 0 ? delay.second.first / delay.second.second : 0);
                    }
                    return dbl_delays;
                }
                case vars::AVERAGE_CAM_DEBITS: {
                    std::map<std::string, std::pair<double, int>> debits;
                    std::vector<CAMMessage> messages = state._sent_cam_messages;

                    for (const auto &cell: _cells[0]) {
                        std::string cell_id = cell.frequency + "/" + cell.orientation;
                        debits[cell_id] = std::make_pair(0, 0);
                    }
                    for (const auto &measure: messages) {
                        std::string cell_id = measure.data.serving_cell.frequency + "/" + measure.data.serving_cell.orientation;
                        std::pair<double, int> &p = debits[cell_id];

                        p.first += measure.debit;
                        p.second++;
                    }

                    std::vector<double> dbl_debits;

                    for (auto &debit: debits) {
                        dbl_debits.push_back(debit.second.first != 0 ? debit.second.first / debit.second.second : 0);
                    }
                    return dbl_debits;
                }
                case vars::AVERAGE_CAM_DISTANCE: {
                    std::map<std::string, std::pair<double, int>> distances;
                    std::vector<CAMMessage> messages = state._sent_cam_messages;

                    for (const auto &cell: _cells[0]) {
                        std::string cell_id = cell.frequency + "/" + cell.orientation;
                        distances[cell_id] = std::make_pair(0, 0);
                    }
                    for (const auto &measure: messages) {
                        std::string cell_id = measure.data.serving_cell.frequency + "/" + measure.data.serving_cell.orientation;
                        std::pair<double, int> &p = distances[cell_id];

                        p.first += measure.distance;
                        p.second++;
                    }

                    std::vector<double> dbl_distances;

                    for (auto &distance: distances) {
                        dbl_distances.push_back(distance.second.first != 0 ? distance.second.first / distance.second.second : 0);
                    }
                    return dbl_distances;
                }
                case vars::CELLS_CAM_COUNT: {
                    std::map<std::string, std::pair<double, int>> counts;
                    std::vector<CAMMessage> messages = state._sent_cam_messages;

                    for (const auto &cell: _cells[0]) {
                        std::string cell_id = cell.frequency + "/" + cell.orientation;
                        counts[cell_id] = std::make_pair(0, 0);
                    }
                    for (const auto &measure: messages) {
                        std::string cell_id = measure.data.serving_cell.frequency + "/" + measure.data.serving_cell.orientation;
                        std::pair<double, int> &p = counts[cell_id];

                        p.first ++;
                        p.second++;
                    }

                    std::vector<double> dbl_counts;

                    for (auto &count: counts) {
                        dbl_counts.push_back(count.second.first);
                    }
                    return dbl_counts;
                }
                case vars::SERVING_CELL_CHANGES: {
                    return state._serving_cell_changes;
                }
                case vars::DENM_VEHICLE_COUNT: {
                    return state._denm_vehicle_indexes.size();
                }
//            case vars::VEHICLE_INDEXES: {
//                std::vector<unsigned int> indexes;
//
//                if (not _vehicles.empty()) {
//                    for (const auto &vehicle: _vehicles) {
//                        indexes.push_back(vehicle.first);
//                    }
//                }
//                return indexes;
//            }
//            case vars::VEHICLE_POSITIONS: {
//                std::vector<Coordinates> positions;
//
//                if (not _vehicles.empty()) {
//                    for (const auto &vehicle: _vehicles) {
//                        positions.push_back(vehicle.second.second);
//                    }
//                }
//                return positions;
//            }
                default:
                    return {};
            }
        }

    private:

        std::vector<Coordinates> _coordinates;
        std::vector<std::vector<Cell>> _cells;

    };

}


#endif //ARTIS_TRAFFIC_MICRO_5G_SIGNALPROCESSING_HPP
