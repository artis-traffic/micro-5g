/**
 * @file artis-traffic/micro/five-g/antenna.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_ANTENNA_HPP
#define ARTIS_TRAFFIC_MICRO_5G_ANTENNA_HPP

#include <artis-star-addons/pdevs/StateMachineDynamics.hpp>
#include <artis-star/kernel/pdevs/Dynamics.hpp>
#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/micro/five-g/Data.hpp>
#include <vector>
#include <map>
#include <deque>
#include <sstream>

#include <artis-traffic/micro/five-g/antenna-state-machine/AntennaIDs.hpp>
#include <artis-traffic/micro/five-g/antenna-state-machine/AntennaTypes.hpp>
#include <artis-traffic/micro/five-g/antenna-state-machine/AntennaRootStateMachine.hpp>
#include <artis-traffic/micro/five-g/antenna-state-machine/AntennaStateMachine.hpp>

namespace artis::traffic::micro::five_g {

    struct AntennaParameters {
        std::vector <Coordinates> coordinates;
        uint link_number;
        std::vector <Cell> cells;
    };

    template<class Model, class Parameters, class Types>
    class AntennaImplementation
            : public artis::addons::pdevs::StateMachineDynamics<artis::common::DoubleTime, Model, Parameters,
                    typename Types::root_state_machine_type> {
    public:
        struct inputs {
            enum values {
                MESSAGE,
                ACTIVE = MESSAGE + 10000,
                INACTIVE = ACTIVE + 10000,
                LINK_TRANSMITTING = INACTIVE + 10000,
                CAM_SIGNAL = LINK_TRANSMITTING + 10000,
                CAM_MESSAGE = CAM_SIGNAL + 10000,
                DENM_SIGNAL = CAM_MESSAGE + 10000,
                DENM_MESSAGE = DENM_SIGNAL + 10000,
                DENM_BROADCAST = DENM_MESSAGE + 10000
            };
        };

        struct outputs {
            enum values {
                MESSAGE, ACTIVE = MESSAGE + 10000, INACTIVE = ACTIVE + 10000,
                CAM_SIGNAL = INACTIVE + 10000, CAM_MESSAGE = CAM_SIGNAL + 10000, 
                DENM_SIGNAL = CAM_MESSAGE + 10000, DENM_MESSAGE = DENM_SIGNAL + 10000,
                DENM_BROADCAST = DENM_MESSAGE + 10000
                
            };
        };

        struct vars {
            enum values {
                VEHICLE_INDEXES,
                VEHICLE_POSITIONS,
                MEASURE_REPORTS,
                MEASURE_REPORTS_ID,
                MEASURE_REPORTS_SERVING_CELL,
                MEASURE_REPORTS_NEIGHBOR_CELLS,
                AVERAGE_MESSAGE_DELAYS_TOTAL,
                AVERAGE_MESSAGE_DELAYS,
                AVERAGE_MESSAGE_END_DELAYS,
                AVERAGE_MESSAGE_DELAYS_DISTANCE,
                AVERAGE_CAM_MESSAGE_DELAYS,
                AVERAGE_CAM_MESSAGE_END_DELAYS,
                AVERAGE_MESSAGE_DEBITS_TOTAL,
                AVERAGE_MESSAGE_DEBITS,
                AVERAGE_MESSAGE_DEBITS_DISTANCE,
                AVERAGE_CAM_MESSAGE_DEBITS,
                AVERAGE_MESSAGE_DISTANCE,
                AVERAGE_CAM_MESSAGE_DISTANCE,
                AVERAGE_DENM_MESSAGE_DELAYS,
                AVERAGE_DENM_MESSAGE_END_DELAYS,
                AVERAGE_DENM_MESSAGE_DEBITS,
                CELLS_VEHICLE_COUNT,
                CELLS_MESSAGE_COUNT,
                CELLS_CAM_MESSAGE_COUNT,
                CELLS_VEHICLE,
                CHARGE_TOTAL,
                CHARGE,
                LOST_MESSAGES_COUNT,
                CELL_IDS,
                ACTIVE_VEHICLES_CELLS,
                TRANSMITTING_VEHICLES_CELLS,
                AVERAGE_NEIGHBOR_CELLS_NUMBER,
                VEHICLE_COUNT,
                AVERAGE_CAM_MESSAGE_DELAYS_NOCELL,
                AVERAGE_CAM_MESSAGE_END_DELAYS_NOCELL,
                AVERAGE_CAM_MESSAGE_DEBITS_NOCELL,
                AVERAGE_CAM_MESSAGE_DISTANCES_NOCELL,
                AVERAGE_DENM_MESSAGE_DELAYS_NOCELL,
                AVERAGE_DENM_MESSAGE_END_DELAYS_NOCELL,
                AVERAGE_DENM_MESSAGE_DEBITS_NOCELL,
                CAM_MESSAGE_IDS
            };
        };

        AntennaImplementation(const std::string &name,
                              const artis::pdevs::Context<artis::common::DoubleTime, Model, Parameters> &context) :
                artis::addons::pdevs::StateMachineDynamics<artis::common::DoubleTime, Model, Parameters,
                        typename Types::root_state_machine_type>(name, context),
                _coordinates(context.parameters().coordinates), _link_number(context.parameters().link_number),
                _cells(context.parameters().cells) {
            for (unsigned int i = 0; i < _link_number; ++i) {
                std::stringstream ss_in;
                ss_in << "message_" << (i + 1);
                this->input_port({inputs::MESSAGE + i, ss_in.str()});

                std::stringstream ss_out;
                ss_out << "message_" << (i + 1);
                this->output_port({outputs::MESSAGE + i, ss_out.str()});

                std::stringstream ss_act;
                ss_act << "active_" << (i + 1);
                this->input_port({inputs::ACTIVE + i, ss_act.str()});

                std::stringstream ss_out_act;
                ss_out_act << "active_" << (i + 1);
                this->output_port({outputs::ACTIVE + i, ss_out_act.str()});

                std::stringstream ss_iact;
                ss_iact << "inactive_" << (i + 1);
                this->input_port({inputs::INACTIVE + i, ss_iact.str()});

                std::stringstream ss_out_iact;
                ss_out_iact << "inactive_" << (i + 1);
                this->output_port({outputs::INACTIVE + i, ss_out_iact.str()});

                std::stringstream ss_ilt;
                ss_ilt << "link_transmitting_" << (i + 1);
                this->input_port({inputs::LINK_TRANSMITTING + i, ss_ilt.str()});

                std::stringstream ss_icsm;
                ss_icsm << "cam_signal_" << (i + 1);
                this->input_port({inputs::CAM_SIGNAL + i, ss_icsm.str()});

                std::stringstream ss_ocsm;
                ss_ocsm << "cam_signal_" << (i + 1);
                this->output_port({outputs::CAM_SIGNAL + i, ss_ocsm.str()});

                std::stringstream ss_icm;
                ss_icm << "cam_message_" << (i + 1);
                this->input_port({inputs::CAM_MESSAGE + i, ss_icm.str()});

                std::stringstream ss_ocm;
                ss_ocm << "cam_message_" << (i + 1);
                this->output_port({outputs::CAM_MESSAGE + i, ss_ocm.str()});

                std::stringstream ss_idsm;
                ss_idsm << "denm_signal_" << (i + 1);
                this->input_port({inputs::DENM_SIGNAL + i, ss_idsm.str()});

                std::stringstream ss_odsm;
                ss_odsm << "denm_signal_" << (i + 1);
                this->output_port({outputs::DENM_SIGNAL + i, ss_odsm.str()});
                
                std::stringstream ss_idenm;
                ss_idenm << "denm_message_" << (i + 1);
                this->input_port({inputs::DENM_MESSAGE + i, ss_idenm.str()});

                std::stringstream ss_odenm;
                ss_odenm << "denm_message_" << (i + 1);
                this->output_port({outputs::DENM_MESSAGE + i, ss_odenm.str()});

                std::stringstream ss_idenmbc;
                ss_idenmbc << "denm_broadcast_" << (i + 1);
                this->input_port({inputs::DENM_BROADCAST + i, ss_idenmbc.str()});

                std::stringstream ss_odenmbc;
                ss_odenmbc << "denm_broadcast_" << (i + 1);
                this->output_port({outputs::DENM_BROADCAST + i, ss_odenmbc.str()});
            }

            this->observables({{vars::VEHICLE_INDEXES,                 "vehicle_indexes"},
                               {vars::VEHICLE_POSITIONS,               "vehicle_positions"},
                               {vars::MEASURE_REPORTS,                 "measure_reports"},
                               {vars::MEASURE_REPORTS_ID,              "measure_reports_ID"},
                               {vars::MEASURE_REPORTS_SERVING_CELL,    "measure_reports_serving_cell"},
                               {vars::MEASURE_REPORTS_NEIGHBOR_CELLS,  "measure_reports_neighbor_cells"},
                               {vars::AVERAGE_MESSAGE_DELAYS_TOTAL,    "average_message_delays_total"},
                               {vars::AVERAGE_MESSAGE_DELAYS,          "average_message_delays"},
                               {vars::AVERAGE_MESSAGE_END_DELAYS,      "average_message_end_delays"},
                               {vars::AVERAGE_CAM_MESSAGE_DELAYS,      "average_cam_message_delays"},
                               {vars::AVERAGE_CAM_MESSAGE_END_DELAYS,  "average_cam_message_end_delays"},
                               {vars::AVERAGE_MESSAGE_DELAYS_DISTANCE, "average_message_delays_distance"},
                               {vars::AVERAGE_MESSAGE_DEBITS_TOTAL,    "average_message_debits_total"},
                               {vars::AVERAGE_MESSAGE_DEBITS,          "average_message_debits"},
                               {vars::AVERAGE_CAM_MESSAGE_DEBITS,      "average_cam_message_debits"},
                               {vars::AVERAGE_MESSAGE_DEBITS_DISTANCE, "average_message_debits_distance"},
                               {vars::AVERAGE_MESSAGE_DISTANCE,        "average_message_distance"},
                               {vars::AVERAGE_CAM_MESSAGE_DISTANCE,    "average_cam_message_distance"},
                               {vars::AVERAGE_DENM_MESSAGE_DELAYS,      "average_denm_message_delays"},
                               {vars::AVERAGE_DENM_MESSAGE_END_DELAYS,  "average_denm_message_end_delays"},
                               {vars::AVERAGE_DENM_MESSAGE_DEBITS,      "average_denm_message_debits"},
                               {vars::CELLS_VEHICLE_COUNT,             "cells_vehicle_count"},
                               {vars::CELLS_MESSAGE_COUNT,             "cells_message_count"},
                               {vars::CELLS_CAM_MESSAGE_COUNT,         "cells_cam_message_count"},
                               {vars::CELLS_VEHICLE,                   "cells_vehicle"},
                               {vars::CHARGE_TOTAL,                    "charge_total"},
                               {vars::CHARGE,                          "charge"},
                               {vars::LOST_MESSAGES_COUNT,             "lost_messages_count"},
                               {vars::CELL_IDS,                        "cell_ids"},
                               {vars::ACTIVE_VEHICLES_CELLS,           "active_vehicles_cells"},
                               {vars::TRANSMITTING_VEHICLES_CELLS,     "transmitting_vehicles_cells"},
                               {vars::AVERAGE_NEIGHBOR_CELLS_NUMBER,   "average_neighbor_cells_number"},
                               {vars::VEHICLE_COUNT,                   "vehicle_count"},
                               {vars::AVERAGE_CAM_MESSAGE_DELAYS_NOCELL,      "average_cam_message_delays_nocell"},
                               {vars::AVERAGE_CAM_MESSAGE_END_DELAYS_NOCELL,  "average_cam_message_end_delays_nocell"},
                               {vars::AVERAGE_CAM_MESSAGE_DEBITS_NOCELL,      "average_cam_message_debits_nocell"},
                               {vars::AVERAGE_CAM_MESSAGE_DISTANCES_NOCELL,      "average_cam_message_distances_nocell"},
                               {vars::AVERAGE_DENM_MESSAGE_DELAYS_NOCELL,      "average_denm_message_delays_nocell"},
                               {vars::AVERAGE_DENM_MESSAGE_END_DELAYS_NOCELL,  "average_denm_message_end_delays_nocell"},
                               {vars::AVERAGE_DENM_MESSAGE_DEBITS_NOCELL,      "average_denm_message_debits_nocell"},
                               {vars::CAM_MESSAGE_IDS,                 "cam_message_ids"}});
        }

        virtual ~AntennaImplementation() = default;

//        void dconf(const artis::traffic::core::Time & /* t* */,
//                   const artis::traffic::core::Time & /* e */,
//                   const artis::traffic::core::Bag & /* bag */) override;
//
//        void dint(const artis::traffic::core::Time & /* t */) override;

        void dext(const artis::traffic::core::Time &t,
                  const artis::traffic::core::Time &e,
                  const artis::traffic::core::Bag &bag) override {
            std::for_each(bag.begin(), bag.end(),
                          [t, this](const artis::traffic::core::ExternalEvent &event) {
                              for (unsigned int i = 0; i < _link_number; i++) {
                                  if (event.on_port(inputs::MESSAGE + i)) {
                                      MeasureReportMessage message{};

                                      event.data()(message);

                                      MeasureReportMessagePair message_pair = {message, i};
//                                      std::cout<<this->get_full_name() << " t "<<t<<" ANTENNA IN " << i << " message "<<message.message_id<< " vehicle " << message.data.vehicle_index << std::endl;
                                      this->root().transition(t, AntennaStateMachineIDs::ANTENNA_MESSAGE,
                                                              typename Types::state_machine_type::ExternalEvent{
                                                                      Types::event_IDs_type::NEW_ANTENNA_MESSAGE,
                                                                      message_pair});

//                                      std::cout<<this->get_full_name() << " t "<<t<<" ANTENNA LINK NOT TRANSMITTING " << i << " cell "<<message.data.serving_cell.cell_id<< " mess " << message.message_id << std::endl;
//
//                                      this->root().transition(t, AntennaStateMachineIDs::ANTENNA_ACTIVE,
//                                                              typename Types::state_machine_type::ExternalEvent{
//                                                                      Types::event_IDs_type::LINK_NOT_TRANSMITTING,
//                                                                      message.data.serving_cell.cell_id});

                                  }
                                  if (event.on_port(inputs::ACTIVE + i)) {
                                      DENMMessage message{};

                                      event.data()(message);

                                      std::string cell_id = message.data.serving_cell.cell_id;

//                                      std::cout<<this->get_full_name() << " t "<<t<<" ANTENNA ACTIVE " << i << " cell "<<cell_id<< " mess " << message.message_id << std::endl;
                                      this->root().transition(t, AntennaStateMachineIDs::ANTENNA_ACTIVE,
                                                              typename Types::state_machine_type::ExternalEvent{
                                                                      Types::event_IDs_type::NEW_ANTENNA_ACTIVE,
                                                                      cell_id});

                                  }
                                  if (event.on_port(inputs::INACTIVE + i)) {
                                      DENMMessage message{};

                                      event.data()(message);

                                      std::string cell_id = message.data.serving_cell.cell_id;

//                                      std::cout<<this->get_full_name() << " t "<<t<<" ANTENNA INACTIVE " << i << " cell "<<cell_id<< " mess " << message.message_id << std::endl;
                                      this->root().transition(t, AntennaStateMachineIDs::ANTENNA_ACTIVE,
                                                              typename Types::state_machine_type::ExternalEvent{
                                                                      Types::event_IDs_type::NEW_ANTENNA_INACTIVE,
                                                                      cell_id});

                                  }
                                  if (event.on_port(inputs::LINK_TRANSMITTING + i)) {
                                      CAMSignalMessage message{};

                                      event.data()(message);

                                      std::string cell_id = message.data.serving_cell.cell_id;

//                                      std::cout<<this->get_full_name() << " t "<<t<<" ANTENNA LINKTRANSMITTING " << i << " cell "<<cell_id<< " mess " << message.message_id << std::endl;
                                      this->root().transition(t, AntennaStateMachineIDs::ANTENNA_ACTIVE,
                                                              typename Types::state_machine_type::ExternalEvent{
                                                                      Types::event_IDs_type::LINK_TRANSMITTING,
                                                                      cell_id});

                                  }
                                  if (event.on_port(inputs::CAM_SIGNAL + i)) {
                                      CAMSignalMessage message{};

                                      event.data()(message);

                                      CAMSignalMessagePair message_pair = {message, i};
//                                      std::cout<<this->get_full_name() << " t "<<t<<" ANTENNA CAMSIGNAL " << i << " message "<<message.message_id<< " vehicle " << message.data.vehicle_index << std::endl;
                                      this->root().transition(t, AntennaStateMachineIDs::ANTENNA_CAM,
                                                              typename Types::state_machine_type::ExternalEvent{
                                                                      Types::event_IDs_type::NEW_ANTENNA_CAM_SIGNAL,
                                                                      message_pair});

//                                      std::cout<<this->get_full_name() << " t "<<t<<" ANTENNA LINK TRANSMITTING CAM " << i << " cell "<<message.data.serving_cell.cell_id<< " mess " << message.message_id << std::endl;
//
//                                      this->root().transition(t, AntennaStateMachineIDs::ANTENNA_ACTIVE,
//                                                              typename Types::state_machine_type::ExternalEvent{
//                                                                      Types::event_IDs_type::LINK_TRANSMITTING,
//                                                                      message.data.serving_cell.cell_id});

                                  }
                                  if (event.on_port(inputs::CAM_MESSAGE + i)) {
                                      CAMMessage message{};

                                      event.data()(message);

                                      CAMMessagePair message_pair = {message, i};
//                                      std::cout << this->get_full_name() << " t " << t << " ANTENNA CAMMESSAGE " << i
//                                                << " message " << message.message_id << " vehicle "
//                                                << message.data.vehicle_index << std::endl;
                                      this->root().transition(t, AntennaStateMachineIDs::ANTENNA_CAM,
                                                              typename Types::state_machine_type::ExternalEvent{
                                                                      Types::event_IDs_type::NEW_ANTENNA_CAM_MESSAGE,
                                                                      message_pair});

//                                      std::cout<<this->get_full_name() << " t "<<t<<" ANTENNA LINK NOT TRANSMITTING " << i << " cell "<<message.data.serving_cell.cell_id<< " mess " << message.message_id << std::endl;
                                      this->root().transition(t, AntennaStateMachineIDs::ANTENNA_ACTIVE,
                                                              typename Types::state_machine_type::ExternalEvent{
                                                                      Types::event_IDs_type::LINK_NOT_TRANSMITTING,
                                                                      message.data.serving_cell.cell_id});
                                  }
                                  if (event.on_port(inputs::DENM_SIGNAL + i)) {
                                      DENMSignalMessage message{};

                                      event.data()(message);

                                      DENMSignalMessagePair message_pair = {message, i};
//                                      std::cout<<this->get_full_name() << " t "<<t<<" ANTENNA DENMSIGNAL " << i << " message "<<message.message_id<< " vehicle " << message.data.vehicle_index << std::endl;
                                      this->root().transition(t, AntennaStateMachineIDs::ANTENNA_DENM,
                                                              typename Types::state_machine_type::ExternalEvent{
                                                                      Types::event_IDs_type::NEW_ANTENNA_DENM_SIGNAL,
                                                                      message_pair});

//                                      std::cout<<this->get_full_name() << " t "<<t<<" ANTENNA LINK TRANSMITTING DENM " << i << " cell "<<message.data.serving_cell.cell_id<< " mess " << message.message_id << std::endl;
//
//                                      this->root().transition(t, AntennaStateMachineIDs::ANTENNA_ACTIVE,
//                                                              typename Types::state_machine_type::ExternalEvent{
//                                                                      Types::event_IDs_type::LINK_TRANSMITTING,
//                                                                      message.data.serving_cell.cell_id});

                                  }
                                  if (event.on_port(inputs::DENM_MESSAGE + i)) {
                                      DENMMessage message{};

                                      event.data()(message);

                                      DENMMessagePair message_pair = {message, i};
//                                      std::cout << this->get_full_name() << " t " << t << " ANTENNA DENMMESSAGE " << i
//                                                << " message " << message.message_id << " vehicle "
//                                                << message.data.vehicle_index << std::endl;
                                      this->root().transition(t, AntennaStateMachineIDs::ANTENNA_DENM,
                                                              typename Types::state_machine_type::ExternalEvent{
                                                                      Types::event_IDs_type::NEW_ANTENNA_DENM_MESSAGE,
                                                                      message_pair});

//                                      std::cout<<this->get_full_name() << " t "<<t<<" ANTENNA LINK NOT TRANSMITTING " << i << " cell "<<message.data.serving_cell.cell_id<< " mess " << message.message_id << std::endl;
//                                      this->root().transition(t, AntennaStateMachineIDs::ANTENNA_ACTIVE,
//                                                              typename Types::state_machine_type::ExternalEvent{
//                                                                      Types::event_IDs_type::LINK_NOT_TRANSMITTING,
//                                                                      message.data.serving_cell.cell_id});
                                  }
                                  if (event.on_port(inputs::DENM_BROADCAST + i)) {
                                      DENMMessage message{};

                                      event.data()(message);

                                      DENMMessagePair message_pair = {message, i};
//                                      std::cout<<this->get_full_name() << " t "<<t<<" ANTENNA DENMBROADCAST " << i << " message "<<message.message_id<< " vehicle " << message.data.vehicle_index << std::endl;
                                      this->root().transition(t, AntennaStateMachineIDs::ANTENNA_DENM,
                                                              typename Types::state_machine_type::ExternalEvent{
                                                                      Types::event_IDs_type::BROADCAST_ANTENNA_DENM_MESSAGE,
                                                                      message_pair});


                                  }
                              }
                          });
        }

        artis::traffic::core::Bag lambda(const artis::traffic::core::Time &t) const override {
            artis::traffic::core::Bag bag;
            const typename Types::root_state_machine_type::external_events_type &events = this->root().outbox();

            std::for_each(events.cbegin(), events.cend(), [t, this, &bag](const auto &e) {
                switch (e.id) {
                    case AntennaEventIDs::SEND_ANTENNA_MESSAGE: {
                        MeasureReportMessagePair message_pair;
                        e.data(message_pair);
                        message_pair.message.cell_change = false;
//                        std::cout<<"ANTENNA SEND MESSAGE t " << t << message_pair.message.message_id<<std::endl;
                        bag.push_back(
                                artis::traffic::core::ExternalEvent(outputs::MESSAGE + message_pair.port_index,
                                                                    message_pair.message));
                        break;
                    }
                    case AntennaEventIDs::SEND_ANTENNA_ACTIVE: {
                        std::vector <std::string> active_cells_id;
                        e.data(active_cells_id);
//                        std::cout<<"ANTENNA SEND ACTIVE t " << t <<std::endl;
                        for (unsigned int i = 0; i < _link_number; i++) {
                            bag.push_back(
                                    artis::traffic::core::ExternalEvent(outputs::ACTIVE + i, active_cells_id));
                        }
                        break;
                    }
                    case AntennaEventIDs::SEND_ANTENNA_INACTIVE: {
                        std::vector <std::string> inactive_cells_id;
                        e.data(inactive_cells_id);
//                        std::cout<<"ANTENNA SEND INACTIVE t " << t <<std::endl;
                        for (unsigned int i = 0; i < _link_number; i++) {
                            bag.push_back(
                                    artis::traffic::core::ExternalEvent(outputs::INACTIVE + i, inactive_cells_id));
                        }
                        break;
                    }
                    case AntennaEventIDs::SEND_ANTENNA_CAM_SIGNAL: {
                        CAMSignalMessagePair message_pair;
                        e.data(message_pair);
                        message_pair.message.cell_change = false;
//                        std::cout<<"ANTENNA SEND CAM SIGNAL t " << t << " " << message_pair.message.message_id<<std::endl;
                        bag.push_back(
                                artis::traffic::core::ExternalEvent(outputs::CAM_SIGNAL + message_pair.port_index,
                                                                    message_pair.message));
                        break;
                    }
                    case AntennaEventIDs::SEND_ANTENNA_CAM_MESSAGE: {
                        CAMMessagePair message_pair;
                        e.data(message_pair);
                        message_pair.message.cell_change = false;
//                        std::cout<<"ANTENNA SEND DENM t " << t << " " << message_pair.message.message_id<<std::endl;
//                        std::cout<<"CAM RELAY ACTIVE VEHICLE "<<message_pair.message.vehicle_count<<std::endl;
                        bag.push_back(
                                artis::traffic::core::ExternalEvent(outputs::CAM_MESSAGE + message_pair.port_index,
                                                                    message_pair.message));
                        break;
                    }
                    case AntennaEventIDs::SEND_ANTENNA_DENM_SIGNAL: {
                        DENMSignalMessagePair message_pair;
                        e.data(message_pair);
                        message_pair.message.cell_change = false;
//                        std::cout<<"ANTENNA SEND DENM SIGNAL t " << t << " " << message_pair.message.message_id<<std::endl;
                        bag.push_back(
                                artis::traffic::core::ExternalEvent(outputs::DENM_SIGNAL + message_pair.port_index,
                                                                    message_pair.message));
                        break;
                    }
                    case AntennaEventIDs::SEND_ANTENNA_DENM_MESSAGE: {
                        DENMMessagePair message_pair;
                        e.data(message_pair);
                        message_pair.message.cell_change = false;
//                        std::cout<<"ANTENNA SEND DENM t " << t << " " << message_pair.message.message_id<<std::endl;
//                        std::cout<<"DENM RELAY ACTIVE VEHICLE "<<message_pair.message.vehicle_count<<std::endl;
                        bag.push_back(
                                artis::traffic::core::ExternalEvent(outputs::DENM_MESSAGE,
                                                                    message_pair.message));
                        break;
                    }
                    case AntennaEventIDs::SEND_BROADCAST_ANTENNA_DENM_MESSAGE: {
                        DENMMessagePair message_pair;
                        e.data(message_pair);
                        message_pair.message.cell_change = false;
//                        std::cout<<"ANTENNA SEND DENM BROADCAST t " << t << " " << message_pair.message.message_id<<std::endl;
//                        std::cout<<"DENM RELAY ACTIVE VEHICLE "<<message_pair.message.vehicle_count<<std::endl;
                        for (unsigned int i = 0; i < _link_number; i++) {
                            bag.push_back(
                                    artis::traffic::core::ExternalEvent(outputs::DENM_BROADCAST + i,
                                                                        message_pair.message));
                        }
                        break;
                    }
                    
                }
            });
            return bag;
        }

        artis::common::event::Value observe(const artis::traffic::core::Time &t, unsigned int index) const override {
            const auto &message_state = this->root().template state_<AntennaMessageState>(
                    Types::state_machine_IDs_type::ANTENNA_MESSAGE);
            const auto &active_state = this->root().template state_<AntennaActiveState>(
                    Types::state_machine_IDs_type::ANTENNA_ACTIVE);
            const auto &cam_state = this->root().template state_<AntennaCAMState>(
                    Types::state_machine_IDs_type::ANTENNA_CAM);
            const auto &denm_state = this->root().template state_<AntennaDENMState>(
                    Types::state_machine_IDs_type::ANTENNA_DENM);
            switch (index) {
                case vars::VEHICLE_INDEXES: {
                    std::vector<unsigned int> indexes(message_state._vehicles.size());

                    for (const auto &vehicle: message_state._vehicles) {
                        indexes.push_back(vehicle.first);
                    }
                    return indexes;
                }
//    case vars::VEHICLE_POSITIONS: {
//      std::vector<Coordinates> positions;
//
//      if (not _vehicles.empty()) {
//        for (const auto &vehicle: _vehicles) {
//          positions.push_back(vehicle.second.second);
//        }
//      }
//      return positions;
//    }
                case vars::MEASURE_REPORTS: {
                    std::vector <Data> measure_reports(message_state._measure_messages.size());

                    for (const auto &measure: message_state._measure_messages) {
                        measure_reports.push_back(measure.data);
                    }
                    return measure_reports;
                }
                case vars::MEASURE_REPORTS_ID: {
                    std::vector <std::string> measure_reports_info;

                    for (const auto &measure: message_state._measure_messages) {
                        if (measure.next_time >= t - 1) {
                            measure_reports_info.push_back(measure.data.measure_id);
                        }
                    }
                    return measure_reports_info;
                }
                case vars::MEASURE_REPORTS_SERVING_CELL: {
                    std::vector <Cell> measure_reports_info;

                    for (const auto &measure: message_state._measure_messages) {
                        if (measure.next_time >= t - 1) {
                            measure_reports_info.push_back(measure.data.serving_cell);
                        }
                    }
                    return measure_reports_info;
                }
                case vars::MEASURE_REPORTS_NEIGHBOR_CELLS: {
                    std::vector <std::vector<Cell>> measure_reports_info;

                    for (const auto &measure: message_state._measure_messages) {
                        if (measure.next_time >= t - 1) {
                            measure_reports_info.push_back(measure.neighbor_cells);
                        }
                    }
                    return measure_reports_info;
                }
                case vars::AVERAGE_MESSAGE_DELAYS_TOTAL: {
                    double average_message_delays = 0;
                    std::vector <CAMMessage> messages = cam_state._cam_messages;

                    if (not messages.empty()) {
                        for (const auto &measure: messages) {
                            average_message_delays += measure.delay;
                        }
                        return average_message_delays / message_state._measure_messages.size();
                    }
                    return -1;
                }
                case vars::AVERAGE_MESSAGE_DELAYS: {
                    std::map <std::string, std::pair<double, int>> delays;
                    std::vector <MeasureReportMessage> messages = message_state._measure_messages;

                    for (const auto &cell: message_state._cells_vehicles) {
                        delays[cell.first] = std::make_pair(0, 0);
                    }
                    for (const auto &measure: messages) {
                        std::pair<double, int> &p = delays[measure.data.serving_cell.cell_id];

                        p.first += measure.delay;
                        p.second++;
                    }

                    std::vector<double> dbl_delays;

                    for (auto &delay: delays) {
                        dbl_delays.push_back(delay.second.first != 0 ? delay.second.first / delay.second.second : 0);
                    }
                    return dbl_delays;
                }
                case vars::AVERAGE_MESSAGE_END_DELAYS: {
                    std::map <std::string, std::pair<double, int>> delays;
                    std::vector <MeasureReportMessage> messages = message_state._measure_messages;

                    for (const auto &cell: message_state._cells_vehicles) {
                        delays[cell.first] = std::make_pair(0, 0);
                    }
                    for (const auto &measure: messages) {
                        std::pair<double, int> &p = delays[measure.data.serving_cell.cell_id];

                        p.first += measure.end_delay;
                        p.second++;
                    }

                    std::vector<double> dbl_delays;

                    for (auto &delay: delays) {
                        dbl_delays.push_back(delay.second.first != 0 ? delay.second.first / delay.second.second : 0);
                    }
                    return dbl_delays;
                }
                case vars::AVERAGE_CAM_MESSAGE_DELAYS: {
                    std::map <std::string, std::pair<double, int>> delays;
                    std::vector <CAMMessage> messages = cam_state._cam_messages;

                    for (const auto &cell: message_state._cells_vehicles) {
                        delays[cell.first] = std::make_pair(0, 0);
                    }
                    for (const auto &measure: messages) {
                        std::pair<double, int> &p = delays[measure.data.serving_cell.cell_id];

                        p.first += measure.delay;
                        p.second++;
                    }

                    std::vector<double> dbl_delays;

                    for (auto &delay: delays) {
                        dbl_delays.push_back(delay.second.first != 0 ? delay.second.first / delay.second.second : 0);
                    }
                    return dbl_delays;
                }
                case vars::AVERAGE_CAM_MESSAGE_END_DELAYS: {
                    std::map <std::string, std::pair<double, int>> delays;
                    std::vector <CAMMessage> messages = cam_state._cam_messages;

                    for (const auto &cell: message_state._cells_vehicles) {
                        delays[cell.first] = std::make_pair(0, 0);
                    }
                    for (const auto &measure: messages) {
                        std::pair<double, int> &p = delays[measure.data.serving_cell.cell_id];

                        p.first += measure.end_delay;
                        p.second++;
                    }

                    std::vector<double> dbl_delays;

                    for (auto &delay: delays) {
                        dbl_delays.push_back(delay.second.first != 0 ? delay.second.first / delay.second.second : 0);
                    }
                    return dbl_delays;
                }
                case vars::AVERAGE_MESSAGE_DELAYS_DISTANCE: {
                    using Delay = std::pair <std::pair<double, int>, std::pair<double, int>>;
                    using Delays = std::map<unsigned int, Delay>;
                    std::map <std::string, Delays> delays;
                    std::vector <CAMMessage> messages = cam_state._cam_messages;

                    for (const auto &cell: message_state._cells_vehicles) {
                        auto &delay = delays[cell.first] = Delays();

                        if (cell.first.find("700") != std::string::npos) {
                            delay[1200] = Delay{std::make_pair(0, 0), std::make_pair(0, 0)};
                        } else if (cell.first.find("3500") != std::string::npos) {
                            delay[150] = Delay{std::make_pair(0, 0), std::make_pair(0, 0)};
                        }
                    }
                    for (const auto &measure: messages) {
                        std::string cell_id = measure.data.serving_cell.cell_id;
                        unsigned int distance = cell_id.find("700") != std::string::npos ? 1200 : 150;
                        auto &p = measure.distance < distance ? delays[cell_id][distance].first
                                                              : delays[cell_id][distance].second;

                        p.first += measure.delay;
                        p.second++;
                    }

                    std::vector<double> dbl_delays;

                    for (const auto &delay: delays) {
                        for (const auto &d: delay.second) {
                            dbl_delays.push_back(
                                    d.second.second.first != 0 ? d.second.second.first / d.second.second.second : 0);
                            dbl_delays.push_back(
                                    d.second.first.first != 0 ? d.second.first.first / d.second.first.second : 0);
                        }
                    }
                    return dbl_delays;
                }
                case vars::AVERAGE_MESSAGE_DEBITS_TOTAL: {
                    double average_message_debits = 0;
                    std::vector <CAMMessage> messages = cam_state._cam_messages;

                    if (not messages.empty()) {
                        for (const auto &measure: messages) {
                            average_message_debits += measure.debit;
                        }
                        return average_message_debits / message_state._measure_messages.size();
                    }
                    return -1;
                }
                case vars::AVERAGE_MESSAGE_DEBITS: {
                    std::map <std::string, std::pair<double, int>> debits;
                    std::vector <MeasureReportMessage> messages = message_state._measure_messages;
                    for (const auto &cell: message_state._cells_vehicles) {
                        debits[cell.first] = std::make_pair(0, 0);
                    }
                    if (not messages.empty()) {
                        for (const auto &measure: messages) {
                            std::pair<double, int> &p = debits[measure.data.serving_cell.cell_id];

                            p.first += measure.debit;
                            p.second++;
                        }
                    }
                    std::vector<double> dbl_debits;

                    for (const auto &debit: debits) {
                        dbl_debits.push_back(debit.second.first != 0 ? debit.second.first / debit.second.second : 0);
                    }
                    return dbl_debits;
                }
                case vars::AVERAGE_CAM_MESSAGE_DEBITS: {
                    std::map <std::string, std::pair<double, int>> debits;
                    std::vector <CAMMessage> messages = cam_state._cam_messages;
                    for (const auto &cell: message_state._cells_vehicles) {
                        debits[cell.first] = std::make_pair(0, 0);
                    }
                    if (not messages.empty()) {
                        for (const auto &measure: messages) {
                            std::pair<double, int> &p = debits[measure.data.serving_cell.cell_id];

                            p.first += measure.debit;
                            p.second++;
                        }
                    }
                    std::vector<double> dbl_debits;

                    for (const auto &debit: debits) {
                        dbl_debits.push_back(debit.second.first != 0 ? debit.second.first / debit.second.second : 0);
                    }
                    return dbl_debits;
                }
                case vars::AVERAGE_MESSAGE_DEBITS_DISTANCE: {
                    using Debit = std::pair <std::pair<double, int>, std::pair<double, int>>;
                    using Debits = std::map<unsigned int, Debit>;
                    std::map <std::string, Debits> debits;
                    std::vector <CAMMessage> messages = cam_state._cam_messages;

                    for (const auto &cell: message_state._cells_vehicles) {

                        auto &debit = debits[cell.first] = Debits();

                        if (cell.first.find("700") != std::string::npos) {
                            debit[1200] = Debit{std::make_pair(0, 0), std::make_pair(0, 0)};
                        } else if (cell.first.find("3500") != std::string::npos) {
                            debit[150] = Debit{std::make_pair(0, 0), std::make_pair(0, 0)};
                        }
                    }
                    for (const auto &measure: messages) {
                        std::string cell_id = measure.data.serving_cell.cell_id;
                        unsigned int distance = cell_id.find("700") != std::string::npos ? 1200 : 150;
                        auto &p = measure.distance < distance ? debits[cell_id][distance].first
                                                              : debits[cell_id][distance].second;

                        p.first += measure.debit;
                        p.second++;
                    }

                    std::vector<double> dbl_debits;

                    for (const auto &debit: debits) {
                        for (const auto &d: debit.second) {
                            dbl_debits.push_back(
                                    d.second.second.first != 0 ? d.second.second.first / d.second.second.second : 0);
                            dbl_debits.push_back(
                                    d.second.first.first != 0 ? d.second.first.first / d.second.first.second : 0);

                        }
                    }
                    return dbl_debits;
                }
                case vars::AVERAGE_DENM_MESSAGE_DELAYS: {
                    std::map <std::string, std::pair<double, int>> delays;
                    std::vector <DENMMessage> messages = denm_state._denm_messages;

                    for (const auto &cell: message_state._cells_vehicles) {
                        delays[cell.first] = std::make_pair(0, 0);
                    }
                    for (const auto &measure: messages) {
                        std::pair<double, int> &p = delays[measure.data.serving_cell.cell_id];

                        p.first += measure.delay;
                        p.second++;
                    }

                    std::vector<double> dbl_delays;

                    for (auto &delay: delays) {
                        dbl_delays.push_back(delay.second.first != 0 ? delay.second.first / delay.second.second : 0);
                    }
                    return dbl_delays;
                }
                case vars::AVERAGE_DENM_MESSAGE_END_DELAYS: {
                    std::map <std::string, std::pair<double, int>> delays;
                    std::vector <DENMMessage> messages = denm_state._denm_messages;

                    for (const auto &cell: message_state._cells_vehicles) {
                        delays[cell.first] = std::make_pair(0, 0);
                    }
                    for (const auto &measure: messages) {
                        std::pair<double, int> &p = delays[measure.data.serving_cell.cell_id];

                        p.first += measure.end_delay;
                        p.second++;
                    }

                    std::vector<double> dbl_delays;

                    for (auto &delay: delays) {
                        dbl_delays.push_back(delay.second.first != 0 ? delay.second.first / delay.second.second : 0);
                    }
                    return dbl_delays;
                }
                case vars::AVERAGE_DENM_MESSAGE_DEBITS: {
                    std::map <std::string, std::pair<double, int>> debits;
                    std::vector <DENMMessage> messages = denm_state._denm_messages;
                    for (const auto &cell: message_state._cells_vehicles) {
                        debits[cell.first] = std::make_pair(0, 0);
                    }
                    if (not messages.empty()) {
                        for (const auto &measure: messages) {
                            std::pair<double, int> &p = debits[measure.data.serving_cell.cell_id];

                            p.first += measure.debit;
                            p.second++;
                        }
                    }
                    std::vector<double> dbl_debits;

                    for (const auto &debit: debits) {
                        dbl_debits.push_back(debit.second.first != 0 ? debit.second.first / debit.second.second : 0);
                    }
                    return dbl_debits;
                }
                case vars::CELLS_VEHICLE_COUNT: {
                    std::vector <uint> counts;

                    for (const auto &cell_vehicle: message_state._cells_vehicles) {
                        counts.push_back(cell_vehicle.second.size());
                    }
                    return counts;
                }
                case vars::CELLS_MESSAGE_COUNT: {
                    std::map <std::string, std::pair<double, int>> counts;
                    std::vector <MeasureReportMessage> messages = message_state._measure_messages;
                    for (const auto &cell: message_state._cells_vehicles) {
                        counts[cell.first] = std::make_pair(0, 0);
                    }
                    if (not messages.empty()) {
                        for (const auto &measure: messages) {
                            std::pair<double, int> &p = counts[measure.data.serving_cell.cell_id];

                            p.first++;
                            p.second++;
                        }
                    }
                    std::vector<double> dbl_counts;

                    for (const auto &debit: counts) {
                        dbl_counts.push_back(debit.second.first);
                    }
                    return dbl_counts;
                }
                case vars::CELLS_CAM_MESSAGE_COUNT: {
                    std::map <std::string, std::pair<double, int>> counts;
                    std::vector <CAMMessage> messages = cam_state._cam_messages;
                    for (const auto &cell: message_state._cells_vehicles) {
                        counts[cell.first] = std::make_pair(0, 0);
                    }
                    if (not messages.empty()) {
                        for (const auto &measure: messages) {
                            std::pair<double, int> &p = counts[measure.data.serving_cell.cell_id];

                            p.first++;
                            p.second++;
                        }
                    }
                    std::vector<double> dbl_counts;

                    for (const auto &debit: counts) {
                        dbl_counts.push_back(debit.second.first);
                    }
                    return dbl_counts;
                }
                case vars::CELLS_VEHICLE: {
                    std::vector <std::string> counts;

                    for (const auto &cell_vehicle: message_state._cells_vehicles) {
                        std::string vehicles;

                        for (uint i = 0; i < cell_vehicle.second.size(); i++) {
                            if (i < cell_vehicle.second.size() - 1) {
                                vehicles += std::to_string(cell_vehicle.second[i]) + '|';
                            } else {
                                vehicles += std::to_string(cell_vehicle.second[i]);
                            }
                        }
                        counts.push_back(cell_vehicle.first + ":" + vehicles);
                    }
                    return counts;
                }
                case vars::CHARGE_TOTAL: {
                    double charge = 0;
                    std::vector <CAMMessage> messages = cam_state._cam_messages;

                    for (const auto &measure: messages) {
                        if (measure.next_time >= t - 1) {
                            charge += measure.message_size();
                        }
                    }
                    return charge;
                }
                case vars::CHARGE: {
                    std::map <std::string, std::pair<double, int>> charges;
                    std::vector <CAMMessage> messages = cam_state._cam_messages;

                    for (const auto &cell: message_state._cells_vehicles) {
                        charges[cell.first] = std::make_pair(0, 0);
                    }
                    if (not messages.empty()) {
                        for (const auto &measure: messages) {
                            if (measure.next_time >= t - 1) {
                                auto &p = charges[measure.data.serving_cell.cell_id];

                                p.first += measure.message_size();
                                p.second++; // inutile ?
                            }
                        }
                    }
                    std::vector<double> dbl_charges;

                    for (const auto &charge: charges) {
                        dbl_charges.push_back(charge.second.first);
                    }
                    return dbl_charges;
                }
                case vars::LOST_MESSAGES_COUNT: {
                    return message_state._lost_messages_count;
                }
                case vars::AVERAGE_MESSAGE_DISTANCE: {
                    std::map <std::string, std::pair<double, int>> distances;
                    std::vector <MeasureReportMessage> messages = message_state._measure_messages;

                    for (const auto &cell: message_state._cells_vehicles) {
                        distances[cell.first] = std::make_pair(0, 0);
                    }
                    if (not messages.empty()) {
                        for (const auto &measure: messages) {
                            std::pair<double, int> &p = distances[measure.data.serving_cell.cell_id];

                            p.first += measure.distance;
                            p.second++;
                        }
                    }
                    std::vector<double> dbl_distances;

                    for (const auto &distance: distances) {
                        dbl_distances.push_back(
                                distance.second.first != 0 ? distance.second.first / distance.second.second : 0);
                    }
                    return dbl_distances;
                }
                case vars::AVERAGE_CAM_MESSAGE_DISTANCE: {
                    std::map <std::string, std::pair<double, int>> distances;
                    std::vector <CAMMessage> messages = cam_state._cam_messages;

                    for (const auto &cell: message_state._cells_vehicles) {
                        distances[cell.first] = std::make_pair(0, 0);
                    }
                    if (not messages.empty()) {
                        for (const auto &measure: messages) {
                            std::pair<double, int> &p = distances[measure.data.serving_cell.cell_id];

                            p.first += measure.distance;
                            p.second++;
                        }
                    }
                    std::vector<double> dbl_distances;

                    for (const auto &distance: distances) {
                        dbl_distances.push_back(
                                distance.second.first != 0 ? distance.second.first / distance.second.second : 0);
                    }
                    return dbl_distances;
                }
                case vars::CELL_IDS: {
                    std::vector <std::string> cell_ids;

                    for (const auto &cell: message_state._cells_vehicles) {
                        cell_ids.push_back(cell.first);
                    }
                    return cell_ids;
                }
                case vars::ACTIVE_VEHICLES_CELLS: {
                    std::map<std::string, int> actives;

                    for (const auto &cell: active_state._cells_active_vehicles) {
                        actives[cell.first] = cell.second;
                    }

                    std::vector<double> dbl_actives;

                    for (const auto &active: actives) {
                        dbl_actives.push_back(active.second);
                    }
                    return dbl_actives;
                }
                case vars::TRANSMITTING_VEHICLES_CELLS: {
                    std::map<std::string, int> transmits;

                    for (const auto &cell: active_state._cells_transmitting_vehicles) {
                        transmits[cell.first] = cell.second;
                    }

                    std::vector<double> dbl_transmits;

                    for (const auto &transmit: transmits) {
                        dbl_transmits.push_back(transmit.second);
                    }
                    return dbl_transmits;
                }
                case vars::AVERAGE_NEIGHBOR_CELLS_NUMBER: {
                    double avg_neighbor_cell = 0;

                    if (message_state._measure_messages.size() > 0) {
                        for (const auto &measure: message_state._measure_messages) {
                            avg_neighbor_cell += measure.neighbor_cells.size();
                        }
                        avg_neighbor_cell /= message_state._measure_messages.size();
                    }
                    return avg_neighbor_cell;
                }
                case vars::VEHICLE_COUNT: {
                    return message_state._vehicles.size();
                }
                case vars::AVERAGE_CAM_MESSAGE_DEBITS_NOCELL: {
                    double avg_debit = 0;
                    if (cam_state._cam_messages.size() > 0) {
                        for (const auto &message: cam_state._cam_messages) {
                            avg_debit += message.debit;
                        }
                        return avg_debit / cam_state._cam_messages.size();
                    }
                    else {
                        return 0;
                    }
                }
                case vars::AVERAGE_CAM_MESSAGE_DELAYS_NOCELL: {
                    double avg_delay = 0;
                    if (cam_state._cam_messages.size() > 0) {
                        for (const auto &message: cam_state._cam_messages) {
                            avg_delay += message.delay;
                        }
                        return avg_delay / cam_state._cam_messages.size();
                    }
                    else {
                        return 0;
                    }
                }
                case vars::AVERAGE_CAM_MESSAGE_DISTANCES_NOCELL: {
                    double avg_distance = 0;
                    if (cam_state._cam_messages.size() > 0) {
                        for (const auto &message: cam_state._cam_messages) {
                            avg_distance += message.distance;
                        }
                        return avg_distance / cam_state._cam_messages.size();
                    }
                    else {
                        return 0;
                    }
                }
                case vars::AVERAGE_CAM_MESSAGE_END_DELAYS_NOCELL: {
                    double avg_delay = 0;
                    if (cam_state._cam_messages.size() > 0) {
                        for (const auto &message: cam_state._cam_messages) {
                            avg_delay += message.end_delay;
                        }
                        return avg_delay / cam_state._cam_messages.size();
                    }
                    else {
                        return 0;
                    }
                }
                case vars::AVERAGE_DENM_MESSAGE_DEBITS_NOCELL: {
                    double avg_debit = 0;
                    if (cam_state._cam_messages.size() > 0) {
                        for (const auto &message: cam_state._cam_messages) {
                            avg_debit += message.debit;
                        }
                        return avg_debit / cam_state._cam_messages.size();
                    }
                    else {
                        return 0;
                    }
                }
                case vars::AVERAGE_DENM_MESSAGE_DELAYS_NOCELL: {
                    double avg_delay = 0;
                    if (cam_state._cam_messages.size() > 0) {
                        for (const auto &message: cam_state._cam_messages) {
                            avg_delay += message.delay;
                        }
                        return avg_delay / cam_state._cam_messages.size();
                    }
                    else {
                        return 0;
                    }
                }
                case vars::AVERAGE_DENM_MESSAGE_END_DELAYS_NOCELL: {
                    double avg_delay = 0;
                    if (cam_state._cam_messages.size() > 0) {
                        for (const auto &message: cam_state._cam_messages) {
                            avg_delay += message.end_delay;
                        }
                        return avg_delay / cam_state._cam_messages.size();
                    }
                    else {
                        return 0;
                    }
                }
                case vars::CAM_MESSAGE_IDS: {
                    std::vector <std::string> message_ids;

                    for (const auto &message: cam_state._cam_messages) {
                        message_ids.push_back(message.message_id);
                    }
                    return message_ids;
                }
                default:
                    return {};
            }
        }

    private:

        std::vector <Coordinates> _coordinates;
        uint _link_number;
        std::vector <Cell> _cells;

    };

    template<class Types, class Parameters>
    class Antenna : public AntennaImplementation<Antenna<Types, Parameters>, Parameters, Types> {
    public:
        Antenna(const std::string &name,
                const artis::pdevs::Context<artis::common::DoubleTime, Antenna<Types, Parameters>, AntennaParameters> &context)
                : AntennaImplementation<Antenna<Types, Parameters>, AntennaParameters, Types>(name, context) {
        }

        ~Antenna() override = default;
    };
}

#endif //ARTIS_TRAFFIC_MICRO_5G_ANTENNA_HPP
