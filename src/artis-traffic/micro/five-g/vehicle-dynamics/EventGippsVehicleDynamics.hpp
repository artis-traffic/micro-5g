/**
 * @file artis-traffic/micro/five-g/EventGippsVehicleDynamics5G.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_EVENTGIPPSVEHICLEDYNAMICS_HPP
#define ARTIS_TRAFFIC_MICRO_5G_EVENTGIPPSVEHICLEDYNAMICS_HPP

#include <artis-traffic/micro/core/vehicle-dynamics/EventGippsVehicleDynamics.hpp>

namespace artis::traffic::micro::five_g {

template<class Vehicle>
class EventGippsVehicleDynamics : public core::EventGippsVehicleDynamics<Vehicle> {
public:
  EventGippsVehicleDynamics(const Vehicle &vehicle, const artis::traffic::core::Time &next_time, double link_length,
                            double speed_limit, bool is_first) : core::EventGippsVehicleDynamics<Vehicle>(vehicle,
                                                                                                          next_time,
                                                                                                          link_length,
                                                                                                          speed_limit,
                                                                                                          is_first) {}

  ~EventGippsVehicleDynamics() override = default;

private:

};

}

#endif //ARTIS_TRAFFIC_MICRO_5G_EVENTGIPPSVEHICLEDYNAMICS_HPP
