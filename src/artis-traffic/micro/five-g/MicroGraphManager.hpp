/**
 * @file artis-traffic/micro/five_g/MicroGraphManager.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_MICRO_GRAPH_MANAGER_HPP
#define ARTIS_TRAFFIC_MICRO_5G_MICRO_GRAPH_MANAGER_HPP

#include <artis-traffic/micro/five-g/Antenna.hpp>
#include <artis-traffic/micro/five-g/Link.hpp>
#include <artis-traffic/micro/five-g/Network.hpp>

#include <artis-traffic/micro/utils/MicroGraphManager.hpp>
#include <artis-traffic/micro/utils/Network.hpp>

#include "VehicleFactory.hpp"

namespace artis::traffic::micro::five_g {

template<class Vehicle, class VehicleEntry, class VehicleState, class Link, class LinkParameters,
  class Stop, class StopParameters, class Generator, class GeneratorParameters,
  typename Time, typename Parameters = artis::common::NoParameters,
  typename GraphParameters = artis::traffic::micro::utils::Network<artis::traffic::micro::utils::InputData,
    artis::traffic::micro::utils::OutputData, artis::traffic::micro::utils::NodeData,
    artis::traffic::micro::utils::JunctionData, StopData,
    LinkData>>
class MicroGraphManager :
  public artis::traffic::micro::utils::MicroGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters, Stop,
    StopParameters, Generator, GeneratorParameters, Time, Parameters, GraphParameters> {
public:
  MicroGraphManager(artis::common::Coordinator<Time> *coordinator, const Parameters &parameters,
                    const GraphParameters &graph_parameters)
    : artis::traffic::micro::utils::MicroGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters, Stop,
    StopParameters, Generator, GeneratorParameters, Time, Parameters, GraphParameters>(coordinator, parameters,
                                                                                       graph_parameters) {}

//  void connect_link_to_antenna(artis::pdevs::Simulator<artis::common::DoubleTime, Link, LinkParameters> &link,
//                               artis::pdevs::Simulator<artis::common::DoubleTime, Antenna,
//                                 AntennaParameters> &antenna) {
//    this->out({&link, Link::outputs::ANTENNA_MESSAGE})
//      >> this->in({&antenna, artis::traffic::micro::five_g::Antenna::inputs::MESSAGE});
//  }
};

}

#endif //ARTIS_TRAFFIC_MICRO_5G_MICRO_GRAPH_MANAGER_HPP
