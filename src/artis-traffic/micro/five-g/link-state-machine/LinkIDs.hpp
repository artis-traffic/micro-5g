/**
 * @file artis-traffic/micro/five-g/link-state-machine/LinkIDs.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_LINK_IDS_HPP
#define ARTIS_TRAFFIC_MICRO_5G_LINK_IDS_HPP

#include <artis-traffic/micro/core/link-state-machine/LinkIDs.hpp>

namespace artis::traffic::micro::five_g {

struct StateMachineIDs : core::StateMachineIDs {
  enum values {
    MESSAGE = core::StateMachineIDs::LAST + 1, NEW_MACHINE_2
  };
};

struct StateIDs : core::StateIDs {
  enum values {
      WAIT_MESSAGE = core::StateIDs::LAST + 1
  };
};

struct EventIDs : core::EventIDs {
  enum values {
      SEND_MESSAGE = core::EventIDs::LAST + 1, IN_VEHICLE_MESSAGE, OUT_VEHICLE, EVENT_MESSAGE, EMPTY,
      SEND_CAM_SIGNAL_MESSAGE, SEND_CAM_MESSAGE, CAM_SIGNAL, GET_DENM, GET_CAM_MESSAGE,
      DENM_SIGNAL, SEND_DENM_SIGNAL_MESSAGE, SEND_DENM_MESSAGE, GET_VEHICLE_LIST, SEND_VEHICLE_LIST,
      GET_MEASURE_MESSAGE
  };
};

}

#endif //ARTIS_TRAFFIC_MICRO_5G_LINK_STATE_MACHINE_HPP
