/**
 * @file artis-traffic/micro/five-g/link-state-machine/LinkStates.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_LINK_STATES_HPP
#define ARTIS_TRAFFIC_MICRO_5G_LINK_STATES_HPP

#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/micro/five-g/Data.hpp>

#include <map>
#include <vector>

namespace artis::traffic::micro::five_g {

    template<class Types>
struct MessageState {
  std::vector<typename Types::vehicle_type> _vehicles;
  uint _messages_count = 0;
  std::vector<DENMMessage> _denm_messages;
  std::vector<CAMMessage> _cam_messages;
};

}

#endif //ARTIS_TRAFFIC_MICRO_5G_LINK_STATES_HPP
