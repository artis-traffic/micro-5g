/**
 * @file artis-traffic/micro/five-g/link-state-machine/OutputStateMachine.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_LINK_STATE_MACHINE_OUTPUT_STATE_MACHINE_HPP
#define ARTIS_TRAFFIC_MICRO_5G_LINK_STATE_MACHINE_OUTPUT_STATE_MACHINE_HPP

#include <artis-traffic/micro/core/link-state-machine/OutputStateMachine.hpp>

#include <iostream>

namespace artis::traffic::micro::five_g {

template<class Types, class Parameters, class StateType>
struct OutputT1 : core::OutputT1<Types, Parameters, StateType> {
  OutputT1(const std::shared_ptr<core::OutputStateMachine<Types, Parameters, StateType>> &machine) :
    core::OutputT1<Types, Parameters, StateType>(machine) {}

  void action(const artis::traffic::core::Time & /* t */, const artis::common::event::Value &value) override {
    const auto &state = this->_machine->root().template state_<MessageState<Types>>(Types::state_machine_IDs_type::MESSAGE);

    value(this->_machine->state_()._vehicle);

      for (auto &vehicle: state._vehicles) {
          if (vehicle.index() == this->_machine->state_()._vehicle.index()) {
              this->_machine->state_()._vehicle.data().next_fiveg_time = vehicle.data().next_fiveg_time;
              this->_machine->state_()._vehicle.data().next_cam_time = vehicle.data().next_cam_time;
          }
      }
  }

  typename core::OutputStateMachine<Types, Parameters, StateType>::Events
  output(const artis::traffic::core::Time & /* t */) const override {
    typename core::OutputStateMachine<Types, Parameters, StateType>::Events events{};

    events.internals.push_back(typename Types::state_machine_type::InternalEvent{
      {Types::event_IDs_type::OUT_VEHICLE, this->_machine->state_()._vehicle},
      Types::state_machine_IDs_type::MESSAGE});

    events.externals.push_back(typename Types::state_machine_type::ExternalEvent{Types::event_IDs_type::SEND_VEHICLE,
                                                                                 this->_machine->state_()._vehicle});
    return events;
  }
};

}

#endif