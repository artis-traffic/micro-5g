/**
 * @file artis-traffic/micro/five-g/link-state-machine/ProcessStateMachine.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_PROCESSSTATEMACHINE_HPP
#define ARTIS_TRAFFIC_MICRO_5G_PROCESSSTATEMACHINE_HPP

#include <artis-traffic/micro/core/link-state-machine/ProcessStateMachine.hpp>

namespace artis::traffic::micro::five_g {
    template<class Types, class Parameters, class StateType>
    struct ProcessT2 : core::ProcessT2<Types, Parameters, StateType> {
        ProcessT2(const std::shared_ptr<core::ProcessStateMachine<Types, Parameters, StateType>> &machine) :
                core::ProcessT2<Types, Parameters, StateType>(machine) {}

        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
            core::ProcessT2<Types, Parameters, StateType>::action(t, value);


        }

        typename core::ProcessStateMachine<Types, Parameters, StateType>::Events
        output(const artis::traffic::core::Time & t) const override {
            typename core::ProcessStateMachine<Types, Parameters, StateType>::Events events{core::ProcessT2<Types, Parameters, StateType>::output(t)};

            std::vector<uint> waked_indexes;
            std::vector<typename std::deque<typename Types::vehicle_entry_type>::iterator> waked_vehicles = this->_machine->waked_vehicles(
                    t);
            if (!waked_vehicles.empty()) {
                for (const auto &it: waked_vehicles) {
                    waked_indexes.push_back(it->vehicle().index());
                }

//                std::cout << "new output ProcessT2 " << waked_vehicles.size() << std::endl;
                events.internals.push_back(
                        typename core::ProcessStateMachine<Types, Parameters, StateType>::InternalEvent{
                                {Types::event_IDs_type::EVENT_MESSAGE, waked_indexes},
                                Types::state_machine_IDs_type::MESSAGE});
            }
            return events;
        }


    };
}
#endif //ARTIS_TRAFFIC_MICRO_5G_PROCESSSTATEMACHINE_HPP
