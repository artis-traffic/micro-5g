/**
 * @file artis-traffic/micro/five-g/link-state-machine/LinkRootStateMachine.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_LINK_ROOT_STATE_MACHINE_HPP
#define ARTIS_TRAFFIC_MICRO_5G_LINK_ROOT_STATE_MACHINE_HPP

#include <artis-traffic/micro/core/link-state-machine/LinkRootStateMachine.hpp>

#include <artis-traffic/micro/five-g/link-state-machine/LinkIDs.hpp>
#include <artis-traffic/micro/five-g/link-state-machine/LinkStateMachine.hpp>
#include <artis-traffic/micro/five-g/link-state-machine/LinkStates.hpp>

#include <artis-traffic/micro/core/link-state-machine/AnswerStateStateMachine.hpp>
#include <artis-traffic/micro/core/link-state-machine/DownOpenCloseStateMachine.hpp>
#include <artis-traffic/micro/core/link-state-machine/InputStateMachine.hpp>
#include <artis-traffic/micro/core/link-state-machine/OpenCloseStateMachine.hpp>
#include <artis-traffic/micro/core/link-state-machine/OutputStateMachine.hpp>
#include <artis-traffic/micro/core/link-state-machine/ProcessStateMachine.hpp>
#include <artis-traffic/micro/core/link-state-machine/StateStateMachine.hpp>
#include <artis-traffic/micro/core/link-state-machine/LinkStates.hpp>

#include <artis-traffic/micro/five-g/link-state-machine/MessageStateMachine.hpp>
#include <artis-traffic/micro/five-g/link-state-machine/ProcessStateMachine.hpp>
#include <artis-traffic/micro/five-g/link-state-machine/OutputStateMachine.hpp>

namespace artis::traffic::micro::five_g {

template<class Types, class Parameters>
class LinkRootStateMachine
  : public core::LinkRootStateMachine<Types, Parameters> {
public:
  LinkRootStateMachine() : core::LinkRootStateMachine<Types, Parameters>() {}

  ~LinkRootStateMachine() override = default;

  void build(const Parameters &parameters) override {
    core::LinkRootStateMachine<Types, Parameters>::build(parameters);
    this->template build_machine<typename Types::root_state_machine_type, MessageStateMachine<Types, Parameters, MessageState<Types>>>(
      Types::state_machine_IDs_type::MESSAGE, parameters);
    this->machine(Types::state_machine_IDs_type::OUTPUT)->switch_transition(
      Types::state_IDs_type::WAIT_READY_TO_EXIT, Types::state_IDs_type::WAIT_READY_TO_EXIT,
      0, new OutputT1<Types, Parameters, core::OutputState<Types>>(
        std::dynamic_pointer_cast<core::OutputStateMachine<Types, Parameters, core::OutputState<Types>>>(
          this->machine(Types::state_machine_IDs_type::OUTPUT))));

      this->machine(Types::state_machine_IDs_type::PROCESS)->switch_transition(
              Types::state_IDs_type::PROCESS_VEHICLES, Types::state_IDs_type::PROCESS_VEHICLES,
              1, new ProcessT2<Types, Parameters, core::ProcessState<Types>>(
              std::dynamic_pointer_cast<core::ProcessStateMachine<Types, Parameters, core::ProcessState<Types>>>(
              this->machine(Types::state_machine_IDs_type::PROCESS))));
  }
};

}

#endif //ARTIS_TRAFFIC_MICRO_5G_LINK_ROOT_STATE_MACHINE_HPP
