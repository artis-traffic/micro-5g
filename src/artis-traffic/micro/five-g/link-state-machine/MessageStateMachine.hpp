/**
 * @file artis-traffic/micro/five-g/link-state-machine/MessageStateMachine.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_MESSAGE_STATE_MACHINE_HPP
#define ARTIS_TRAFFIC_MICRO_5G_MESSAGE_STATE_MACHINE_HPP

#include <artis-traffic/micro/five-g/link-state-machine/LinkIDs.hpp>
#include <artis-traffic/micro/five-g/link-state-machine/LinkStateMachine.hpp>
#include <artis-traffic/micro/five-g/link-state-machine/LinkStates.hpp>

#include <artis-traffic/micro/five-g/Data.hpp>

#include <artis-star-addons/utils/StateMachine.hpp>

#include <iostream>

namespace artis::traffic::micro::five_g {

template<class Types, class Parameters, class StateType>
class MessageStateMachine : public artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType> {
public:
  MessageStateMachine(const typename Types::root_state_machine_type &root, const Parameters &parameters)
    : artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType>(root, {}),
      _coordinates(parameters.coordinates), _length(parameters.length) {}

  void build(const std::shared_ptr<MessageStateMachine<Types, Parameters, StateType>> &machine);

  Coordinates compute_coordinates(const artis::traffic::core::Time & t, typename Types::vehicle_type vehicle) {
      Coordinates position{-1, -1};
      double total_length = 0;

      const auto &state = this->root().template state_<core::ProcessState<Types>>(
              Types::state_machine_IDs_type::PROCESS);
      const auto &vecs = state._vehicles;
      auto it = std::find_if(vecs.begin(), vecs.end(),
                             [&](const typename Types::vehicle_entry_type &v) {
                                 return (v.vehicle().index() == vehicle.index());
                             });
      double vehicle_link_position = 0;
      if (it != vecs.end()) {
          vehicle_link_position = it->compute_position(t);
      }
      else {
          vehicle_link_position = _length;
      }

      for (unsigned int i = 0; i < _coordinates.size() - 1; i++) {
          Coordinates c1 = _coordinates[i];
          Coordinates c2 = _coordinates[i + 1];
          double d = sqrt(pow(c2.x - c1.x, 2) + pow(c2.y - c1.y, 2));

          if (vehicle_link_position <= total_length + d) {
              double dt = vehicle_link_position - total_length;
              double ti = dt / d;

              position = {(1 - ti) * c1.x + ti * c2.x, (1 - ti) * c1.y + ti * c2.y};
              break;
          }
          total_length += d;
      }
      if (position.x == -1 and position.y == -1) {
          position = _coordinates[_coordinates.size() - 1];
      }

      return position;
  }

// types
  DECLARE_STATE_TRANSITION_TYPES(MessageStateMachine)

  const std::vector<Coordinates > _coordinates;
  double _length;
};

template<class Types, class Parameters, class StateType>
struct WaitMessageState : MessageStateMachine<Types, Parameters, StateType>::State_t {
  WaitMessageState(const std::shared_ptr<MessageStateMachine<Types, Parameters, StateType>> &machine) :
    MessageStateMachine<Types, Parameters, StateType>::State_t(StateIDs::WAIT_MESSAGE, machine) {}

  artis::traffic::core::Time ta(const artis::traffic::core::Time &t) const override {
    auto &vehicles = this->_machine->state_()._vehicles;

    if (vehicles.empty()) {
      return artis::common::DoubleTime::infinity;
    } else {

//      return std::min_element(vehicles.begin(), vehicles.end(),
//                              [](const auto &l, const auto &r) { return l.data().next_fiveg_time < r.data().next_fiveg_time; }
//      )->data().next_fiveg_time - t;

        return std::min(std::min_element(vehicles.begin(), vehicles.end(),
                                [](const auto &l, const auto &r) { return l.data().next_fiveg_time < r.data().next_fiveg_time; }
        )->data().next_fiveg_time - t,
                        std::min_element(vehicles.begin(), vehicles.end(),
                                         [](const auto &l, const auto &r) { return l.data().next_cam_time < r.data().next_cam_time; }
                        )->data().next_cam_time - t
        );
    }
  }

};

template<class Types, class Parameters, class StateType>
struct MessageTransitionNewVehicle : MessageStateMachine<Types, Parameters, StateType>::Transition_t {
  MessageTransitionNewVehicle(const std::shared_ptr<MessageStateMachine<Types, Parameters, StateType>> &machine) :
    MessageStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  typename Types::vehicle_type vehicle;
  DENMSignalMessage message;

  void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
    value(vehicle);
    this->_machine->state_()._vehicles.push_back(vehicle);

    Coordinates position = this->_machine->compute_coordinates(t, vehicle);

    this->_machine->state_()._messages_count++;
    Data message_data = Data{"", vehicle.index(), position,
                                       vehicle.data().serving_cell};
    message = DENMSignalMessage{"", 0, t, false, true, t, -1, 0,
                          vehicle.data().message_count, t, message_data, true};
  }

  bool event(const artis::traffic::core::Time & /* t */, int event) override {
    return event == Types::event_IDs_type::IN_VEHICLE_MESSAGE;
  }

    typename MessageStateMachine<Types, Parameters, StateType>::Events
    output(const artis::traffic::core::Time & t) const override {
        typename MessageStateMachine<Types, Parameters, StateType>::Events events{};

        events.externals.push_back(
                typename MessageStateMachine<Types, Parameters, StateType>::ExternalEvent{
                        Types::event_IDs_type::SEND_DENM_SIGNAL_MESSAGE, message});

        return events;
    }
};

template<class Types, class Parameters, class StateType>
struct MessageTransitionSendMessage : MessageStateMachine<Types, Parameters, StateType>::Transition_t {
  MessageTransitionSendMessage(const std::shared_ptr<MessageStateMachine<Types, Parameters, StateType>> &machine) :
    MessageStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  std::deque<typename Types::vehicle_type> waked_vehicles;

  void action(const artis::traffic::core::Time &t, const artis::common::event::Value & /* value */) override {
    waked_vehicles.clear();
    for (auto &vehicle: this->_machine->state_()._vehicles) {
      if (std::abs(t - vehicle.data().next_fiveg_time) < 1e-6) {
        waked_vehicles.push_back(vehicle);
        vehicle.data().next_fiveg_time += vehicle.data().fiveg_timestep;
        if (vehicle.data().next_fiveg_time - std::ceil(vehicle.data().next_fiveg_time * 1e6) / 1e6 < 1e-6) {
            vehicle.data().next_fiveg_time = std::ceil(vehicle.data().next_fiveg_time * 1e6) / 1e6;
        }
        vehicle.data().message_count++;
      }
      if(std::abs(t - vehicle.data().next_cam_time) < 1e-6) {
          waked_vehicles.push_back(vehicle);
          vehicle.data().next_cam_time += vehicle.data().cam_timestep;
          if (vehicle.data().next_cam_time - std::ceil(vehicle.data().next_cam_time * 1e6) / 1e6 < 1e-6) {
              vehicle.data().next_cam_time = std::ceil(vehicle.data().next_cam_time * 1e6) / 1e6;
          }
          vehicle.data().message_count++;
      }
    }
  }

  bool no_event() const override { return true; }

  typename MessageStateMachine<Types, Parameters, StateType>::Events
  output(const artis::traffic::core::Time & t) const override {
    typename MessageStateMachine<Types, Parameters, StateType>::Events events{};

    for (const auto &vehicle: waked_vehicles) {
      Coordinates position = this->_machine->compute_coordinates(t, vehicle);

      if (std::abs(t - vehicle.data().next_fiveg_time) < 1e-6) {
          this->_machine->state_()._messages_count++;
          Data message_data = Data{"", vehicle.index(), position,
                                                             vehicle.data().serving_cell};
          MeasureReportMessage message = MeasureReportMessage{"", 0, t, false, false, t, -1, 0,
                                                              vehicle.data().message_count, t, message_data, {}};
          events.externals.push_back(
                  typename MessageStateMachine<Types, Parameters, StateType>::ExternalEvent{
                          Types::event_IDs_type::SEND_MESSAGE, message});
      }
      if(std::abs(t - vehicle.data().next_cam_time) < 1e-6) {
          this->_machine->state_()._messages_count++;
          Data message_data = Data{"", vehicle.index(), position,
                                                             vehicle.data().serving_cell};
          CAMSignalMessage message = CAMSignalMessage{"", 0, t, false, false, t, -1, 0,
                                                              vehicle.data().message_count, t, message_data};
          events.externals.push_back(
                  typename MessageStateMachine<Types, Parameters, StateType>::ExternalEvent{
                          Types::event_IDs_type::SEND_CAM_SIGNAL_MESSAGE, message});
      }

    }
    return events;
  }
};

template<class Types, class Parameters, class StateType>
    struct MessageTransitionSendCAM : MessageStateMachine<Types, Parameters, StateType>::Transition_t {
        MessageTransitionSendCAM(const std::shared_ptr<MessageStateMachine<Types, Parameters, StateType>> &machine) :
                MessageStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        CAMSignalMessage signal_message;
        CAMMessage message;

        void action(const artis::traffic::core::Time &t, const artis::common::event::Value & value) override {
            value(signal_message);
            message = CAMMessage(signal_message);

        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::CAM_SIGNAL;
        }

        typename MessageStateMachine<Types, Parameters, StateType>::Events
        output(const artis::traffic::core::Time & t) const override {
            typename MessageStateMachine<Types, Parameters, StateType>::Events events{};

            events.externals.push_back(
                    typename MessageStateMachine<Types, Parameters, StateType>::ExternalEvent{
                        Types::event_IDs_type::SEND_CAM_MESSAGE, message});

            return events;
        }
    };

    template<class Types, class Parameters, class StateType>
    struct MessageTransitionSendDENM : MessageStateMachine<Types, Parameters, StateType>::Transition_t {
        MessageTransitionSendDENM(const std::shared_ptr<MessageStateMachine<Types, Parameters, StateType>> &machine) :
                MessageStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        DENMSignalMessage signal_message;
        DENMMessage message;

        void action(const artis::traffic::core::Time &t, const artis::common::event::Value & value) override {
            value(signal_message);
            message = DENMMessage(signal_message);

        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::DENM_SIGNAL;
        }

        typename MessageStateMachine<Types, Parameters, StateType>::Events
        output(const artis::traffic::core::Time & t) const override {
            typename MessageStateMachine<Types, Parameters, StateType>::Events events{};

            events.externals.push_back(
                    typename MessageStateMachine<Types, Parameters, StateType>::ExternalEvent{
                            Types::event_IDs_type::SEND_DENM_MESSAGE, message});

            return events;
        }
    };

template<class Types, class Parameters, class StateType>
struct MessageTransitionVehicleOut : MessageStateMachine<Types, Parameters, StateType>::Transition_t {
  MessageTransitionVehicleOut(const std::shared_ptr<MessageStateMachine<Types, Parameters, StateType>> &machine) :
    MessageStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  uint vehicle_index;
  typename Types::vehicle_type out_vehicle;
  DENMMessage message;

  void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
    value(out_vehicle);
      for (auto &vehicle: this->_machine->state_()._vehicles) {
          if (out_vehicle.index() == vehicle.index()) {
              Coordinates position = this->_machine->compute_coordinates(t, vehicle);

              this->_machine->state_()._messages_count++;
              Data message_data = Data{"", vehicle.index(), position,
                                               vehicle.data().serving_cell};
              message = DENMMessage{"", 0, t, false, true, t, -1, 0,
                                                vehicle.data().message_count, t, message_data, false, 1};

              this->_machine->state_()._vehicles.erase(std::remove
              (this->_machine->state_()._vehicles.begin(),
               this->_machine->state_()._vehicles.end(), vehicle), this->_machine->state_()._vehicles.end());
          }
      }
  }

  bool event(const artis::traffic::core::Time & /* t */, int event) override {
    return event == Types::event_IDs_type::OUT_VEHICLE;
  }

    typename MessageStateMachine<Types, Parameters, StateType>::Events
    output(const artis::traffic::core::Time & t) const override {
        typename MessageStateMachine<Types, Parameters, StateType>::Events events{};

        events.externals.push_back(
                typename MessageStateMachine<Types, Parameters, StateType>::ExternalEvent{
                    Types::event_IDs_type::SEND_DENM_MESSAGE, message});

        return events;
    }
};

    template<class Types, class Parameters, class StateType>
    struct MessageTransitionEventMessage : MessageStateMachine<Types, Parameters, StateType>::Transition_t {
        MessageTransitionEventMessage(const std::shared_ptr<MessageStateMachine<Types, Parameters, StateType>> &machine) :
                MessageStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        std::vector<uint> waked_indexes;
        std::deque<typename Types::vehicle_type> waked_vehicles;

        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
            value(waked_indexes);
            waked_vehicles.clear();
            for (auto &index : waked_indexes) {
                for (auto &vehicle: this->_machine->state_()._vehicles) {
                    if (vehicle.index() == index) {
                        vehicle.data().next_fiveg_time = t;
                    }
                }
            }

            for (auto &vehicle: this->_machine->state_()._vehicles) {
                if (std::abs(t - vehicle.data().next_fiveg_time) < 1e-6) {
                    waked_vehicles.push_back(vehicle);
                    vehicle.data().next_fiveg_time += vehicle.data().fiveg_timestep;
                    if (vehicle.data().next_fiveg_time - std::ceil(vehicle.data().next_fiveg_time * 1e6) / 1e6 < 1e-6) {
                        vehicle.data().next_fiveg_time = std::ceil(vehicle.data().next_fiveg_time * 1e6) / 1e6;
                    }
                    vehicle.data().message_count++;
                }
            }

        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::EVENT_MESSAGE;
        }

        typename MessageStateMachine<Types, Parameters, StateType>::Events
        output(const artis::traffic::core::Time & t) const override {
            typename MessageStateMachine<Types, Parameters, StateType>::Events events{};

            for (const auto &vehicle: waked_vehicles) {
                Coordinates position = this->_machine->compute_coordinates(t, vehicle);

                if (std::abs(t - vehicle.data().next_fiveg_time) < 1e-6) {
                    this->_machine->state_()._messages_count++;
                    Data message_data = Data{"", vehicle.index(), position,
                                                                       vehicle.data().serving_cell};
                    MeasureReportMessage message = MeasureReportMessage{"", 0, t, false, true, t, -1, 0,
                                                                        vehicle.data().message_count, t, message_data, {}};
                    events.externals.push_back(
                            typename MessageStateMachine<Types, Parameters, StateType>::ExternalEvent{
                                    Types::event_IDs_type::SEND_MESSAGE, message});
                }
                if(std::abs(t - vehicle.data().next_cam_time) < 1e-6) {
                    this->_machine->state_()._messages_count++;
                    Data message_data = Data{"", vehicle.index(), position,
                                                                       vehicle.data().serving_cell};
                    CAMSignalMessage message = CAMSignalMessage{"", 0, t, false, true, t, -1, 0,
                                                                        vehicle.data().message_count, t, message_data};
                    events.externals.push_back(
                            typename MessageStateMachine<Types, Parameters, StateType>::ExternalEvent{
                                    Types::event_IDs_type::SEND_CAM_SIGNAL_MESSAGE, message});
                }
            }
            return events;
        }
    };

    template<class Types, class Parameters, class StateType>
    struct MessageTransitionGetDENM : MessageStateMachine<Types, Parameters, StateType>::Transition_t {
        MessageTransitionGetDENM(const std::shared_ptr<MessageStateMachine<Types, Parameters, StateType>> &machine) :
                MessageStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        DENMMessage message;
        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
            value(message);
            message.end_delay = t - message.end_delay;
            this->_machine->state_()._denm_messages.push_back(message);
        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::GET_DENM;
        }

    };

    template<class Types, class Parameters, class StateType>
    struct MessageTransitionGetCAMMessage : MessageStateMachine<Types, Parameters, StateType>::Transition_t {
        MessageTransitionGetCAMMessage(const std::shared_ptr<MessageStateMachine<Types, Parameters, StateType>> &machine) :
                MessageStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        CAMMessage message;
        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
            value(message);
            message.end_delay = t - message.end_delay;
            this->_machine->state_()._cam_messages.push_back(message);
        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::GET_CAM_MESSAGE;
        }

    };

    template<class Types, class Parameters, class StateType>
    struct MessageTransitionSendVehicleList : MessageStateMachine<Types, Parameters, StateType>::Transition_t {
        MessageTransitionSendVehicleList(const std::shared_ptr<MessageStateMachine<Types, Parameters, StateType>> &machine) :
                MessageStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        std::vector<typename Types::vehicle_type> vehicles_list;

        void action(const artis::traffic::core::Time &t, const artis::common::event::Value & value) override {
            vehicles_list.clear();
//            const auto &state = this->_machine->root().template state_<core::ProcessState<Types>>(
//                    Types::state_machine_IDs_type::PROCESS);
//            const auto &vecs = state._vehicles;
//            for (auto entry : vecs) {
//                vehicles_list.push_back(entry.vehicle().index());
//            }
            vehicles_list = this->_machine->state_()._vehicles;

        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::GET_VEHICLE_LIST;
        }

        typename MessageStateMachine<Types, Parameters, StateType>::Events
        output(const artis::traffic::core::Time & t) const override {
            typename MessageStateMachine<Types, Parameters, StateType>::Events events{};

            events.externals.push_back(
                    typename MessageStateMachine<Types, Parameters, StateType>::ExternalEvent{
                            Types::event_IDs_type::SEND_VEHICLE_LIST, vehicles_list});

            return events;
        }
    };

    template<class Types, class Parameters, class StateType>
    struct MessageTransitionGetMeasureMessage : MessageStateMachine<Types, Parameters, StateType>::Transition_t {
        MessageTransitionGetMeasureMessage(const std::shared_ptr<MessageStateMachine<Types, Parameters, StateType>> &machine) :
                MessageStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        MeasureReportMessage message;
        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
            value(message);
            for (auto &vehicle: this->_machine->state_()._vehicles) {
                if (message.data.vehicle_index == vehicle.index()) {
                    vehicle.data().serving_cell = message.data.serving_cell;
                }
            }
        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::GET_MEASURE_MESSAGE;
        }

    };
    
    template<class Types, class Parameters, class StateType>
    struct MessageTransitionEmpty : MessageStateMachine<Types, Parameters, StateType>::Transition_t {
        MessageTransitionEmpty(const std::shared_ptr<MessageStateMachine<Types, Parameters, StateType>> &machine) :
                MessageStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}


        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::EMPTY;
        }

    };

template<class Types, class Parameters, class StateType>
void
MessageStateMachine<Types, Parameters, StateType>::build(
  const std::shared_ptr<MessageStateMachine<Types, Parameters, StateType>> &machine) {
  this->state(new WaitMessageState(machine));
  this->initial_state(StateIDs::WAIT_MESSAGE);
  this->transition(StateIDs::WAIT_MESSAGE, StateIDs::WAIT_MESSAGE, new MessageTransitionNewVehicle(machine));
  this->transition(StateIDs::WAIT_MESSAGE, StateIDs::WAIT_MESSAGE, new MessageTransitionSendMessage(machine));
  this->transition(StateIDs::WAIT_MESSAGE, StateIDs::WAIT_MESSAGE, new MessageTransitionSendCAM(machine));
  this->transition(StateIDs::WAIT_MESSAGE, StateIDs::WAIT_MESSAGE, new MessageTransitionSendDENM(machine));
  this->transition(StateIDs::WAIT_MESSAGE, StateIDs::WAIT_MESSAGE, new MessageTransitionVehicleOut(machine));
  this->transition(StateIDs::WAIT_MESSAGE, StateIDs::WAIT_MESSAGE, new MessageTransitionEventMessage(machine));
  this->transition(StateIDs::WAIT_MESSAGE, StateIDs::WAIT_MESSAGE, new MessageTransitionEmpty(machine));
  this->transition(StateIDs::WAIT_MESSAGE, StateIDs::WAIT_MESSAGE, new MessageTransitionGetDENM(machine));
  this->transition(StateIDs::WAIT_MESSAGE, StateIDs::WAIT_MESSAGE, new MessageTransitionGetCAMMessage(machine));
  this->transition(StateIDs::WAIT_MESSAGE, StateIDs::WAIT_MESSAGE, new MessageTransitionSendVehicleList(machine));
  this->transition(StateIDs::WAIT_MESSAGE, StateIDs::WAIT_MESSAGE, new MessageTransitionGetMeasureMessage(machine));
}

}
#endif //ARTIS_TRAFFIC_MICRO_5G_MESSAGE_STATE_MACHINE_HPP
