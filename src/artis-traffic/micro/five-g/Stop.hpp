/**
 * @file artis-traffic/micro/five-g/Stop.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_STOP_HPP
#define ARTIS_TRAFFIC_MICRO_5G_STOP_HPP

#include <artis-traffic/micro/core/Stop.hpp>

#include <artis-traffic/micro/five-g/Data.hpp>
#include <artis-traffic/micro/five-g/Vehicle.hpp>

#include <artis-traffic/micro/five-g/stop-state-machine/StopIDs.hpp>
#include <artis-traffic/micro/five-g/stop-state-machine/StopTypes.hpp>
#include <artis-traffic/micro/five-g/stop-state-machine/StopRootStateMachine.hpp>

namespace artis::traffic::micro::five_g {

    struct StopParameters : artis::traffic::micro::core::StopParameters {
        std::vector<Coordinates> coordinates;
    };

    template<class Model, class Parameters, class Types>
    class StopImplementation : public core::StopImplementation<Model, Parameters, Types> {
    public:
        struct inputs : core::StopImplementation<Model, Parameters, Types>::inputs {
            enum values {
                ANTENNA_MESSAGE = core::StopImplementation<Model, Parameters, Types>::inputs::LAST + 500,
                CAM_SIGNAL = ANTENNA_MESSAGE + 500, CAM_MESSAGE = CAM_SIGNAL + 500,
                DENM_SIGNAL = CAM_MESSAGE + 500, DENM_MESSAGE = DENM_SIGNAL + 500,
            };
        };

        struct outputs : core::StopImplementation<Model, Parameters, Types>::outputs {
            enum values {
                ANTENNA_MESSAGE = core::StopImplementation<Model, Parameters, Types>::outputs::LAST + 500,
                CAM_SIGNAL = ANTENNA_MESSAGE + 500, CAM_MESSAGE = CAM_SIGNAL + 500,
                DENM_SIGNAL = CAM_MESSAGE + 500, DENM_MESSAGE = DENM_SIGNAL + 500
            };
        };

        struct vars : core::StopImplementation<Model, Parameters, Types>::vars {
            enum values {
                MEASURE_REPORTS_SERVING_CELL = core::StopImplementation<Model, Parameters, Types>::vars::LAST + 1,
                LOST_MESSAGES_COUNT, MESSAGES_COUNT
            };
        };

        StopImplementation(const std::string &name,
                           const artis::pdevs::Context<artis::common::DoubleTime, Model, Parameters> &context) :
                core::StopImplementation<Model, Parameters, Types>(name, context) {
            _messages_count = 0;
            _lost_messages_count = 0;
            // new input/output port
            this->input_port({inputs::ANTENNA_MESSAGE, "antenna_message"});
            this->input_port({inputs::CAM_SIGNAL, "cam_signal"});
            this->input_port({inputs::CAM_MESSAGE, "cam_message"});
            this->input_port({inputs::DENM_SIGNAL, "denm_signal"});
            this->input_port({inputs::DENM_MESSAGE, "denm_message"});

            this->output_port({outputs::ANTENNA_MESSAGE, "antenna_message"});
            this->output_port({outputs::CAM_SIGNAL, "cam_signal"});
            this->output_port({outputs::CAM_MESSAGE, "cam_message"});
            this->output_port({outputs::DENM_SIGNAL, "denm_signal"});
            this->output_port({outputs::DENM_MESSAGE, "denm_message"});

            // new observables
            this->observables({{vars::MEASURE_REPORTS_SERVING_CELL, "measure_reports_serving_cell"}});
            this->observables({{vars::LOST_MESSAGES_COUNT, "lost_messages_count"}});
            this->observables({{vars::MESSAGES_COUNT, "messages_count"}});
        }

        ~StopImplementation() override = default;

        void dext(const artis::traffic::core::Time &t, const artis::traffic::core::Time &e,
                  const artis::traffic::core::Bag &bag) override {
            super::dext(t, e, bag);
            std::for_each(bag.begin(), bag.end(),
                          [t, this](const artis::traffic::core::ExternalEvent &event) {
                              if (event.on_port(inputs::IN)) {
                                  auto state = this->root().template state_<micro::core::ProcessVehicleState<Types>>(Types::state_machine_IDs_type::PROCESS_VEHICLE);
                                  typename Types::vehicle_type vehicle{};

                                  event.data()(vehicle);

//                    std::cout << get_full_name() << " => ADD VEHICLE = " << vehicle.to_string() << " t=" << t << std::endl;

                                  if (vehicle.data().next_fiveg_time - t < 1e-6) {
                                      vehicle.data().next_fiveg_time = t;
                                  }
                                  this->root().transition(t, StopStateMachineIDs::MESSAGE,
                                                          typename Types::state_machine_type::ExternalEvent{
                                                                  Types::event_IDs_type::IN_VEHICLE_MESSAGE,
                                                                  vehicle});
                              }
                              else if (event.on_port(inputs::ANTENNA_MESSAGE)) {
                                  MeasureReportMessage message;
                                  event.data()(message);

//                        std::cout<<"LINK RECEIVED MESSAGE "<<message.data.measure_id<<" - "<<
//                        message.to_string()<<std::endl;
                                  _measure_messages.push_back(message);

//                        std::cout << this->get_full_name() << " => LINK IN at " << t << " - " << message.to_string()
//                                  << std::endl;

                                  auto state = this->root().template state_<micro::core::ProcessVehicleState<Types>>(Types::state_machine_IDs_type::PROCESS_VEHICLE);

                                  if(state._vehicle.index() != message.data.vehicle_index) {
                                      _lost_messages_count++;
                                  }
                                  _messages_count++;
                                  this->root().transition(t, StopStateMachineIDs::MESSAGE,
                                                          typename Types::state_machine_type::ExternalEvent{
                                                                  Types::event_IDs_type::EMPTY});
                              }
                              else if (event.on_port(inputs::CAM_SIGNAL)) {
                                  CAMSignalMessage message;
                                  event.data()(message);

//                                  std::cout<<"STOP RECEIVED CAMSIGNAL t "<<t<<" " <<message.message_id<<" - "<<
//                                           message.to_string()<<std::endl;

                                  this->root().transition(t, StopStateMachineIDs::MESSAGE,
                                                          typename Types::state_machine_type::ExternalEvent{
                                                                  Types::event_IDs_type::STOP_CAM_SIGNAL, message});
                              }
                              else if (event.on_port(inputs::DENM_MESSAGE)) {
                                  DENMMessage message;
                                  event.data()(message);

//                                  std::cout<<"STOP RECEIVED DENM t "<<t<<" " <<message.message_id<<" - "<<
//                                           message.to_string()<<std::endl;

                                  this->root().transition(t, StopStateMachineIDs::MESSAGE,
                                                          typename Types::state_machine_type::ExternalEvent{
                                                                  Types::event_IDs_type::STOP_GET_DENM, message});
                              }
                              else if (event.on_port(inputs::CAM_MESSAGE)) {
                                  CAMMessage message;
                                  event.data()(message);

//                                  std::cout<<"STOP RECEIVED CAM t "<<t<<" " <<message.message_id<<" - "<<
//                                           message.to_string()<<std::endl;

                                  //TODO transition et stocker
                                  this->root().transition(t, StopStateMachineIDs::MESSAGE,
                                                          typename Types::state_machine_type::ExternalEvent{
                                                                  Types::event_IDs_type::STOP_GET_CAM_MESSAGE, message});
                              }
                          });
        }

        void start(const artis::traffic::core::Time &t) override {
            super::start(t);
        }

        artis::traffic::core::Bag lambda(const artis::traffic::core::Time &t) const override {
            artis::traffic::core::Bag bag = super::lambda(t);
            const typename Types::root_state_machine_type::external_events_type &events = this->root().outbox();

//  std::cout << "[" << this->get_full_name() << "] lambda at " << t << std::endl;

            std::for_each(events.cbegin(), events.cend(), [t, this, &bag](const auto &e) {
                switch (e.id) {
                    case Types::event_IDs_type::STOP_SEND_MESSAGE: {
                        MeasureReportMessage message;

                        e.data(message);

//                    std::cout << this->get_full_name() << " => STOP OUT at " << t << " - " << message.to_string()
//                    << std::endl;

                        if (message.data.serving_cell.cell_id == "fake") {
                            message.cell_change = true;
                        }
                        const auto &state = this->root().template state_<StopMessageState<Types>>(Types::state_machine_IDs_type::MESSAGE);
                        message.message_id = std::to_string(message.data.vehicle_index) + "/m/" + std::to_string(message.vehicle_message_count);
                        message.data.measure_id = std::to_string(message.data.vehicle_index) + "/r/" + std::to_string(message.vehicle_message_count);
                        bag.push_back(artis::traffic::core::ExternalEvent(outputs::ANTENNA_MESSAGE, message));
                        break;
                    }
                    case Types::event_IDs_type::STOP_SEND_CAM_SIGNAL_MESSAGE: {
                        CAMSignalMessage message;

                        e.data(message);

//                        std::cout << this->get_full_name() << " => STOP SEND CAMSIGNAL at " << t << " - " << message.to_string()
//                                  << std::endl;

                        if (message.data.serving_cell.cell_id == "fake") {
                            message.cell_change = true;
                        }
                        const auto &state = this->root().template state_<StopMessageState<Types>>(Types::state_machine_IDs_type::MESSAGE);
                        message.message_id = std::to_string(message.data.vehicle_index) + "/cams/" + std::to_string(message.vehicle_message_count);
                        message.data.measure_id = std::to_string(message.data.vehicle_index) + "/camd/" + std::to_string(message.vehicle_message_count);
                        bag.push_back(artis::traffic::core::ExternalEvent(outputs::CAM_SIGNAL, message));
                        break;
                    }
                    case Types::event_IDs_type::STOP_SEND_CAM_MESSAGE: {
                        CAMMessage message;

                        e.data(message);

//                        std::cout << this->get_full_name() << " => STOP SEND CAM at " << t << " - " << message.to_string()
//                                  << std::endl;

                        bag.push_back(artis::traffic::core::ExternalEvent(outputs::CAM_MESSAGE, message));
                        break;
                    }
                }
            });
            return bag;
        }

        artis::common::event::Value observe(const artis::traffic::core::Time &t,
                                            unsigned int index) const override {
            artis::common::event::Value value = super::observe(t, index);

            if (value.is_null()) {
                switch (index) {
                    case vars::MEASURE_REPORTS_SERVING_CELL: {
                        std::vector<Cell> measure_reports_info;

                        if (not _measure_messages.empty()) {
                            for (const auto &measure: _measure_messages) {
                                if (measure.next_time >= t - 1) {
                                    measure_reports_info.push_back(measure.data.serving_cell);
                                }
                            }
                        }
                        return measure_reports_info;
                    }
                    case vars::LOST_MESSAGES_COUNT: {
                        return _lost_messages_count;
                    }
                    case vars::MESSAGES_COUNT: {
                        return _messages_count;
                    }
                    default:
                        return {};
                }
            }
            return value;
        }

    private:
        using super = core::StopImplementation<Model, Parameters, Types>;
        std::vector<MeasureReportMessage> _measure_messages;
        uint _messages_count;
        uint _lost_messages_count;

    };

    template<class Types, class Parameters>
    class Stop : public StopImplementation<Stop<Types, Parameters>, Parameters, Types> {
    public:
        Stop(const std::string &name,
             const artis::pdevs::Context<artis::common::DoubleTime, Stop<Types, Parameters>, StopParameters> &context)
                : StopImplementation<Stop<Types, Parameters>, StopParameters, Types>(name, context) {
        }

        ~Stop() override = default;
    };

}
#endif //ARTIS_TRAFFIC_MICRO_5G_STOP_HPP
