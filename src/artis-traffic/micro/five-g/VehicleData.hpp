/**
 * @file artis-traffic/micro/five-g/VehicleData.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_VEHICLE_DATA_HPP
#define ARTIS_TRAFFIC_MICRO_5G_VEHICLE_DATA_HPP

#include <artis-traffic/micro/core/VehicleData.hpp>
#include <artis-traffic/core/Base.hpp>
#include "Data.hpp"

namespace artis::traffic::micro::five_g {

struct VehicleData : core::VehicleData {
  double fiveg_timestep;
  artis::traffic::core::Time next_fiveg_time;
  double cam_timestep;
  artis::traffic::core::Time next_cam_time;
  Cell serving_cell;
  std::vector<Cell> neighbor_cells;
  uint message_count;

  std::string to_string() const {
    return core::VehicleData::to_string() + " ; " + std::to_string(fiveg_timestep) + " ; " +
           std::to_string(next_fiveg_time) +
            " ; " + std::to_string(cam_timestep) + " ; " +
            std::to_string(next_cam_time) + " ; " + serving_cell.to_string();
  }
};

}

#endif