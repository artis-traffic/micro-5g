/**
 * @file artis-traffic/micro/five-g/signal_processing-state-machine/ActiveStateMachine.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_ACTIVESTATEMACHINE_HPP
#define ARTIS_TRAFFIC_MICRO_5G_ACTIVESTATEMACHINE_HPP

#include <artis-traffic/micro/five-g/signal_processing-state-machine/SignalProcessingIDs.hpp>
#include <artis-traffic/micro/five-g/signal_processing-state-machine/SignalProcessingStates.hpp>

#include <artis-traffic/micro/five-g/signal_processing-state-machine/SignalProcessingStateMachine.hpp>

#include <algorithm>
#include <cmath>
#include <iostream>
#include <memory>

namespace artis::traffic::micro::five_g {

    template<class Types, class Parameters, class StateType>
    class ActiveStateMachine
            : public artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType> {
    public:
        ActiveStateMachine(const typename Types::root_state_machine_type &root,
                             const Parameters &parameters)
                : artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType>(root, {}),
                  _cells(parameters.cells) {}

        ~ActiveStateMachine() override = default;

        void build(const std::shared_ptr<ActiveStateMachine<Types, Parameters, StateType>> &machine);

        typedef typename ActiveStateMachine<Types, Parameters, StateType>::template State<ActiveStateMachine<Types, Parameters, StateType>> State_t;
        typedef typename ActiveStateMachine<Types, Parameters, StateType>::template Transition<ActiveStateMachine<Types, Parameters, StateType>> Transition_t;

        // parameters
        std::vector<std::vector<Cell>> _cells;
    };

    template<class Types, class Parameters, class StateType>
    struct InitActiveState : ActiveStateMachine<Types, Parameters, StateType>::State_t {
        InitActiveState(
                const std::shared_ptr<ActiveStateMachine<Types, Parameters, StateType>> &machine) :
                ActiveStateMachine<Types, Parameters, StateType>::State_t(SignalProcessingStateIDs::INIT_ACTIVE,
                                                                          machine) {}

        artis::traffic::core::Time ta(const artis::traffic::core::Time &t) const override {
            return 0;
        }

    };

    template<class Types, class Parameters, class StateType>
    struct WaitActiveState : ActiveStateMachine<Types, Parameters, StateType>::State_t {
        WaitActiveState(
                const std::shared_ptr<ActiveStateMachine<Types, Parameters, StateType>> &machine) :
        ActiveStateMachine<Types, Parameters, StateType>::State_t(SignalProcessingStateIDs::WAIT_ACTIVE,
                machine) {}
                
    };

    template<class Types, class Parameters, class StateType>
    struct ActiveTransitionInit : ActiveStateMachine<Types, Parameters, StateType>::Transition_t {
        ActiveTransitionInit(const std::shared_ptr<ActiveStateMachine<Types, Parameters, StateType>> &machine)
                :
                ActiveStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {

//            std::cout<<"SP NEW ACTIVE " << serving_cell_id << std::endl;
            std::vector<std::vector<Cell>> cells = this->_machine->_cells;
            for (auto row_cell : cells) {
                for (auto cell : row_cell) {
                    this->_machine->state_()._cells_active_vehicles[cell.cell_id] = 0;
                }
            }

        }

        bool no_event() const override { return true; }

    };

    template<class Types, class Parameters, class StateType>
    struct ActiveTransitionNewActive : ActiveStateMachine<Types, Parameters, StateType>::Transition_t {
        ActiveTransitionNewActive(const std::shared_ptr<ActiveStateMachine<Types, Parameters, StateType>> &machine)
        :
        ActiveStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        std::vector<std::string> active_cells_id;

        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
            value(active_cells_id);

            for (auto serving_cell_id : active_cells_id) {
                this->_machine->state_()._cells_active_vehicles[serving_cell_id]++;
            }
        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::NEW_ACTIVE;
        }

    };

    template<class Types, class Parameters, class StateType>
    struct ActiveTransitionNewInactive : ActiveStateMachine<Types, Parameters, StateType>::Transition_t {
        ActiveTransitionNewInactive(const std::shared_ptr<ActiveStateMachine<Types, Parameters, StateType>> &machine)
                :
                ActiveStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        std::vector<std::string> active_cells_id;

        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
            value(active_cells_id);

            for (auto serving_cell_id : active_cells_id) {
                this->_machine->state_()._cells_active_vehicles[serving_cell_id]--;
            }
        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::NEW_INACTIVE;
        }

    };
    
    template<class Types, class Parameters, class StateType>
    void ActiveStateMachine<Types, Parameters, StateType>::build(
            const std::shared_ptr<ActiveStateMachine<Types, Parameters, StateType>> &machine) {
        this->state(new InitActiveState(machine));
        this->state(new WaitActiveState(machine));
        this->initial_state(SignalProcessingStateIDs::INIT_ACTIVE);
        this->transition(SignalProcessingStateIDs::INIT_ACTIVE, SignalProcessingStateIDs::WAIT_ACTIVE,
                         new ActiveTransitionInit(machine));
        this->transition(SignalProcessingStateIDs::WAIT_ACTIVE, SignalProcessingStateIDs::WAIT_ACTIVE,
                         new ActiveTransitionNewActive(machine));
        this->transition(SignalProcessingStateIDs::WAIT_ACTIVE, SignalProcessingStateIDs::WAIT_ACTIVE,
                         new ActiveTransitionNewInactive(machine));
    }
    
}
#endif //ARTIS_TRAFFIC_MICRO_5G_ACTIVESTATEMACHINE_HPP
