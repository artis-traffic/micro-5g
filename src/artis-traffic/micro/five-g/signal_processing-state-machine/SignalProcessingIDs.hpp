/**
 * @file artis-traffic/micro/five-g/signal_processing-state-machine/SignalProcessingIDs.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_FIVE_G_SIGNAL_PROCESSING_IDS_HPP
#define ARTIS_TRAFFIC_FIVE_G_SIGNAL_PROCESSING_IDS_HPP

namespace artis::traffic::micro::five_g {

struct SignalProcessingStateMachineIDs {
  enum values {
    TRANSMIT, ACTIVE
  };
};

struct SignalProcessingStateIDs {
  enum values {
    INIT_TRANSMIT, WAIT_TRANSMIT, WAIT_VEHICLE_LIST, INIT_ACTIVE, WAIT_ACTIVE
  };
};

struct SignalProcessingEventIDs {
  enum values {
    NEW_MESSAGE, TRANSMISSION, RECEIVING, NEW_ACTIVE, NOT_RECEIVING, NEW_INACTIVE, TRANSMITTING, NOT_TRANSMITTING,
    NEW_CAM_SIGNAL, SEND_CAM_SIGNAL, NEW_CAM_MESSAGE_UPLINK, NEW_CAM_MESSAGE_DOWNLINK, SEND_CAM_MESSAGE,
    DENM_TRANSMISSION, NEW_DENM_SIGNAL, SEND_DENM_SIGNAL, NEW_DENM_MESSAGE_UPLINK, NEW_DENM_MESSAGE_DOWNLINK,
    ASK_VEHICLE_LIST, GET_VEHICLE_LIST, BROADCAST_DENM_MESSAGE_RELAY, BROADCAST_DENM_MESSAGE_DOWNLINK
  };
};

}

#endif //ARTIS_TRAFFIC_FIVE_G_SIGNAL_PROCESSING_IDS_HPP
