/**
 * @file artis-traffic/micro/five_g/signal_processing-state-machine/SignalProcessingStates.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_FIVE_G_SIGNAL_PROCESSING_STATES_HPP
#define ARTIS_TRAFFIC_FIVE_G_SIGNAL_PROCESSING_STATES_HPP

#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/micro/five-g/Data.hpp>

#include <map>
#include <set>
#include <vector>

namespace artis::traffic::micro::five_g {

    template<class Types>
struct TransmitState {
    std::vector<MeasureReportMessage> _measure_messages;
    std::map<uint, Coordinates> _vehicles;
    std::vector<DENMMessage> _denm_messages;
    std::vector<DENMMessage> _sent_denm_messages;
    std::map<uint, Cell> _vehicles_serving_cell;
    int _serving_cell_changes;
    std::vector<CAMMessage> _cam_messages;
    std::vector<CAMMessage> _sent_cam_messages;
    CAMMessage _waiting_cam_message;
    DENMMessage _waiting_denm_message;
    std::set<uint> _denm_vehicle_indexes;
    std::vector<typename Types::vehicle_type> _vehicle_list;
};

struct ActiveState {
    std::map<std::string, uint > _cells_active_vehicles;
};
}

#endif //ARTIS_TRAFFIC_FIVE_G_SIGNAL_PROCESSING_STATES_HPP
