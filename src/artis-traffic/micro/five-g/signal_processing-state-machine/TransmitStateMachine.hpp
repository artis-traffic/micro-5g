/**
 * @file artis-traffic/micro/five-g/signal_processing-state-machine/TransmitStateMachine.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_TRANSMITSTATEMACHINE_HPP
#define ARTIS_TRAFFIC_MICRO_5G_TRANSMITSTATEMACHINE_HPP

#include <artis-traffic/micro/five-g/signal_processing-state-machine/SignalProcessingIDs.hpp>
#include <artis-traffic/micro/five-g/signal_processing-state-machine/SignalProcessingStates.hpp>

#include <artis-traffic/micro/five-g/signal_processing-state-machine/SignalProcessingStateMachine.hpp>

#include <algorithm>
#include <cmath>
#include <iostream>
#include <memory>

namespace artis::traffic::micro::five_g {

    template<class Types, class Parameters, class StateType>
    class TransmitStateMachine
            : public artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType> {
    public:
        TransmitStateMachine(const typename Types::root_state_machine_type &root,
                             const Parameters &parameters)
                : artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType>(root, {}),
                  _coordinates(parameters.coordinates), _cells(parameters.cells) {}

        ~TransmitStateMachine() override = default;

        void build(const std::shared_ptr <TransmitStateMachine<Types, Parameters, StateType>> &machine);

        int change_coordinates(Coordinates coordinates) {
//            int coordinates_index = -1;
//            if (_coordinates[0] == Coordinates{-1,-1}) {
//                coordinates_index = 0;
//                _coordinates[0] = coordinates;
//            }
//            if (_coordinates[1] == Coordinates{-1,-1}) {
//                coordinates_index = 1;
//                _coordinates[1] = coordinates;
//            }
            _coordinates[0] = coordinates;
            return 0;
        }

        double compute_distance(uint coordinates_index) {
            double d = sqrt(pow(_coordinates[coordinates_index].x - _coordinates[0].x, 2)
                            + pow(_coordinates[coordinates_index].y - _coordinates[0].y, 2));

            return d;
        }

        std::vector<int> get_frequencies(double distance) {
            std::vector<int> frequencies;
            for (uint i = 0; i < _cells.size(); i++) {
                for (uint j = 0; j < _cells[i].size(); j++) {
                    if (_cells[i][j].frequency == 26000) {
                        frequencies.push_back(26000);
                    }
                }
            }
            // distance théorique: 1200m
            // distance réelle pour débit à 0.5: 250m ou 400m
            if (distance <= 250) {
                frequencies.push_back(3500);
                frequencies.push_back(700);
            }
                // distance théorique: 8000m
                // distance réelle pour débit à 1.2: 5100m
            else if (distance <= 5100) {
                frequencies.push_back(700);
            }
            return frequencies;
        }

        void measure_select_serving_cell(MeasureReportMessage &message, std::vector<int> frequencies, int relay_in_index) {
            int frequency = -1;
            if (std::find(frequencies.begin(), frequencies.end(), 3500) != frequencies.end()) {
                frequency = 3500;
            } else if (std::find(frequencies.begin(), frequencies.end(), 700) != frequencies.end()) {
                frequency = 700;
            }
            std::vector <Cell> neighbor_cells;
            if (frequency != -1) {
                double best_angle = 999;
                double best_distance = 1001;
                Cell best_cell;
                int i = 2;
                for (auto cell: _cells[relay_in_index - 1]) {
                    if (cell.frequency == 26000) {
                        double distance = compute_distance(i);
                        if (distance < best_distance) {
                            message.data.serving_cell = cell;
                            best_cell = cell;
                            best_angle = 0;
                            best_distance = distance;
                        }
                        i++;
                    } else if (cell.frequency == frequency) {
                        std::vector<double> u = {cos(cell.orientation), sin(cell.orientation)};
                        std::vector<double> v = {_coordinates[0].x - _coordinates[1].x,
                                                 _coordinates[0].y - _coordinates[1].y};
                        double prodscal = u[0] * v[0] + u[1] * v[1];
                        double NormeU = sqrt(pow(u[0], 2) + pow(u[1], 2));
                        double NormeV = sqrt(pow(v[0], 2) + pow(v[1], 2));
                        double angle = acos(prodscal / (NormeU * NormeV)) * 180 / M_PI;

                        if ((angle < best_angle) or
                            (angle == best_angle and best_cell.frequency == 700 and cell.frequency == 35000)) {
                            message.data.serving_cell = cell;
                            best_cell = cell;
                            best_angle = angle;
                        }
                    }
                }
                for (auto cell: _cells[0]) {
                    double distance = compute_distance(1);
//                    if ((cell.frequency == frequency and not(cell == best_cell))
//                        or (frequency == 3500 and cell.frequency == 700)) {
                    if (distance <= 250 or (distance <= 5100 and cell.frequency == 700)) {
                        if (not(cell == best_cell)) {
                            neighbor_cells.push_back(cell);
                        }

                    }
                }
                for (int i = 1; i < _cells.size(); i++) {
                    for (auto cell: _cells[i]) {
                        if (compute_distance(i + 1) <= 210 and not(cell == best_cell)) {
                            neighbor_cells.push_back(cell);
                        }
                    }
                }
                message.neighbor_cells = neighbor_cells;
            }
        }

        void
        select_serving_cell(Message &message, std::vector<int> frequencies, int relay_in_index) {
            int frequency = -1;
            if (std::find(frequencies.begin(), frequencies.end(), 3500) != frequencies.end()) {
                frequency = 3500;
            } else if (std::find(frequencies.begin(), frequencies.end(), 700) != frequencies.end()) {
                frequency = 700;
            }
            if (frequency != -1) {
                double best_angle = 999;
                double best_distance = 1001;
                Cell best_cell;
                int i = 2;
                for (auto cell: _cells[relay_in_index - 1]) {
                    if (cell.frequency == 26000) {
                        double distance = compute_distance(i);
                        if (distance < best_distance) {
                            message.data.serving_cell = cell;
                            best_cell = cell;
                            best_angle = 0;
                            best_distance = distance;
                        }
                        i++;
                    } else if (cell.frequency == frequency) {
                        std::vector<double> u = {cos(cell.orientation), sin(cell.orientation)};
                        std::vector<double> v = {_coordinates[0].x - _coordinates[1].x,
                                                 _coordinates[0].y - _coordinates[1].y};
                        double prodscal = u[0] * v[0] + u[1] * v[1];
                        double NormeU = sqrt(pow(u[0], 2) + pow(u[1], 2));
                        double NormeV = sqrt(pow(v[0], 2) + pow(v[1], 2));
                        double angle = acos(prodscal / (NormeU * NormeV)) * 180 / M_PI;

                        if ((angle < best_angle) or
                            (angle == best_angle and best_cell.frequency == 700 and cell.frequency == 35000)) {
                            message.data.serving_cell = cell;
                            best_cell = cell;
                            best_angle = angle;
                        }
                    }
                }
            }
        }

        void
        cam_message_select_serving_cell(CAMMessage &message, std::vector<int> frequencies, int relay_in_index) {
            int frequency = -1;
            if (std::find(frequencies.begin(), frequencies.end(), 3500) != frequencies.end()) {
                frequency = 3500;
            } else if (std::find(frequencies.begin(), frequencies.end(), 700) != frequencies.end()) {
                frequency = 700;
            }
            if (frequency != -1) {
                double best_angle = 999;
                double best_distance = 1001;
                Cell best_cell;
                int i = 2;
                for (auto cell: _cells[relay_in_index - 1]) {
                    if (cell.frequency == 26000) {
                        double distance = compute_distance(i);
                        if (distance < best_distance) {
                            message.data.serving_cell = cell;
                            best_cell = cell;
                            best_angle = 0;
                            best_distance = distance;
                        }
                        i++;
                    } else if (cell.frequency == frequency) {
                        std::vector<double> u = {cos(cell.orientation), sin(cell.orientation)};
                        std::vector<double> v = {_coordinates[0].x - _coordinates[1].x,
                                                 _coordinates[0].y - _coordinates[1].y};
                        double prodscal = u[0] * v[0] + u[1] * v[1];
                        double NormeU = sqrt(pow(u[0], 2) + pow(u[1], 2));
                        double NormeV = sqrt(pow(v[0], 2) + pow(v[1], 2));
                        double angle = acos(prodscal / (NormeU * NormeV)) * 180 / M_PI;

                        if ((angle < best_angle) or
                            (angle == best_angle and best_cell.frequency == 700 and cell.frequency == 35000)) {
                            message.data.serving_cell = cell;
                            best_cell = cell;
                            best_angle = angle;
                        }
                    }
                }
            }
        }

        void denm_select_serving_cell(DENMMessage &message, std::vector<int> frequencies, int relay_in_index) {
            int frequency = -1;
            if (std::find(frequencies.begin(), frequencies.end(), 3500) != frequencies.end()) {
                frequency = 3500;
            } else if (std::find(frequencies.begin(), frequencies.end(), 700) != frequencies.end()) {
                frequency = 700;
            }
            if (frequency != -1) {
                double best_angle = 999;
                double best_distance = 1001;
                Cell best_cell;
                int i = 2;
                for (auto cell: _cells[relay_in_index - 1]) {
                    if (cell.frequency == 26000) {
                        double distance = compute_distance(i);
                        if (distance < best_distance) {
                            message.data.serving_cell = cell;
                            best_cell = cell;
                            best_angle = 0;
                            best_distance = distance;
                        }
                        i++;
                    } else if (cell.frequency == frequency) {
                        std::vector<double> u = {cos(cell.orientation), sin(cell.orientation)};
                        std::vector<double> v = {_coordinates[0].x - _coordinates[1].x,
                                                 _coordinates[0].y - _coordinates[1].y};
                        double prodscal = u[0] * v[0] + u[1] * v[1];
                        double NormeU = sqrt(pow(u[0], 2) + pow(u[1], 2));
                        double NormeV = sqrt(pow(v[0], 2) + pow(v[1], 2));
                        double angle = acos(prodscal / (NormeU * NormeV)) * 180 / M_PI;

                        if ((angle < best_angle) or
                            (angle == best_angle and best_cell.frequency == 700 and cell.frequency == 35000)) {
                            message.data.serving_cell = cell;
                            best_cell = cell;
                            best_angle = angle;
                        }
                    }
                }
            }
        }

        int get_port_index() {
//            for (uint i = 0; i < _cells.size(); i++) {
//                for (uint j = 0; j < _cells[i].size(); j++) {
//                    if (_cells[i][j].frequency == 26000) {
//                        return i + 1;
//                    }
//                }
//            }
//            return 1;
            if (_cells.size() > 1) {
                double best_distance = 99999;
                uint best_index = 0;
                std::vector<double> distances;
                for (uint i = 1; i < _cells.size(); i++) {
                    double distance = compute_distance(i + 1);
                    distances.push_back(distance);
                    if (distance < best_distance) {
                        best_distance = distance;
                        best_index = i;
                    }
                }
                if (int(best_distance) <= 212) {
                    return best_index + 1;
                }
            }
            return 1;
        }

        double hata_quasi_open(double distance) {
            return 15.35344921 * log(1.24172605 * distance + 13.81357129) - 9.50381819;
        }

        double uma(double distance) {
            return 19.78337805 * log(18.46316012 * distance + 545.80430058) - 56.13456372;
        }

        double transmission(MeasureReportMessage &message, int relay_in_index, int active_vehicles) {
            int coordinates_index = 0;

            double distance = compute_distance(relay_in_index);
            std::vector<int> frequencies = get_frequencies(distance);
            if (message.data.serving_cell.cell_id == "fake" or message.cell_change) {
                measure_select_serving_cell(message, frequencies, relay_in_index);
            }

            // uplink
            if (message.in_port_index == 0) {
                return uplink_transmission(message, distance, coordinates_index);
            }
                // downlink
            else {
                return downlink_transmission(message, distance, coordinates_index, active_vehicles);
            }
        }

        template<typename Message>
        double transmission(Message &message, int relay_in_index, int active_vehicles) {
            int coordinates_index = 0;

            double distance = compute_distance(relay_in_index);
            std::vector<int> frequencies = get_frequencies(distance);
            if (message.data.serving_cell.cell_id == "fake" or message.cell_change) {
                select_serving_cell(message, frequencies, relay_in_index);
            }

            // uplink
            if (message.in_port_index == 0) {
                return uplink_transmission(message, distance, coordinates_index);
            }
                // downlink
            else {
                return downlink_transmission(message, distance, coordinates_index, active_vehicles);
            }
        }

//        double cam_signal_transmission(CAMSignalMessage &message, int relay_in_index) {
//            int coordinates_index = 0;
////            if (_coordinates[0] == Coordinates{-1, -1}) {
////                coordinates_index = 0;
////                _coordinates[0] = message.data.position;
////            }
////            if (_coordinates[1] == Coordinates{-1, -1}) {
////                coordinates_index = 1;
////                _coordinates[1] = message.data.position;
////            }
//            double distance = compute_distance(relay_in_index);
//            std::vector<int> frequencies = get_frequencies(distance);
//            if (message.data.serving_cell.cell_id == "fake" or message.cell_change) {
//                select_serving_cell(message, frequencies, relay_in_index);
//            }
//
//            // uplink
//            if (message.in_port_index == 0) {
//                return cam_signal_uplink_transmission(message, distance, coordinates_index);
//            }
//                // downlink
//            else {
//                std::cout << "ERROR CAM DOWNLINK" << std::endl;
//                return 100000;
////                return downlink_transmission(message, distance, coordinates_index);
//            }
//        }
//
//        double denm_transmission(Message &message, int relay_in_index) {
//            int coordinates_index = 0;
////            if (_coordinates[0] == Coordinates{-1, -1}) {
////                coordinates_index = 0;
////                _coordinates[0] = message.data.position;
////            }
////            if (_coordinates[1] == Coordinates{-1, -1}) {
////                coordinates_index = 1;
////                _coordinates[1] = message.data.position;
////            }
//            double distance = compute_distance(relay_in_index);
//            std::vector<int> frequencies = get_frequencies(distance);
//            if (message.data.serving_cell.cell_id == "fake" or message.cell_change) {
//                denm_select_serving_cell(message, frequencies, relay_in_index);
//            }
//
//            // uplink
//            if (message.in_port_index == 0) {
//                return uplink_transmission(message, distance, coordinates_index);
//            }
//                // downlink
//            else {
//                return downlink_transmission(message, distance, coordinates_index);
//            }
//        }
//
//        double cam_message_transmission(CAMMessage &message, int relay_in_index) {
//            int coordinates_index = 0;
////            if (_coordinates[0] == Coordinates{-1, -1}) {
////                coordinates_index = 0;
////                _coordinates[0] = message.data.position;
////            }
////            if (_coordinates[1] == Coordinates{-1, -1}) {
////                coordinates_index = 1;
////                _coordinates[1] = message.data.position;
////            }
//            double distance = compute_distance(relay_in_index);
//            std::vector<int> frequencies = get_frequencies(distance);
//            if (message.data.serving_cell.cell_id == "fake" or message.cell_change) {
//                cam_message_select_serving_cell(message, frequencies, relay_in_index);
//            }
//
//            // uplink
//            if (message.in_port_index == 0) {
//                std::cout << "ERROR CAM MESSAGE UPLINK" << std::endl;
//                return 100000;
//            }
//                // downlink
//            else {
//                return cam_message_downlink_transmission(message, distance, coordinates_index);
//            }
//        }

        template<typename Message>
        double uplink_transmission(Message &message, double distance, int coordinates_index) {
            double attenuation = 0;
            double bandwidth = 0;
            double sinr = 0;
            double linear_sinr = 0;
            double debit = 0;
            if (message.data.serving_cell.frequency == 3500) {
                attenuation = uma(distance);
                bandwidth = 34020000;
                sinr = (103.97 - 3 + 25.05 - 0.5 - 3 - 8.96 - 1 - 25 - attenuation + 23);
                linear_sinr = pow(10, (sinr / 10));
                debit = (1 / 0.0005) * 189 * 12 * 11 * 0.23 * 0.8697 * log2(1 + linear_sinr / 1.2956);
            } else {
                attenuation = hata_quasi_open(distance);
                bandwidth = 4320000;
                sinr = (107.65 - 2 + 17 - 0.5 - 3 - 7.68 - 1 - 15 - attenuation + 23);
                linear_sinr = pow(10, (sinr / 10));
                debit = (1 / 0.0005) * 24 * 12 * 11 * 1 * 0.8697 * std::log2(1 + linear_sinr / 1.2956);
            }
            message.data.serving_cell.rsrp = attenuation;
            message.data.serving_cell.rsrq = distance;
            message.data.serving_cell.sinr = sinr;
            // débit max shannon
            double max_debit = (bandwidth * std::log2(1 + pow(10, (sinr / 10)))) * 1e-6;
            // débit effectif
            message.data.serving_cell.rsrq = debit;
            // bytes => bits
            double size = message.message_size();
            // mbps => bps
            max_debit = max_debit * 1e6;
            double delay = size / debit;
//            std::cout<<"UPLINK cell "<<message.data.serving_cell.cell_id<<" mess id "<<message.message_id<<" debit (mbps) "<<debit * 1e-6<<" delay "<<delay<<" size "<<size<<std::endl;
            if (coordinates_index != -1) {
                _coordinates[coordinates_index] = Coordinates{-1, -1};
            }
            message.delay = delay;
            message.debit = debit;
            message.distance = distance;
            return delay;
        }

//        double cam_signal_uplink_transmission(CAMSignalMessage &message, double distance, int coordinates_index) {
//            double attenuation = 0;
//            double bandwidth = 0;
//            double sinr = 0;
//            double linear_sinr = 0;
//            double debit = 0;
//            if (message.data.serving_cell.frequency == 3500) {
//                attenuation = uma(distance);
//                bandwidth = 34020000;
//                sinr = (103.97 - 3 + 25.05 - 0.5 - 3 - 8.96 - 1 - 25 - attenuation + 23);
//                linear_sinr = pow(10, (sinr / 10));
//                debit = (1 / 0.0005) * 189 * 12 * 11 * 0.23 * 0.8697 * log2(1 + linear_sinr / 1.2956);
//            } else {
//                attenuation = hata_quasi_open(distance);
//                bandwidth = 4320000;
//                sinr = (107.65 - 2 + 17 - 0.5 - 3 - 7.68 - 1 - 15 - attenuation + 23);
//                linear_sinr = pow(10, (sinr / 10));
//                debit = (1 / 0.0005) * 24 * 12 * 11 * 1 * 0.8697 * std::log2(1 + linear_sinr / 1.2956);
//            }
//            // débit max shannon
//            double max_debit = (bandwidth * std::log2(1 + pow(10, (sinr / 10)))) * 1e-6;
//            // bytes => bits
//            double size = message.message_size();
//            // mbps => bps
//            max_debit = max_debit * 1e6;
//            double delay = size / debit;
////            std::cout<<"UPLINK CAMSignal cell "<<message.data.serving_cell.cell_id<<" mess id "<<message.message_id<<" debit (mbps) "<<debit * 1e-6<<" delay "<<delay<<" size "<<size<<std::endl;
//            if (coordinates_index != -1) {
//                _coordinates[coordinates_index] = Coordinates{-1, -1};
//            }
//            message.delay = delay;
//            message.debit = debit;
//            message.distance = distance;
//            return delay;
//        }
//
//        double denm_uplink_transmission(DENMMessage &message, double distance, int coordinates_index) {
//            double attenuation = 0;
//            double bandwidth = 0;
//            double sinr = 0;
//            double linear_sinr = 0;
//            double debit = 0;
//            if (message.data.serving_cell.frequency == 3500) {
//                attenuation = uma(distance);
//                bandwidth = 34020000;
//                sinr = (103.97 - 3 + 25.05 - 0.5 - 3 - 8.96 - 1 - 25 - attenuation + 23);
//                linear_sinr = pow(10, (sinr / 10));
//                debit = (1 / 0.0005) * 189 * 12 * 11 * 0.23 * 0.8697 * log2(1 + linear_sinr / 1.2956);
//            } else {
//                attenuation = hata_quasi_open(distance);
//                bandwidth = 4320000;
//                sinr = (107.65 - 2 + 17 - 0.5 - 3 - 7.68 - 1 - 15 - attenuation + 23);
//                linear_sinr = pow(10, (sinr / 10));
//                debit = (1 / 0.0005) * 24 * 12 * 11 * 1 * 0.8697 * std::log2(1 + linear_sinr / 1.2956);
//            }
//            // débit max shannon
//            double max_debit = (bandwidth * std::log2(1 + pow(10, (sinr / 10)))) * 1e-6;
//            // bytes => bits
//            double size = message.message_size();
//            // mbps => bps
//            max_debit = max_debit * 1e6;
//            double delay = size / debit;
////            std::cout<<"UPLINK DENMSignal cell "<<message.data.serving_cell.cell_id<<" mess id "<<message.message_id<<" debit (mbps) "<<debit * 1e-6<<" delay "<<delay<<" size "<<size<<std::endl;
//            if (coordinates_index != -1) {
//                _coordinates[coordinates_index] = Coordinates{-1, -1};
//            }
//            message.delay = delay;
//            message.debit = debit;
//            message.distance = distance;
//            return delay;
//        }

        template<typename Message>
        double downlink_transmission(Message &message, double distance, int coordinates_index, int active_vehicles) {
            double attenuation = 0;
            double sinr = 0;
            double linear_sinr = 0;
            double debit = 0;
            double zone_distance = 0;
            if (message.data.serving_cell.frequency == 3500) {
                zone_distance = distance - (distance / 5);
                attenuation = uma(zone_distance);
                sinr = 115.15 - attenuation;
                linear_sinr = pow(10, (sinr / 10));

                if (linear_sinr < 25) {
                    debit = std::min(103.3 * std::log2(1 + linear_sinr / 5.7), 1729.4);
                } else {
                    debit = std::min(157.5 * std::log2(1 + linear_sinr / 24.7), 1729.4);
                }
            } else {
                int zone = distance / 100;
                zone_distance = zone * 100;
                attenuation = hata_quasi_open(zone_distance);
                sinr = 132.33 - attenuation;
                linear_sinr = pow(10, (sinr / 10));

                if (linear_sinr < 25) {
                    debit = std::min(6.5 * std::log2(1 + linear_sinr / 2.6), 110.0);
                } else {
                    debit = std::min(9 * std::log2(1 + linear_sinr / 9.8), 110.0);
                }
            }

            const auto &state = this->root().template state_<ActiveState>(Types::state_machine_IDs_type::ACTIVE);
            debit = (debit * 1e6) / active_vehicles;
            message.data.serving_cell.rsrp = attenuation;
            message.data.serving_cell.rsrq = distance;
            message.data.serving_cell.sinr = sinr;
            message.data.serving_cell.rsrq = debit;
            double size = message.message_size();
            double delay = size / debit;
//            std::cout<<"DOWNLINK zone " << zone_distance << " dist " << distance <<
//            " attenuation " << attenuation << " sinr " << linear_sinr
//            << " debit (mbps) " << debit * 1e-6 << " delay " << delay <<" act " << active_vehicles <<
//            " cell " << message.data.serving_cell.cell_id << " mess id " << message.message_id << " size " << size << std::endl;
            if (coordinates_index != -1) {
                _coordinates[coordinates_index] = Coordinates{-1, -1};
            }
            message.delay = delay;
            message.debit = debit;
            message.distance = distance;
            return delay;
        }

//        double cam_message_downlink_transmission(CAMMessage &message, double distance, int coordinates_index) {
//            double attenuation = 0;
//            double sinr = 0;
//            double linear_sinr = 0;
//            double debit = 0;
//            if (message.data.serving_cell.frequency == 3500) {
//                int zone_distance = distance - (distance / 5);
//                attenuation = uma(zone_distance);
//                sinr = 115.15 - attenuation;
//                linear_sinr = pow(10, (sinr / 10));
//
//                if (linear_sinr < 25) {
//                    debit = std::min(103.3 * std::log2(1 + linear_sinr / 5.7), 1729.4);
//                } else {
//                    debit = std::min(157.5 * std::log2(1 + linear_sinr / 24.7), 1729.4);
//                }
//            } else {
//                int zone = distance / 100;
//                double zone_distance = zone * 100;
//                attenuation = hata_quasi_open(zone_distance);
//                sinr = 132.33 - attenuation;
//                linear_sinr = pow(10, (sinr / 10));
//
//                if (linear_sinr < 25) {
//                    debit = std::min(6.5 * std::log2(1 + linear_sinr / 2.6), 110.0);
//                } else {
//                    debit = std::min(9 * std::log2(1 + linear_sinr / 9.8), 110.0);
//                }
//            }
//
//            const auto &state = this->root().template state_<ActiveState>(Types::state_machine_IDs_type::ACTIVE);
//            debit = debit * 1e6;
//            message.data.serving_cell.rsrp = attenuation;
//            message.data.serving_cell.rsrq = distance;
//            message.data.serving_cell.sinr = sinr;
//            message.data.serving_cell.rsrq = debit;
//            double size = message.message_size();
//            double delay = size / debit;
////            std::cout<<"DOWNLINK zone " << zone_distance << " dist " << distance <<
////            " attenuation " << attenuation << " sinr " << linear_sinr
////            << " debit (mbps) " << debit * 1e-6 << " delay " << delay <<" act " << state._cells_active_vehicles.at(message.data.serving_cell.cell_id) <<
////            " cell " << message.data.serving_cell.cell_id << " mess id " << message.message_id << " size " << size << std::endl;
//            if (coordinates_index != -1) {
//                _coordinates[coordinates_index] = Coordinates{-1, -1};
//            }
//            message.delay = delay;
//            message.debit = debit;
//            message.distance = distance;
//            return delay;
//        }
//
//        double denm_downlink_transmission(DENMMessage &message, double distance, int coordinates_index) {
//            double attenuation = 0;
//            double sinr = 0;
//            double linear_sinr = 0;
//            double debit = 0;
//            if (message.data.serving_cell.frequency == 3500) {
//                double zone_distance = distance - (distance / 5);
//                attenuation = uma(zone_distance);
//                sinr = 115.15 - attenuation;
//                linear_sinr = pow(10, (sinr / 10));
//
//                if (linear_sinr < 25) {
//                    debit = std::min(103.3 * std::log2(1 + linear_sinr / 5.7), 1729.4);
//                } else {
//                    debit = std::min(157.5 * std::log2(1 + linear_sinr / 24.7), 1729.4);
//                }
//            } else {
//                int zone = distance / 100;
//                double zone_distance = zone * 100;
//                attenuation = hata_quasi_open(zone_distance);
//                sinr = 132.33 - attenuation;
//                linear_sinr = pow(10, (sinr / 10));
//
//                if (linear_sinr < 25) {
//                    debit = std::min(6.5 * std::log2(1 + linear_sinr / 2.6), 110.0);
//                } else {
//                    debit = std::min(9 * std::log2(1 + linear_sinr / 9.8), 110.0);
//                }
//            }
//
//            const auto &state = this->root().template state_<ActiveState>(Types::state_machine_IDs_type::ACTIVE);
//            // add self
//            int active_vehicles = state._cells_active_vehicles.at(message.data.serving_cell.cell_id) + 1;
//            debit = (debit / active_vehicles) * 1e6;
//            message.data.serving_cell.rsrp = attenuation;
//            message.data.serving_cell.rsrq = distance;
//            message.data.serving_cell.sinr = sinr;
//            message.data.serving_cell.rsrq = debit;
//            double size = message.message_size();
//            double delay = size / debit;
////            std::cout<<"DENM DOWNLINK zone " << zone_distance << " dist " << distance <<
////                     " attenuation " << attenuation << " sinr " << linear_sinr
////                     << " debit (mbps) " << debit * 1e-6 << " delay " << delay <<" act " << state._cells_active_vehicles.at(message.data.serving_cell.cell_id) <<
////                     " cell " << message.data.serving_cell.cell_id << " mess id " << message.message_id << " size " << size << std::endl;
//            if (coordinates_index != -1) {
//                _coordinates[coordinates_index] = Coordinates{-1, -1};
//            }
//            message.delay = delay;
//            message.debit = debit;
//            message.distance = distance;
//            return delay;
//        }

        std::vector <MeasureReportMessage> waked_messages(
                const artis::traffic::core::Time &t) {
            std::vector <MeasureReportMessage> list;

            for (const auto &message: this->_state._measure_messages) {
                if (std::abs(t - message.next_time) < 1e-5) {
                    list.push_back(message);
                }
            }

            return list;
        }

        std::vector <DENMMessage> waked_denm_messages(
                const artis::traffic::core::Time &t) {
            std::vector <DENMMessage> list;

            for (const auto &message: this->_state._denm_messages) {
                if (std::abs(t - message.next_time) < 1e-5) {
                    list.push_back(message);
                }
            }

            return list;
        }

        std::vector <CAMMessage> waked_cam_messages(
                const artis::traffic::core::Time &t) {
            std::vector <CAMMessage> list;

            for (const auto &message: this->_state._cam_messages) {
                if (std::abs(t - message.next_time) < 1e-5) {
                    list.push_back(message);
                }
            }

            return list;
        }

// types
        typedef typename TransmitStateMachine<Types, Parameters, StateType>::template State<TransmitStateMachine<Types, Parameters, StateType>> State_t;
        typedef typename TransmitStateMachine<Types, Parameters, StateType>::template Transition<TransmitStateMachine<Types, Parameters, StateType>> Transition_t;

        // parameters
        std::vector <Coordinates> _coordinates;
        std::vector <std::vector<Cell>> _cells;
    };


    template<class Types, class Parameters, class StateType>
    struct InitTransmitState : TransmitStateMachine<Types, Parameters, StateType>::State_t {
        InitTransmitState(
                const std::shared_ptr <TransmitStateMachine<Types, Parameters, StateType>> &machine) :
                TransmitStateMachine<Types, Parameters, StateType>::State_t(SignalProcessingStateIDs::INIT_TRANSMIT,
                                                                            machine) {}

        artis::traffic::core::Time ta(const artis::traffic::core::Time &t) const override {
            return 0;
        }

    };

    template<class Types, class Parameters, class StateType>
    struct WaitTransmitState : TransmitStateMachine<Types, Parameters, StateType>::State_t {
        WaitTransmitState(
                const std::shared_ptr <TransmitStateMachine<Types, Parameters, StateType>> &machine) :
                TransmitStateMachine<Types, Parameters, StateType>::State_t(SignalProcessingStateIDs::WAIT_TRANSMIT,
                                                                            machine) {}

        artis::traffic::core::Time ta(const artis::traffic::core::Time &t) const override {
            const std::vector <MeasureReportMessage> &messages = this->_machine->state_()._measure_messages;
            const std::vector <CAMMessage> &cam_messages = this->_machine->state_()._cam_messages;
            const std::vector <DENMMessage> &denm_messages = this->_machine->state_()._denm_messages;

            artis::traffic::core::Time min_measure;
            artis::traffic::core::Time min_cam;
            artis::traffic::core::Time min_denm;
            if (messages.empty()) {
                min_measure = artis::common::DoubleTime::infinity;
            } else {
                auto it = messages.cbegin();

                min_measure = std::min_element(messages.cbegin(), messages.cend(),
                                               [](const MeasureReportMessage &m1,
                                                  const MeasureReportMessage &m2) {
                                                   return m1.next_time < m2.next_time;
                                               })->next_time - t;
            }

            if (cam_messages.empty()) {
                min_cam = artis::common::DoubleTime::infinity;
            } else {
                auto it = messages.cbegin();

                min_cam = std::min_element(cam_messages.cbegin(), cam_messages.cend(),
                                            [](const CAMMessage &m1,
                                               const CAMMessage &m2) {
                                                return m1.next_time < m2.next_time;
                                            })->next_time - t;
            }
            
            if (denm_messages.empty()) {
                min_denm = artis::common::DoubleTime::infinity;
            } else {
                auto it = messages.cbegin();

                min_denm = std::min_element(denm_messages.cbegin(), denm_messages.cend(),
                                            [](const DENMMessage &m1,
                                               const DENMMessage &m2) {
                                                return m1.next_time < m2.next_time;
                                            })->next_time - t;
            }

            return std::min(std::min(min_measure, min_cam), min_denm);
        }
    };

    template<class Types, class Parameters, class StateType>
    struct TransmitTransitionInit : TransmitStateMachine<Types, Parameters, StateType>::Transition_t {
        TransmitTransitionInit(const std::shared_ptr <TransmitStateMachine<Types, Parameters, StateType>> &machine)
                :
                TransmitStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        void action(const artis::traffic::core::Time &t, const artis::common::event::Value &value) override {

            this->_machine->state_()._serving_cell_changes = 0;

        }

        bool no_event() const override { return true; }

    };

    template<class Types, class Parameters, class StateType>
    struct TransmitTransitionNewMessage : TransmitStateMachine<Types, Parameters, StateType>::Transition_t {
        TransmitTransitionNewMessage(
                const std::shared_ptr <TransmitStateMachine<Types, Parameters, StateType>> &machine)
                :
                TransmitStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        MeasureReportMessage message;

        void action(const artis::traffic::core::Time &t, const artis::common::event::Value &value) override {
            value(message);

            int coordinates_index =
                    this->_machine->change_coordinates(this->_machine->state_()._vehicles[message.data.vehicle_index]);
            double old_distance = this->_machine->compute_distance(this->_machine->get_port_index());
//            if (coordinates_index != -1) {
//                this->_machine->_coordinates[coordinates_index] = Coordinates{-1, -1};
//            }

            coordinates_index =
                    this->_machine->change_coordinates(message.data.position);
            double new_distance = this->_machine->compute_distance(this->_machine->get_port_index());
//            if (coordinates_index != -1) {
//                this->_machine->_coordinates[coordinates_index] = Coordinates{-1, -1};
//            }

            if (message.cell_change or not message.is_event or this->_machine->get_frequencies(old_distance)
                                                               != this->_machine->get_frequencies(new_distance)) {

                if (message.in_port_index == 0) {
                    int in_port_index = this->_machine->get_port_index();
                    message.next_time += this->_machine->transmission(message, in_port_index, 1);
                    message.in_port_index = in_port_index;
                } else {
                    message.next_time += this->_machine->transmission(message, message.in_port_index, 1);
                    message.in_port_index = 0;
                }
                bool found_vehicle = false;

                for (const auto vehicle: this->_machine->state_()._vehicles_serving_cell) {
                    if (vehicle.first == message.data.vehicle_index) {
                        found_vehicle = true;
                        if (vehicle.second.cell_id != message.data.serving_cell.cell_id) {
                            this->_machine->state_()._serving_cell_changes++;
                        }
                    }
                }
                if (!found_vehicle) {
                    this->_machine->state_()._serving_cell_changes++;
                }
                this->_machine->_coordinates[coordinates_index] = Coordinates{-1, -1};

                this->_machine->state_()._measure_messages.push_back(message);
                this->_machine->state_()._vehicles[message.data.vehicle_index] = message.data.position;
                this->_machine->state_()._vehicles_serving_cell[message.data.vehicle_index] = message.data.serving_cell;
            }
        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::NEW_MESSAGE;
        }

//        typename TransmitStateMachine<Types, Parameters, StateType>::Events
//        output(const artis::traffic::core::Time & /* t */) const override {
//            typename TransmitStateMachine<Types, Parameters, StateType>::Events events{};
//            if (message.in_port_index == 0) {
//                events.externals.push_back(
//                        typename TransmitStateMachine<Types, Parameters,
//                                StateType>::ExternalEvent{SignalProcessingEventIDs::RECEIVING, {message}});
//            }
//            else {
//                events.externals.push_back(
//                        typename TransmitStateMachine<Types, Parameters,
//                                StateType>::ExternalEvent{SignalProcessingEventIDs::TRANSMITTING, {message}});
//            }
//            return events;
//        }

    };

    template<class Types, class Parameters, class StateType>
    struct TransmitTransitionNewCAMSignal : TransmitStateMachine<Types, Parameters, StateType>::Transition_t {
        TransmitTransitionNewCAMSignal(
                const std::shared_ptr <TransmitStateMachine<Types, Parameters, StateType>> &machine)
                :
                TransmitStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        CAMSignalMessage message;

        void action(const artis::traffic::core::Time &t, const artis::common::event::Value &value) override {
            value(message);

            int coordinates_index =
                    this->_machine->change_coordinates(this->_machine->state_()._vehicles[message.data.vehicle_index]);
            double old_distance = this->_machine->compute_distance(this->_machine->get_port_index());

            coordinates_index =
                    this->_machine->change_coordinates(message.data.position);
            double new_distance = this->_machine->compute_distance(this->_machine->get_port_index());

            if (message.in_port_index == 0) {
                int in_port_index = this->_machine->get_port_index();
                message.next_time += this->_machine->transmission(message, in_port_index, 1);
                message.in_port_index = in_port_index;
                this->_machine->state_()._vehicles[message.data.vehicle_index] = message.data.position;
            } else {
                message.in_port_index = 0;
            }
            this->_machine->_coordinates[coordinates_index] = Coordinates{-1, -1};

//            }
        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::NEW_CAM_SIGNAL;
        }

        typename TransmitStateMachine<Types, Parameters, StateType>::Events
        output(const artis::traffic::core::Time &t) const override {
            typename TransmitStateMachine<Types, Parameters, StateType>::Events events{};
            if (message.in_port_index == 0) {
                events.externals.push_back(
                        typename TransmitStateMachine<Types, Parameters,
                                StateType>::ExternalEvent{SignalProcessingEventIDs::SEND_CAM_SIGNAL, {message}});
            } else {
//                events.externals.push_back(
//                        typename TransmitStateMachine<Types, Parameters,
//                                StateType>::ExternalEvent{SignalProcessingEventIDs::TRANSMITTING, {message}});
                events.externals.push_back(
                        typename TransmitStateMachine<Types, Parameters,
                                StateType>::ExternalEvent{SignalProcessingEventIDs::SEND_CAM_SIGNAL, {message}});
            }
            return events;
        }

    };

    template<class Types, class Parameters, class StateType>
    struct TransmitTransitionNewCAMMessageUplink : TransmitStateMachine<Types, Parameters, StateType>::Transition_t {
        TransmitTransitionNewCAMMessageUplink(
                const std::shared_ptr <TransmitStateMachine<Types, Parameters, StateType>> &machine)
                :
                TransmitStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        CAMMessage message;

        void action(const artis::traffic::core::Time &t, const artis::common::event::Value &value) override {
            value(message);

            int coordinates_index =
                    this->_machine->change_coordinates(this->_machine->state_()._vehicles[message.data.vehicle_index]);
            double old_distance = this->_machine->compute_distance(this->_machine->get_port_index());

            coordinates_index =
                    this->_machine->change_coordinates(message.data.position);
            double new_distance = this->_machine->compute_distance(this->_machine->get_port_index());

            int in_port_index = this->_machine->get_port_index();
            message.next_time += this->_machine->transmission(message, in_port_index, 1);
            message.in_port_index = in_port_index;

            this->_machine->state_()._cam_messages.push_back(message);
            this->_machine->_coordinates[coordinates_index] = Coordinates{-1, -1};
        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::NEW_CAM_MESSAGE_UPLINK;
        }

        typename TransmitStateMachine<Types, Parameters, StateType>::Events
        output(const artis::traffic::core::Time &t) const override {
            typename TransmitStateMachine<Types, Parameters, StateType>::Events events{};
            //only send message directly if its uplink
            if (message.in_port_index > 0) {
                events.externals.push_back(
                        typename TransmitStateMachine<Types, Parameters,
                                StateType>::ExternalEvent{SignalProcessingEventIDs::SEND_CAM_MESSAGE, {message}});
            }
            return events;
        }

    };

    template<class Types, class Parameters, class StateType>
    struct TransmitTransitionNewCAMMessageDownlink : TransmitStateMachine<Types, Parameters, StateType>::Transition_t {
        TransmitTransitionNewCAMMessageDownlink(
                const std::shared_ptr <TransmitStateMachine<Types, Parameters, StateType>> &machine)
                :
                TransmitStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        CAMMessage message;

        void action(const artis::traffic::core::Time &t, const artis::common::event::Value &value) override {
            value(message);

            this->_machine->state_()._waiting_cam_message = message;

        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::NEW_CAM_MESSAGE_DOWNLINK;
        }

        typename TransmitStateMachine<Types, Parameters, StateType>::Events
        output(const artis::traffic::core::Time & /* t */) const override {
            typename TransmitStateMachine<Types, Parameters, StateType>::Events events{};
            events.externals.push_back(
                    typename TransmitStateMachine<Types, Parameters,
                            StateType>::ExternalEvent{SignalProcessingEventIDs::ASK_VEHICLE_LIST});
            return events;
        }

    };

    template<class Types, class Parameters, class StateType>
    struct TransmitTransitionNewDENMSignal : TransmitStateMachine<Types, Parameters, StateType>::Transition_t {
        TransmitTransitionNewDENMSignal(
                const std::shared_ptr <TransmitStateMachine<Types, Parameters, StateType>> &machine)
                :
                TransmitStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        DENMSignalMessage message;

        void action(const artis::traffic::core::Time &t, const artis::common::event::Value &value) override {
            value(message);

            int coordinates_index =
                    this->_machine->change_coordinates(this->_machine->state_()._vehicles[message.data.vehicle_index]);
            double old_distance = this->_machine->compute_distance(this->_machine->get_port_index());

            coordinates_index =
                    this->_machine->change_coordinates(message.data.position);
            double new_distance = this->_machine->compute_distance(this->_machine->get_port_index());

            if (message.in_port_index == 0) {
                int in_port_index = this->_machine->get_port_index();
                message.next_time += this->_machine->transmission(message, in_port_index, 1);
                message.in_port_index = in_port_index;
                this->_machine->state_()._vehicles[message.data.vehicle_index] = message.data.position;
            } else {
                message.in_port_index = 0;
            }
            this->_machine->_coordinates[coordinates_index] = Coordinates{-1, -1};

//            }
        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::NEW_DENM_SIGNAL;
        }

        typename TransmitStateMachine<Types, Parameters, StateType>::Events
        output(const artis::traffic::core::Time &t) const override {
            typename TransmitStateMachine<Types, Parameters, StateType>::Events events{};
            if (message.in_port_index == 0) {
                events.externals.push_back(
                        typename TransmitStateMachine<Types, Parameters,
                                StateType>::ExternalEvent{SignalProcessingEventIDs::SEND_DENM_SIGNAL, {message}});
            } else {
//                events.externals.push_back(
//                        typename TransmitStateMachine<Types, Parameters,
//                                StateType>::ExternalEvent{SignalProcessingEventIDs::TRANSMITTING, {message}});
                events.externals.push_back(
                        typename TransmitStateMachine<Types, Parameters,
                                StateType>::ExternalEvent{SignalProcessingEventIDs::SEND_DENM_SIGNAL, {message}});
            }
            return events;
        }

    };
    
    template<class Types, class Parameters, class StateType>
    struct TransmitTransitionNewDENMMessageUplink : TransmitStateMachine<Types, Parameters, StateType>::Transition_t {
        TransmitTransitionNewDENMMessageUplink(
                const std::shared_ptr <TransmitStateMachine<Types, Parameters, StateType>> &machine)
                :
                TransmitStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        DENMMessage message;

        void action(const artis::traffic::core::Time &t, const artis::common::event::Value &value) override {
            value(message);

            int coordinates_index =
                    this->_machine->change_coordinates(this->_machine->state_()._vehicles[message.data.vehicle_index]);
            double old_distance = this->_machine->compute_distance(this->_machine->get_port_index());

            coordinates_index =
                    this->_machine->change_coordinates(message.data.position);
            double new_distance = this->_machine->compute_distance(this->_machine->get_port_index());

            int in_port_index = this->_machine->get_port_index();
            message.next_time += this->_machine->transmission(message, in_port_index, 1);
            message.in_port_index = in_port_index;

            this->_machine->state_()._denm_messages.push_back(message);
            if (message.entering_vehicle) {
                this->_machine->state_()._denm_vehicle_indexes.insert(message.data.vehicle_index);
            }
            else {
                this->_machine->state_()._denm_vehicle_indexes.erase(message.data.vehicle_index);
            }
            this->_machine->_coordinates[coordinates_index] = Coordinates{-1, -1};
        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::NEW_DENM_MESSAGE_UPLINK;
        }

        typename TransmitStateMachine<Types, Parameters, StateType>::Events
        output(const artis::traffic::core::Time &t) const override {
            typename TransmitStateMachine<Types, Parameters, StateType>::Events events{};
            //only send message directly if its uplink
            if (message.in_port_index > 0) {
                events.externals.push_back(
                        typename TransmitStateMachine<Types, Parameters,
                                StateType>::ExternalEvent{SignalProcessingEventIDs::DENM_TRANSMISSION, {message}});
            }
            return events;
        }
    };

    template<class Types, class Parameters, class StateType>
    struct TransmitTransitionNewDENMMessageDownlink : TransmitStateMachine<Types, Parameters, StateType>::Transition_t {
        TransmitTransitionNewDENMMessageDownlink(
                const std::shared_ptr <TransmitStateMachine<Types, Parameters, StateType>> &machine)
                :
                TransmitStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        DENMMessage message;

        void action(const artis::traffic::core::Time &t, const artis::common::event::Value &value) override {
            value(message);

            this->_machine->state_()._waiting_denm_message = message;

        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::NEW_DENM_MESSAGE_DOWNLINK;
        }

        typename TransmitStateMachine<Types, Parameters, StateType>::Events
        output(const artis::traffic::core::Time & /* t */) const override {
            typename TransmitStateMachine<Types, Parameters, StateType>::Events events{};

            events.externals.push_back(
                    typename TransmitStateMachine<Types, Parameters,
                            StateType>::ExternalEvent{SignalProcessingEventIDs::ASK_VEHICLE_LIST});
            return events;
        }

    };

    template<class Types, class Parameters, class StateType>
    struct TransmitTransitionGetVehicleList : TransmitStateMachine<Types, Parameters, StateType>::Transition_t {
        TransmitTransitionGetVehicleList(
                const std::shared_ptr <TransmitStateMachine<Types, Parameters, StateType>> &machine)
                :
                TransmitStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        std::vector<typename Types::vehicle_type> vehicle_list;
        DENMMessage base_denm_message;
        std::vector<DENMMessage> denm_messages;
        CAMMessage base_cam_message;
        std::vector<CAMMessage> cam_messages;
        bool is_denm;

        void action(const artis::traffic::core::Time &t, const artis::common::event::Value &value) override {
            vehicle_list.clear();
            value(vehicle_list);
            this->_machine->state_()._vehicle_list = vehicle_list;
            is_denm = false;

            if (this->_machine->state_()._waiting_denm_message.message_id != "") {
                is_denm = true;
                uint cell_vehicle_count = 0;
                for (auto vehicle: this->_machine->state_()._vehicle_list) {
                    if (vehicle.data().serving_cell.cell_id == this->_machine->state_()._waiting_denm_message.data.serving_cell.cell_id) {
                        cell_vehicle_count++;
                    }
                }
                base_denm_message = this->_machine->state_()._waiting_denm_message;
                for (auto vehicle: this->_machine->state_()._vehicle_list) {
                    base_denm_message = this->_machine->state_()._waiting_denm_message;
                    DENMMessage denm_message = base_denm_message;

                    int coordinates_index =
                            this->_machine->change_coordinates(
                                    this->_machine->state_()._vehicles[vehicle.index()]);

                    Cell serving_cell = vehicle.data().serving_cell;

                    bool vehicle_using_relay = false;
                    for (auto cell : this->_machine->_cells[denm_message.in_port_index - 1]) {
                        if (cell.cell_id == serving_cell.cell_id) {
                            vehicle_using_relay = true;
                        }
                    }

                    if (vehicle_using_relay) {
                        denm_message.next_time += this->_machine->transmission(denm_message, denm_message.in_port_index,
                                                                               denm_message.vehicle_count);
                        denm_message.in_port_index = 0;

                        this->_machine->state_()._denm_messages.push_back(denm_message);
                    }
                    this->_machine->_coordinates[coordinates_index] = Coordinates{-1, -1};
                }
            }

            if (this->_machine->state_()._waiting_cam_message.message_id != "") {
                uint cell_vehicle_count = 0;
                for (auto vehicle: this->_machine->state_()._vehicle_list) {
                    if (vehicle.data().serving_cell.cell_id == this->_machine->state_()._waiting_cam_message.data.serving_cell.cell_id) {
                        cell_vehicle_count++;
                    }
                }
                base_cam_message = this->_machine->state_()._waiting_cam_message;
                for (auto vehicle: this->_machine->state_()._vehicle_list) {
                    base_cam_message = this->_machine->state_()._waiting_cam_message;
                    CAMMessage cam_message = base_cam_message;

                    int coordinates_index =
                            this->_machine->change_coordinates(
                                    this->_machine->state_()._vehicles[vehicle.index()]);


                    Cell serving_cell = vehicle.data().serving_cell;

                    bool vehicle_using_relay = false;
                    for (auto cell : this->_machine->_cells[cam_message.in_port_index - 1]) {
                        if (cell.cell_id == serving_cell.cell_id) {
                            vehicle_using_relay = true;
                        }
                    }

                    if (vehicle_using_relay) {
                        cam_message.next_time += this->_machine->transmission(cam_message, cam_message.in_port_index,
                                                                              cell_vehicle_count);
                        cam_message.in_port_index = 0;

                        this->_machine->state_()._cam_messages.push_back(cam_message);
                    }
                    this->_machine->_coordinates[coordinates_index] = Coordinates{-1, -1};
                }
            }
            this->_machine->state_()._waiting_denm_message = DENMMessage();
            this->_machine->state_()._waiting_cam_message = CAMMessage();
        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::GET_VEHICLE_LIST;
        }

        typename TransmitStateMachine<Types, Parameters, StateType>::Events
        output(const artis::traffic::core::Time & /* t */) const override {
            typename TransmitStateMachine<Types, Parameters, StateType>::Events events{};
            if (is_denm) {
                events.externals.push_back(
                        typename TransmitStateMachine<Types, Parameters,
                                StateType>::ExternalEvent{SignalProcessingEventIDs::BROADCAST_DENM_MESSAGE_RELAY,
                                                          base_denm_message});
            }
//            events.externals.push_back(
//                    typename TransmitStateMachine<Types, Parameters,
//                            StateType>::ExternalEvent{SignalProcessingEventIDs::RECEIVING, {message}});
            return events;
        }

    };

    template<class Types, class Parameters, class StateType>
    struct TransmitTransitionBroadcastDENMMessageRelay : TransmitStateMachine<Types, Parameters, StateType>::Transition_t {
        TransmitTransitionBroadcastDENMMessageRelay(
                const std::shared_ptr <TransmitStateMachine<Types, Parameters, StateType>> &machine)
                :
                TransmitStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        DENMMessage message;

        void action(const artis::traffic::core::Time &t, const artis::common::event::Value &value) override {
            value(message);

            uint cell_vehicle_count = 0;
            for (auto vehicle: this->_machine->state_()._vehicle_list) {
                if (vehicle.data().serving_cell.cell_id == message.data.serving_cell.cell_id) {
                    cell_vehicle_count++;
                }
            }
            for (auto vehicle: this->_machine->state_()._vehicle_list) {
                //base_denm_message = this->_machine->state_()._waiting_denm_message;
                DENMMessage denm_message = message;

                int coordinates_index =
                        this->_machine->change_coordinates(
                                this->_machine->state_()._vehicles[vehicle.index()]);

                Cell serving_cell = vehicle.data().serving_cell;

                bool vehicle_using_relay = false;
                for (auto cell : this->_machine->_cells[denm_message.in_port_index - 1]) {
                    if (cell.cell_id == serving_cell.cell_id) {
                        vehicle_using_relay = true;
                    }
                }

                if (vehicle_using_relay) {
                    denm_message.next_time += this->_machine->transmission(denm_message, denm_message.in_port_index,
                                                                           denm_message.vehicle_count);
                    denm_message.in_port_index = 0;

                    this->_machine->state_()._denm_messages.push_back(denm_message);
                }
                this->_machine->_coordinates[coordinates_index] = Coordinates{-1, -1};
            }

        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::BROADCAST_DENM_MESSAGE_DOWNLINK;
        }

//        typename TransmitStateMachine<Types, Parameters, StateType>::Events
//        output(const artis::traffic::core::Time & /* t */) const override {
//            typename TransmitStateMachine<Types, Parameters, StateType>::Events events{};
//
//            events.externals.push_back(
//                    typename TransmitStateMachine<Types, Parameters,
//                            StateType>::ExternalEvent{SignalProcessingEventIDs::ASK_VEHICLE_LIST});
//            return events;
//        }

    };

    template<class Types, class Parameters, class StateType>
    struct TransmitTransitionTransmission : TransmitStateMachine<Types, Parameters, StateType>::Transition_t {
        TransmitTransitionTransmission(
                const std::shared_ptr <TransmitStateMachine<Types, Parameters, StateType>> &machine) :
                TransmitStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        std::vector <MeasureReportMessage> waked_messages;
        std::vector <DENMMessage> waked_denm_messages;
        std::vector <CAMMessage> waked_cam_messages;

        void action(const artis::traffic::core::Time &t, const artis::common::event::Value &value) override {
            waked_messages = this->_machine->waked_messages(t);
            waked_denm_messages = this->_machine->waked_denm_messages(t);
            waked_cam_messages = this->_machine->waked_cam_messages(t);
            for (auto &message: waked_messages) {
                auto it = std::find(this->_machine->state_()._measure_messages.begin(),
                                    this->_machine->state_()._measure_messages.end(), message);
                if (it != this->_machine->state_()._measure_messages.end())
                    this->_machine->state_()._measure_messages.erase(it);
            }
            for (auto &message: waked_denm_messages) {
                this->_machine->state_()._sent_denm_messages.push_back(message);
                auto it = std::find(this->_machine->state_()._denm_messages.begin(),
                                    this->_machine->state_()._denm_messages.end(), message);
                if (it != this->_machine->state_()._denm_messages.end())
                    this->_machine->state_()._denm_messages.erase(it);
            }
            for (auto &message: waked_cam_messages) {
                this->_machine->state_()._sent_cam_messages.push_back(message);
                auto it = std::find(this->_machine->state_()._cam_messages.begin(),
                                    this->_machine->state_()._cam_messages.end(), message);
                if (it != this->_machine->state_()._cam_messages.end())
                    this->_machine->state_()._cam_messages.erase(it);
            }
        }

        bool no_event() const override { return true; }

        typename TransmitStateMachine<Types, Parameters, StateType>::Events
        output(const artis::traffic::core::Time &t) const override {
            typename TransmitStateMachine<Types, Parameters, StateType>::Events events{};
            for (auto &message: waked_messages) {
                events.externals.push_back(
                        typename TransmitStateMachine<Types, Parameters,
                                StateType>::ExternalEvent{SignalProcessingEventIDs::TRANSMISSION, {message}});
                // going to link, so downlink message
//                if (message.in_port_index == 0) {
//                    events.externals.push_back(
//                            typename TransmitStateMachine<Types, Parameters,
//                                    StateType>::ExternalEvent{SignalProcessingEventIDs::NOT_RECEIVING, {message}});
//                }
            }
            for (auto &message: waked_denm_messages) {
                events.externals.push_back(
                        typename TransmitStateMachine<Types, Parameters,
                                StateType>::ExternalEvent{SignalProcessingEventIDs::DENM_TRANSMISSION, {message}});
                // going to link, so downlink message
                if (message.in_port_index == 0) {
//                    events.externals.push_back(
//                            typename TransmitStateMachine<Types, Parameters,
//                                    StateType>::ExternalEvent{SignalProcessingEventIDs::NOT_RECEIVING, {message}});
                }
            }
            for (auto &message: waked_cam_messages) {
                events.externals.push_back(
                        typename TransmitStateMachine<Types, Parameters,
                                StateType>::ExternalEvent{SignalProcessingEventIDs::SEND_CAM_MESSAGE, {message}});
                // going to link, so downlink message
//                if (message.in_port_index == 0) {
//                    events.externals.push_back(
//                            typename TransmitStateMachine<Types, Parameters,
//                                    StateType>::ExternalEvent{SignalProcessingEventIDs::NOT_RECEIVING, {message}});
//                }
            }
            return events;
        }
    };

    template<class Types, class Parameters, class StateType>
    void TransmitStateMachine<Types, Parameters, StateType>::build(
            const std::shared_ptr <TransmitStateMachine<Types, Parameters, StateType>> &machine) {
        this->state(new InitTransmitState(machine));
        this->state(new WaitTransmitState(machine));
        this->initial_state(SignalProcessingStateIDs::INIT_TRANSMIT);
        this->initial_state(SignalProcessingStateIDs::WAIT_TRANSMIT);
        this->transition(SignalProcessingStateIDs::INIT_TRANSMIT, SignalProcessingStateIDs::WAIT_TRANSMIT,
                         new TransmitTransitionInit(machine));
        this->transition(SignalProcessingStateIDs::WAIT_TRANSMIT, SignalProcessingStateIDs::WAIT_TRANSMIT,
                         new TransmitTransitionNewMessage(machine));
        this->transition(SignalProcessingStateIDs::WAIT_TRANSMIT, SignalProcessingStateIDs::WAIT_TRANSMIT,
                         new TransmitTransitionTransmission(machine));
        this->transition(SignalProcessingStateIDs::WAIT_TRANSMIT, SignalProcessingStateIDs::WAIT_TRANSMIT,
                         new TransmitTransitionNewCAMSignal(machine));
        this->transition(SignalProcessingStateIDs::WAIT_TRANSMIT, SignalProcessingStateIDs::WAIT_TRANSMIT,
                         new TransmitTransitionNewCAMMessageUplink(machine));
        this->transition(SignalProcessingStateIDs::WAIT_TRANSMIT, SignalProcessingStateIDs::WAIT_TRANSMIT,
                         new TransmitTransitionNewCAMMessageDownlink(machine));
        this->transition(SignalProcessingStateIDs::WAIT_TRANSMIT, SignalProcessingStateIDs::WAIT_TRANSMIT,
                         new TransmitTransitionNewDENMSignal(machine));
        this->transition(SignalProcessingStateIDs::WAIT_TRANSMIT, SignalProcessingStateIDs::WAIT_TRANSMIT,
                         new TransmitTransitionNewDENMMessageUplink(machine));
        this->transition(SignalProcessingStateIDs::WAIT_TRANSMIT, SignalProcessingStateIDs::WAIT_TRANSMIT,
                         new TransmitTransitionNewDENMMessageDownlink(machine));
        this->transition(SignalProcessingStateIDs::WAIT_TRANSMIT, SignalProcessingStateIDs::WAIT_TRANSMIT,
                         new TransmitTransitionBroadcastDENMMessageRelay(machine));
        this->transition(SignalProcessingStateIDs::WAIT_TRANSMIT, SignalProcessingStateIDs::WAIT_TRANSMIT,
                         new TransmitTransitionGetVehicleList(machine));
    }
}
#endif //ARTIS_TRAFFIC_MICRO_5G_TRANSMITSTATEMACHINE_HPP
