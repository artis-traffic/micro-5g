/**
 * @file artis-traffic/micro/five_g/signal_processing-state-machine/SignalProcessingRootStateMachine.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_FIVE_G_SIGNAL_PROCESSING_STATE_MACHINE_SIGNAL_PROCESSING_ROOT_STATE_MACHINE_HPP
#define ARTIS_TRAFFIC_MICRO_FIVE_G_SIGNAL_PROCESSING_STATE_MACHINE_SIGNAL_PROCESSING_ROOT_STATE_MACHINE_HPP

#include <artis-traffic/micro/core/Data.hpp>
#include <artis-traffic/micro/five-g/signal_processing-state-machine/ActiveStateMachine.hpp>
#include <artis-traffic/micro/five-g/signal_processing-state-machine/SignalProcessingStates.hpp>
#include <artis-traffic/micro/five-g/signal_processing-state-machine/SignalProcessingStateMachine.hpp>
#include <artis-traffic/micro/five-g/signal_processing-state-machine/TransmitStateMachine.hpp>

#include <artis-star-addons/utils/RootStateMachine.hpp>

namespace artis::traffic::micro::five_g {

template<class Types, class Parameters>
using SignalProcessingStateMachineTypes = std::tuple<
        TransmitStateMachine<Types, Parameters, TransmitState<Types>>,
        ActiveStateMachine<Types, Parameters, ActiveState>
  >;

DEFINE_ROOT_STATE_MACHINE(SignalProcessingRootStateMachine, SignalProcessingStateMachineTypes)

}

#endif //ARTIS_TRAFFIC_MICRO_FIVE_G_SIGNAL_PROCESSING_STATE_MACHINE_SIGNAL_PROCESSING_ROOT_STATE_MACHINE_HPP