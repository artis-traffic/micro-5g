/**
 * @file artis-traffic/micro/five_g/JsonReader.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_JSON_READER_HPP
#define ARTIS_TRAFFIC_MICRO_5G_JSON_READER_HPP

#include <artis-traffic/micro/utils/JsonReader.hpp>

#include <artis-traffic/micro/five-g/Network.hpp>

#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/kernel/pdevs/Simulator.hpp>

#include <iostream>
#include <vector>

#include <nlohmann/json.hpp>
#include "SignalProcessing.hpp"

namespace artis::traffic::micro::five_g {

class JsonReader : public artis::traffic::micro::utils::AbstractJsonReader<Network> {
public:
  using super = artis::traffic::micro::utils::AbstractJsonReader<Network>;

  void parse_antennas(const nlohmann::json &data) {
    const nlohmann::json &antennas = data["antennas"];

    for (const nlohmann::json &antenna: antennas) {
      AntennaEntity<AntennaData> new_antenna{{antenna["id"].get<std::string>()},
                                             {}};

      new_antenna.data.parse(antenna);
      _network.antennas[new_antenna.ID] = new_antenna;
    }
  }

    void parse_relays(const nlohmann::json &data) {
        const nlohmann::json &relays = data["relays"];

        for (const nlohmann::json &relay: relays) {
            RelayEntity<RelayData> new_relay{{relay["id"].get<std::string>()},
                                                   {}};

            new_relay.data.parse(relay);
            _network.relays[new_relay.ID] = new_relay;
        }
    }


  void parse_network(const std::string &str) override {
    nlohmann::json data = nlohmann::json::parse(str);

    super::parse_network(str);
    parse_antennas(data);
    parse_relays(data);

      int signal_idx = 0;
      for (const auto &l: _network.links) {
          // int out_idx = 0;
          int i = 1;
          std::string id = "SP/" + std::to_string(signal_idx);
          std::vector<Coordinates> coordinates;
          coordinates.push_back({-1, -1});
          std::vector<std::vector<Cell>> cells;
          std::vector<std::string> relays_id;
          for (const auto &a: _network.antennas) {
              // link -> antenna

              coordinates.push_back(a.second.data.coordinates[0]);
              cells.push_back(a.second.data.cells);
          }

          for (const auto &r: _network.relays) {
              Coordinates r_c = r.second.data.coordinates[0];
              for (Coordinates c: l.second.data.coordinates) {
                  if (std::abs(c.x - r_c.x) <= 212 and std::abs(c.y - r_c.y) <= 212) {
                      coordinates.push_back(r.second.data.coordinates[0]);
                      cells.push_back(r.second.data.cells);
                      relays_id.push_back(r.second.data.id);
                      break;
                  }
              }

          }

//          if (relays_id.size() > 1) {
//              std::cout << "relay size >2 " << id << std::endl;
//          }
          SignalProcessingEntity<SignalProcessingData> new_signal_processing{{id},
                                                                             {id, coordinates, cells}};
          _network.signal_processings[new_signal_processing.ID] = new_signal_processing;
          signal_idx++;
      }
      for (const auto &l: _network.stops) {
          // int out_idx = 0;
          int i = 1;
          std::string id = "SP/" + std::to_string(signal_idx);
          std::vector<Coordinates> coordinates;
          coordinates.push_back({-1, -1});
          std::vector<std::vector<Cell>> cells;
          std::vector<std::string> relays_id;
          for (const auto &a: _network.antennas) {
              // link -> antenna

              coordinates.push_back(a.second.data.coordinates[0]);
              cells.push_back(a.second.data.cells);
          }

          for (const auto &r: _network.relays) {
              Coordinates r_c = r.second.data.coordinates[0];
              for (Coordinates c: l.second.data.coordinates) {
                  if (std::abs(c.x - r_c.x) <= 212 and std::abs(c.y - r_c.y) <= 212) {
                      coordinates.push_back(r.second.data.coordinates[0]);
                      cells.push_back(r.second.data.cells);
                      relays_id.push_back(r.second.data.id);
                      break;
                  }
              }

          }

//          if (relays_id.size() > 1) {
//              std::cout << "relay size >2 " << id << std::endl;
//          }
          SignalProcessingEntity<SignalProcessingData> new_signal_processing{{id},
                                                                             {id, coordinates, cells}};
          _network.signal_processings[new_signal_processing.ID] = new_signal_processing;
          signal_idx++;
      }
  }
};

}

#endif //ARTIS_TRAFFIC_MICRO_5G_JSON_READER_HPP
