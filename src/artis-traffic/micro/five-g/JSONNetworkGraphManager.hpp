/**
 * @file traffic/micro/five-g/JSONNetworkGraphManager.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_JSON_NETWORK_GRAPH_MANAGER_HPP
#define ARTIS_TRAFFIC_MICRO_5G_JSON_NETWORK_GRAPH_MANAGER_HPP

#include <artis-traffic/micro/five-g/JsonReader.hpp>

#include <artis-traffic/micro/utils/JSONNetworkGraphManager.hpp>
#include <artis-traffic/micro/utils/JSONNetworkView.hpp>

#include "Antenna.hpp"
#include "MicroGraphManager.hpp"
#include "VehicleFactory.hpp"
#include "Link.hpp"
#include "Relay.hpp"
#include "SignalProcessing.hpp"
#include "Stop.hpp"

namespace artis::traffic::micro::five_g {
template<class Vehicle, class VehicleEntry, class VehicleState, class Link, class LinkParameters,
  class Stop, class StopParameters, class Generator, class GeneratorParameters, class SignalProcessing, class SignalProcessingParameters,
          class Antenna, class AntennaParameters, class Relay, class RelayParameters,
  typename Time, typename Parameters = artis::common::NoParameters>
class JSONNetworkGraphManager :
  public artis::traffic::micro::utils::JSONNetworkGraphManager<Vehicle,
    VehicleEntry, VehicleState, Link, LinkParameters, Stop, StopParameters, Generator,
    GeneratorParameters, Time, Parameters, artis::traffic::micro::five_g::Network> {
private:
  using json_network_graph_manager_super = artis::traffic::micro::utils::JSONNetworkGraphManager<Vehicle,
    VehicleEntry, VehicleState, Link, LinkParameters, Stop, StopParameters, Generator,
    GeneratorParameters, Time, Parameters, artis::traffic::micro::five_g::Network>;

public:
  enum sub_models {
    ANTENNA = json_network_graph_manager_super::LAST + 1000,
    SIGNAL_PROCESSING = ANTENNA + 1000,
    RELAY = SIGNAL_PROCESSING + 1000
  };

  JSONNetworkGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                          const artis::common::NoParameters &parameters,
                          const artis::traffic::micro::five_g::Network &graph_parameters)
    : artis::traffic::micro::utils::JSONNetworkGraphManager<Vehicle,
    VehicleEntry, VehicleState, Link, LinkParameters, Stop, StopParameters, Generator,
    GeneratorParameters, Time, Parameters, artis::traffic::micro::five_g::Network>(coordinator,
                                                                                   parameters,
                                                                                   graph_parameters) {
      add_antennas(graph_parameters);
      add_relays(graph_parameters);
      int signal_idx = 0;
    for (const auto &l: graph_parameters.links) {
      // int out_idx = 0;
        int i = 1;
        std::string id = "SP/" + std::to_string(signal_idx);
//        std::vector<Coordinates> coordinates;
//        coordinates.push_back({-1, -1});
//        std::vector<std::vector<Cell>> cells;
        std::vector<std::string> relays_id;
//        for (const auto &a: graph_parameters.antennas) {
//            // link -> antenna
//
//            coordinates.push_back(a.second.data.coordinates[0]);
//            cells.push_back(a.second.data.cells);
//        }
//
        for (const auto &r: graph_parameters.relays) {
            Coordinates r_c = r.second.data.coordinates[0];
            for (Coordinates c: l.second.data.coordinates) {
                if (std::abs(c.x - r_c.x) <= 212 and std::abs(c.y - r_c.y) <= 212) {
//                    coordinates.push_back(r.second.data.coordinates[0]);
//                    cells.push_back(r.second.data.cells);
                    relays_id.push_back(r.second.data.id);
                    break;
                }
            }

        }

//        if (relays_id.size() > 1) {
//            std::cout<<"relay size >2 "<< id <<std::endl;
//        }
        std::vector<Coordinates> coordinates = graph_parameters.signal_processings.at(id).data.coordinates;
        std::vector<std::vector<Cell>> cells = graph_parameters.signal_processings.at(id).data.cells;
        five_g::SignalProcessingParameters signal_processing_parameters{coordinates, cells};
        _signal_processings[id] =
                new SignalProcessingSimulator("signal_processing_" + id, signal_processing_parameters);
        this->add_child(SIGNAL_PROCESSING + signal_idx, this->_signal_processings[id]);
        this->connect_link_to_signal_processing(*this->_links[l.first], *_signal_processings[id]);
      for (const auto &a: graph_parameters.antennas) {
        // antenna -> signal_processing

          this->connect_antenna_to_signal_processing(*this->_antennas[a.first], *_signal_processings[id], signal_idx, i);
          i++;
      }

      for (const auto &r: graph_parameters.relays) {
          // relay -> signal_processing
          for (auto r_id : relays_id) {
              if (r_id == r.second.data.id) {
                  this->connect_relay_to_signal_processing(*this->_relays[r.first], *_signal_processings[id], signal_idx, i);
                  i++;
              }
          }
      }
    signal_idx++;
    }
      for (const auto &l: graph_parameters.stops) {
          int i = 1;
          std::string id = "SP/" + std::to_string(signal_idx);
          std::vector<std::string> relays_id;

          for (const auto &r: graph_parameters.relays) {
              Coordinates r_c = r.second.data.coordinates[0];
              for (Coordinates c: l.second.data.coordinates) {
                  if (std::abs(c.x - r_c.x) <= 210 and std::abs(c.y - r_c.y) <= 210) {
                      relays_id.push_back(r.second.data.id);
                      break;
                  }
              }

          }

//          if (relays_id.size() > 1) {
//              std::cout<<"relay size >2 "<< id <<std::endl;
//          }
          std::vector<Coordinates> coordinates = graph_parameters.signal_processings.at(id).data.coordinates;
          std::vector<std::vector<Cell>> cells = graph_parameters.signal_processings.at(id).data.cells;
          five_g::SignalProcessingParameters signal_processing_parameters{coordinates, cells};
          _signal_processings[id] =
                  new SignalProcessingSimulator("signal_processing_" + id, signal_processing_parameters);
          this->add_child(SIGNAL_PROCESSING + signal_idx, this->_signal_processings[id]);
          this->connect_stop_to_signal_processing(*this->_stops[l.first], *_signal_processings[id]);
          for (const auto &a: graph_parameters.antennas) {
              // antenna -> signal_processing

              this->connect_antenna_to_signal_processing(*this->_antennas[a.first], *_signal_processings[id], signal_idx, i);
              i++;
          }

          for (const auto &r: graph_parameters.relays) {
              // relay -> signal_processing
              for (auto r_id : relays_id) {
                  if (r_id == r.second.data.id) {
                      this->connect_relay_to_signal_processing(*this->_relays[r.first], *_signal_processings[id], signal_idx, i);
                      i++;
                  }
              }
          }
          signal_idx++;
      }
  }

  ~JSONNetworkGraphManager() override {
    for (const auto &p: _antennas) { delete p.second; }
    for (const auto &p: _signal_processings) { delete p.second; }
    for (const auto &p: _relays) { delete p.second; }
  }

  void add_links(const artis::traffic::micro::five_g::Network &graph_parameters) override {// links
    int i = 0;

    for (const auto &p: graph_parameters.links) {
      five_g::LinkParameters link_parameters{{p.second.data.max_speed,
                                              p.second.data.length,
                                              p.second.data.downstream_link_number,
                                              p.second.data.concurrent_link_number,
                                              p.second.data.concurrent_stop_number,
                                              p.second.data.priorities},
                                             p.second.data.coordinates};

      this->_links[p.first] = new typename json_network_graph_manager_super::LinkSimulator("link_" + p.first,
                                                                                           link_parameters);
      this->add_child(json_network_graph_manager_super::LINK + i, this->_links[p.first]);
      i++;
    }
  }

    void add_stops(const artis::traffic::micro::five_g::Network &graph_parameters) override {// stops
        int i = 0;

        for (const auto &p: graph_parameters.stops) {
            five_g::StopParameters stop_parameters{{p.second.data.exits_links_speeds,
                                                   p.second.data.concurrent_link_number,
                                                   p.second.data.concurrent_stop_number,
                                                   p.second.data.priorities},
                                                   p.second.data.coordinates};

            this->_stops[p.first] = new typename json_network_graph_manager_super::StopSimulator("stop_" + p.first,
                                                                                                 stop_parameters);
            this->add_child(json_network_graph_manager_super::STOP + i, this->_stops[p.first]);
            i++;
        }
    }

    void add_antennas(const artis::traffic::micro::five_g::Network &graph_parameters) {// antennas
        int i = 0;

        for (const auto &p: graph_parameters.antennas) {
            five_g::AntennaParameters antenna_parameters{
                                                   p.second.data.coordinates,
                                                   static_cast<uint>(graph_parameters.links.size() + graph_parameters.stops.size()),
                                                    p.second.data.cells};

            _antennas[p.first] = new AntennaSimulator("antenna_" + p.first, antenna_parameters);
            this->add_child(ANTENNA + i, this->_antennas[p.first]);
            i++;
        }
    }

    void add_relays(const artis::traffic::micro::five_g::Network &graph_parameters) {// relays
        int i = 0;

        for (const auto &p: graph_parameters.relays) {
            five_g::RelayParameters relay_parameters{
                    p.second.data.coordinates,
                    static_cast<uint>(graph_parameters.links.size() + graph_parameters.stops.size()),
                    p.second.data.cells};

            _relays[p.first] = new RelaySimulator("relay_" + p.first, relay_parameters);
            this->add_child(RELAY + i, this->_relays[p.first]);
            i++;
        }
    }


  void connect_link_to_antenna(artis::pdevs::Simulator<artis::common::DoubleTime, Link, LinkParameters> &link,
                               artis::pdevs::Simulator<artis::common::DoubleTime, Antenna,
                                 AntennaParameters> &antenna) {
    this->out({&link, Link::outputs::ANTENNA_MESSAGE})
      >> this->in({&antenna, Antenna::inputs::MESSAGE});
  }

    void connect_link_to_signal_processing(artis::pdevs::Simulator<artis::common::DoubleTime, Link, LinkParameters> &link,
                                 artis::pdevs::Simulator<artis::common::DoubleTime, SignalProcessing,
                                         SignalProcessingParameters> &signal_processing) {
        this->out({&link, Link::outputs::ANTENNA_MESSAGE})
                >> this->in({&signal_processing, SignalProcessing::inputs::IN});
        this->out({&signal_processing, SignalProcessing::outputs::OUT})
                >> this->in({&link, Link::inputs::ANTENNA_MESSAGE});

        this->out({&link, Link::outputs::CAM_SIGNAL})
                >> this->in({&signal_processing, SignalProcessing::inputs::CAM_SIGNAL});
        this->out({&signal_processing, SignalProcessing::outputs::CAM_SIGNAL})
                >> this->in({&link, Link::inputs::CAM_SIGNAL});

        this->out({&link, Link::outputs::CAM_MESSAGE})
                >> this->in({&signal_processing, SignalProcessing::inputs::CAM_MESSAGE});
        this->out({&signal_processing, SignalProcessing::outputs::CAM_MESSAGE})
                >> this->in({&link, Link::inputs::CAM_MESSAGE});

        this->out({&link, Link::outputs::DENM_SIGNAL})
                >> this->in({&signal_processing, SignalProcessing::inputs::DENM_SIGNAL});
        this->out({&signal_processing, SignalProcessing::outputs::DENM_SIGNAL})
                >> this->in({&link, Link::inputs::DENM_SIGNAL});
        
        this->out({&link, Link::outputs::DENM_MESSAGE})
                >> this->in({&signal_processing, SignalProcessing::inputs::DENM_MESSAGE});
        this->out({&signal_processing, SignalProcessing::outputs::DENM_MESSAGE})
                >> this->in({&link, Link::inputs::DENM_MESSAGE});

        this->out({&link, Link::outputs::VEHICLE_LIST})
                >> this->in({&signal_processing, SignalProcessing::inputs::VEHICLE_LIST});
        this->out({&signal_processing, SignalProcessing::outputs::VEHICLE_LIST})
                >> this->in({&link, Link::inputs::VEHICLE_LIST});
    }

    void connect_stop_to_signal_processing(artis::pdevs::Simulator<artis::common::DoubleTime, Stop, StopParameters> &stop,
                                           artis::pdevs::Simulator<artis::common::DoubleTime, SignalProcessing,
                                                   SignalProcessingParameters> &signal_processing) {
        this->out({&stop, Stop::outputs::ANTENNA_MESSAGE})
                >> this->in({&signal_processing, SignalProcessing::inputs::IN});
        this->out({&signal_processing, SignalProcessing::outputs::OUT})
                >> this->in({&stop, Stop::inputs::ANTENNA_MESSAGE});

        this->out({&stop, Stop::outputs::CAM_SIGNAL})
                >> this->in({&signal_processing, SignalProcessing::inputs::CAM_SIGNAL});
        this->out({&signal_processing, SignalProcessing::outputs::CAM_SIGNAL})
                >> this->in({&stop, Stop::inputs::CAM_SIGNAL});

        this->out({&stop, Stop::outputs::CAM_MESSAGE})
                >> this->in({&signal_processing, SignalProcessing::inputs::CAM_MESSAGE});
        this->out({&signal_processing, SignalProcessing::outputs::CAM_MESSAGE})
                >> this->in({&stop, Stop::inputs::CAM_MESSAGE});

        this->out({&stop, Stop::outputs::DENM_SIGNAL})
                >> this->in({&signal_processing, SignalProcessing::inputs::DENM_SIGNAL});
        this->out({&signal_processing, SignalProcessing::outputs::DENM_SIGNAL})
                >> this->in({&stop, Stop::inputs::DENM_SIGNAL});
        
        this->out({&stop, Stop::outputs::DENM_MESSAGE})
                >> this->in({&signal_processing, SignalProcessing::inputs::DENM_MESSAGE});
        this->out({&signal_processing, SignalProcessing::outputs::DENM_MESSAGE})
                >> this->in({&stop, Stop::inputs::DENM_MESSAGE});
    }

    void connect_antenna_to_signal_processing(artis::pdevs::Simulator<artis::common::DoubleTime, Antenna, AntennaParameters> &antenna,
                                           artis::pdevs::Simulator<artis::common::DoubleTime, SignalProcessing,
                                                   SignalProcessingParameters> &signal_processing, uint antenna_out_index = 0,
                                                   uint signal_out_index = 0) {
        this->out({&antenna, Antenna::outputs::MESSAGE + antenna_out_index})
                >> this->in({&signal_processing, SignalProcessing::inputs::IN + signal_out_index});
        this->out({&signal_processing, SignalProcessing::outputs::OUT + signal_out_index})
                >> this->in({&antenna, Antenna::inputs::MESSAGE + antenna_out_index});

        this->out({&signal_processing, SignalProcessing::outputs::ACTIVE})
                >> this->in({&antenna, Antenna::inputs::ACTIVE + antenna_out_index});
        this->out({&antenna, Antenna::outputs::ACTIVE + antenna_out_index})
                >> this->in({&signal_processing, SignalProcessing::inputs::ACTIVE});

        this->out({&signal_processing, SignalProcessing::outputs::INACTIVE})
                >> this->in({&antenna, Antenna::inputs::INACTIVE + antenna_out_index});
        this->out({&antenna, Antenna::outputs::INACTIVE + antenna_out_index})
                >> this->in({&signal_processing, SignalProcessing::inputs::INACTIVE});

        this->out({&signal_processing, SignalProcessing::outputs::LINK_TRANSMITTING})
                >> this->in({&antenna, Antenna::inputs::LINK_TRANSMITTING + antenna_out_index});

        this->out({&antenna, Antenna::outputs::CAM_SIGNAL + antenna_out_index})
                >> this->in({&signal_processing, SignalProcessing::inputs::CAM_SIGNAL + signal_out_index});
        this->out({&signal_processing, SignalProcessing::outputs::CAM_SIGNAL + signal_out_index})
                >> this->in({&antenna, Antenna::inputs::CAM_SIGNAL + antenna_out_index});

        this->out({&signal_processing, SignalProcessing::outputs::CAM_MESSAGE + signal_out_index})
                >> this->in({&antenna, Antenna::inputs::CAM_MESSAGE + antenna_out_index});
        this->out({&antenna, Antenna::outputs::CAM_MESSAGE + antenna_out_index})
                >> this->in({&signal_processing, SignalProcessing::inputs::CAM_MESSAGE + signal_out_index});

        this->out({&antenna, Antenna::outputs::DENM_SIGNAL + antenna_out_index})
                >> this->in({&signal_processing, SignalProcessing::inputs::DENM_SIGNAL + signal_out_index});
        this->out({&signal_processing, SignalProcessing::outputs::DENM_SIGNAL + signal_out_index})
                >> this->in({&antenna, Antenna::inputs::DENM_SIGNAL + antenna_out_index});
        
        this->out({&signal_processing, SignalProcessing::outputs::DENM_MESSAGE + signal_out_index})
                >> this->in({&antenna, Antenna::inputs::DENM_MESSAGE + antenna_out_index});
        this->out({&antenna, Antenna::outputs::DENM_MESSAGE + antenna_out_index})
                >> this->in({&signal_processing, SignalProcessing::inputs::DENM_MESSAGE + signal_out_index});

        this->out({&signal_processing, SignalProcessing::outputs::DENM_BROADCAST + signal_out_index})
                >> this->in({&antenna, Antenna::inputs::DENM_BROADCAST + antenna_out_index});
        this->out({&antenna, Antenna::outputs::DENM_BROADCAST + antenna_out_index})
                >> this->in({&signal_processing, SignalProcessing::inputs::DENM_BROADCAST + signal_out_index});
    }

    void connect_relay_to_signal_processing(artis::pdevs::Simulator<artis::common::DoubleTime, Relay, RelayParameters> &relay,
                                              artis::pdevs::Simulator<artis::common::DoubleTime, SignalProcessing,
                                                      SignalProcessingParameters> &signal_processing, uint relay_out_index = 0,
                                              uint signal_out_index = 0) {
        this->out({&relay, Relay::outputs::MESSAGE + relay_out_index})
                >> this->in({&signal_processing, SignalProcessing::inputs::IN + signal_out_index});
        this->out({&signal_processing, SignalProcessing::outputs::OUT + signal_out_index})
                >> this->in({&relay, Relay::inputs::MESSAGE + relay_out_index});

        this->out({&signal_processing, SignalProcessing::outputs::ACTIVE})
                >> this->in({&relay, Relay::inputs::ACTIVE + relay_out_index});
        this->out({&relay, Relay::outputs::ACTIVE + relay_out_index})
                >> this->in({&signal_processing, SignalProcessing::inputs::ACTIVE});

        this->out({&signal_processing, SignalProcessing::outputs::INACTIVE})
                >> this->in({&relay, Relay::inputs::INACTIVE + relay_out_index});
        this->out({&relay, Relay::outputs::INACTIVE + relay_out_index})
                >> this->in({&signal_processing, SignalProcessing::inputs::INACTIVE});

        this->out({&signal_processing, SignalProcessing::outputs::LINK_TRANSMITTING})
                >> this->in({&relay, Relay::inputs::LINK_TRANSMITTING + relay_out_index});

        this->out({&relay, Relay::outputs::CAM_SIGNAL + relay_out_index})
                >> this->in({&signal_processing, SignalProcessing::inputs::CAM_SIGNAL + signal_out_index});
        this->out({&signal_processing, SignalProcessing::outputs::CAM_SIGNAL + signal_out_index})
                >> this->in({&relay, Relay::inputs::CAM_SIGNAL + relay_out_index});

        this->out({&signal_processing, SignalProcessing::outputs::CAM_MESSAGE + signal_out_index})
                >> this->in({&relay, Relay::inputs::CAM_MESSAGE + relay_out_index});
        this->out({&relay, Relay::outputs::CAM_MESSAGE + relay_out_index})
                >> this->in({&signal_processing, SignalProcessing::inputs::CAM_MESSAGE + signal_out_index});

        this->out({&relay, Relay::outputs::DENM_SIGNAL + relay_out_index})
                >> this->in({&signal_processing, SignalProcessing::inputs::DENM_SIGNAL + signal_out_index});
        this->out({&signal_processing, SignalProcessing::outputs::DENM_SIGNAL + signal_out_index})
                >> this->in({&relay, Relay::inputs::DENM_SIGNAL + relay_out_index});
        
        this->out({&signal_processing, SignalProcessing::outputs::DENM_MESSAGE + signal_out_index})
                >> this->in({&relay, Relay::inputs::DENM_MESSAGE + relay_out_index});
        this->out({&relay, Relay::outputs::DENM_MESSAGE + relay_out_index})
                >> this->in({&signal_processing, SignalProcessing::inputs::DENM_MESSAGE + signal_out_index});

        this->out({&signal_processing, SignalProcessing::outputs::DENM_BROADCAST + signal_out_index})
                >> this->in({&relay, Relay::inputs::DENM_BROADCAST + relay_out_index});
        this->out({&relay, Relay::outputs::DENM_BROADCAST + relay_out_index})
                >> this->in({&signal_processing, SignalProcessing::inputs::DENM_BROADCAST + signal_out_index});
    }

//    void connect_relay_to_signal_processing(artis::pdevs::Simulator<artis::common::DoubleTime, Relay, RelayParameters>&relay,
//                                              artis::pdevs::Simulator<artis::common::DoubleTime, SignalProcessing,
//                                                      SignalProcessingParameters> &signal_processing, uint relay_out_index = 0,
//                                                      uint signal_out_index = 0) {
//        this->out({&relay, Relay::outputs::MESSAGE + relay_out_index})
//                >> this->in({&signal_processing, SignalProcessing::inputs::IN + signal_out_index});
//        this->out({&signal_processing, SignalProcessing::outputs::OUT + signal_out_index})
//                >> this->in({&relay, Relay::inputs::MESSAGE + relay_out_index});
//    }

private:
  using AntennaSimulator = artis::pdevs::Simulator<artis::common::DoubleTime,
    Antenna, AntennaParameters>;
  typedef std::map<std::string, AntennaSimulator *> Antennas;
    using SignalProcessingSimulator = artis::pdevs::Simulator<artis::common::DoubleTime,
            SignalProcessing, SignalProcessingParameters>;
    typedef std::map<std::string, SignalProcessingSimulator *> SignalProcessings;
    using RelaySimulator = artis::pdevs::Simulator<artis::common::DoubleTime,
            Relay, RelayParameters>;
    typedef std::map<std::string, RelaySimulator *> Relays;

  Antennas _antennas;
  Relays _relays;
  SignalProcessings _signal_processings;
};

    template<class Vehicle, class VehicleEntry, class VehicleState, class Link, class LinkParameters,
            class Stop, class StopParameters, class Generator, class GeneratorParameters,
                    class SignalProcessing, class SignalProcessingParameters, class Antenna, class AntennaParameters,
                            class Relay, class RelayParameters, class Network,
            typename Time, typename Parameters = artis::common::NoParameters, typename GraphParameters = artis::common::NoParameters>
    class JSONAntennaView : public artis::traffic::core::View {
    public:
        JSONAntennaView(const Network &network) {
            int i = 0;

            for (const auto &p: network.antennas) {
//                selector("Antenna_" + p.first + ":vehicle_indexes",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Time, Parameters>::ANTENNA + i,
//                          Antenna::vars::VEHICLE_INDEXES});
//                selector("Antenna_" + p.first + ":vehicle_positions",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Time, Parameters>::ANTENNA + i,
//                          Antenna::vars::VEHICLE_POSITIONS});
//                selector("Antenna_" + p.first + ":measure_reports_id",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Time, Parameters>::ANTENNA + i,
//                          Antenna::vars::MEASURE_REPORTS_ID});
//                selector("Antenna_" + p.first + ":measure_reports_serving_cell",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Time, Parameters>::ANTENNA + i,
//                          Antenna::vars::MEASURE_REPORTS_SERVING_CELL});
//                selector("Antenna_" + p.first + ":measure_reports_neighbor_cells",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Time, Parameters>::ANTENNA + i,
//                          Antenna::vars::MEASURE_REPORTS_NEIGHBOR_CELLS});
//                selector("Antenna_" + p.first + ":average_message_delays",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna, AntennaParameters, Relay, RelayParameters, Time, Parameters>::ANTENNA + i,
//                          Antenna::vars::AVERAGE_MESSAGE_DELAYS});
//                selector("Antenna_" + p.first + ":average_cam_message_delays",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna, AntennaParameters, Relay, RelayParameters, Time, Parameters>::ANTENNA + i,
//                          Antenna::vars::AVERAGE_CAM_MESSAGE_DELAYS});
//                selector("Antenna_" + p.first + ":average_cam_message_end_delays",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna, AntennaParameters, Relay, RelayParameters, Time, Parameters>::ANTENNA + i,
//                          Antenna::vars::AVERAGE_CAM_MESSAGE_END_DELAYS});
//                selector("Antenna_" + p.first + ":average_message_delays_distance",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters, Antenna, AntennaParameters,
//                                 Time, Parameters>::ANTENNA + i,
//                          Antenna::vars::AVERAGE_MESSAGE_DELAYS_DISTANCE});
//                selector("Antenna_" + p.first + ":average_message_debits",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna, AntennaParameters, Relay, RelayParameters, Time, Parameters>::ANTENNA + i,
//                          Antenna::vars::AVERAGE_MESSAGE_DEBITS});
//                selector("Antenna_" + p.first + ":average_cam_message_debits",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna, AntennaParameters, Relay, RelayParameters, Time, Parameters>::ANTENNA + i,
//                          Antenna::vars::AVERAGE_CAM_MESSAGE_DEBITS});
//                selector("Antenna_" + p.first + ":average_message_debits_distance",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::ANTENNA + i,
//                          Antenna::vars::AVERAGE_MESSAGE_DEBITS_DISTANCE});
//                selector("Antenna_" + p.first + ":average_message_distance",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::ANTENNA + i,
//                          Antenna::vars::AVERAGE_MESSAGE_DISTANCE});
//                selector("Antenna_" + p.first + ":average_cam_message_distance",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::ANTENNA + i,
//                          Antenna::vars::AVERAGE_CAM_MESSAGE_DISTANCE});
//                selector("Antenna_" + p.first + ":cells_vehicle_count",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::ANTENNA + i,
//                          Antenna::vars::CELLS_VEHICLE_COUNT});
//                selector("Antenna_" + p.first + ":cells_message_count",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::ANTENNA + i,
//                          Antenna::vars::CELLS_MESSAGE_COUNT});
//                selector("Antenna_" + p.first + ":cells_cam_message_count",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::ANTENNA + i,
//                          Antenna::vars::CELLS_CAM_MESSAGE_COUNT});
//                selector("Antenna_" + p.first + ":cells_vehicle",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna, AntennaParameters, Relay, RelayParameters, Time, Parameters>::ANTENNA + i,
//                          Antenna::vars::CELLS_VEHICLE});
//                selector("Antenna_" + p.first + ":charge",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::ANTENNA + i,
//                          Antenna::vars::CHARGE});
//                selector("Antenna_" + p.first + ":charge_total",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Time, Parameters>::ANTENNA + i,
//                          Antenna::vars::CHARGE_TOTAL});
//                selector("Antenna_" + p.first + ":active_vehicles_cells",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::ANTENNA + i,
//                          Antenna::vars::ACTIVE_VEHICLES_CELLS});
//                selector("Antenna_" + p.first + ":transmitting_vehicles_cells",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::ANTENNA + i,
//                          Antenna::vars::TRANSMITTING_VEHICLES_CELLS});
//                selector("Antenna_" + p.first + ":cam_message_ids",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::ANTENNA + i,
//                          Antenna::vars::CAM_MESSAGE_IDS});
//                selector("Antenna_" + p.first + ":lost_messages_count",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::ANTENNA + i,
//                          Antenna::vars::LOST_MESSAGES_COUNT});
                selector("Antenna_" + p.first + ":vehicle_count",
                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
                                 Antenna, AntennaParameters, Relay, RelayParameters, Time, Parameters>::ANTENNA + i,
                          Antenna::vars::VEHICLE_COUNT});
                selector("Antenna_" + p.first + ":average_cam_message_delays_nocell",
                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
                                 Relay, RelayParameters, Relay, RelayParameters, Time, Parameters>::ANTENNA + i,
                          Antenna::vars::AVERAGE_CAM_MESSAGE_DELAYS_NOCELL});
                selector("Antenna_" + p.first + ":average_cam_message_end_delays_nocell",
                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
                                 Antenna, AntennaParameters, Relay, RelayParameters, Time, Parameters>::ANTENNA + i,
                          Antenna::vars::AVERAGE_CAM_MESSAGE_END_DELAYS_NOCELL});
                selector("Antenna_" + p.first + ":average_cam_message_debits_nocell",
                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
                                 Relay, RelayParameters, Relay, RelayParameters, Time, Parameters>::ANTENNA + i,
                          Antenna::vars::AVERAGE_CAM_MESSAGE_DEBITS_NOCELL});
                selector("Antenna_" + p.first + ":average_cam_message_distances_nocell",
                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
                                 Relay, RelayParameters, Relay, RelayParameters, Time, Parameters>::ANTENNA + i,
                          Antenna::vars::AVERAGE_CAM_MESSAGE_DISTANCES_NOCELL});
                selector("Antenna_" + p.first + ":average_denm_message_delays_nocell",
                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
                                 Relay, RelayParameters, Relay, RelayParameters, Time, Parameters>::ANTENNA + i,
                          Antenna::vars::AVERAGE_DENM_MESSAGE_DELAYS_NOCELL});
                selector("Antenna_" + p.first + ":average_denm_message_end_delays_nocell",
                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
                                 Antenna, AntennaParameters, Relay, RelayParameters, Time, Parameters>::ANTENNA + i,
                          Antenna::vars::AVERAGE_DENM_MESSAGE_END_DELAYS_NOCELL});
                selector("Antenna_" + p.first + ":average_denm_message_debits_nocell",
                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
                                 Relay, RelayParameters, Relay, RelayParameters, Time, Parameters>::ANTENNA + i,
                          Antenna::vars::AVERAGE_DENM_MESSAGE_DEBITS_NOCELL});
                selector("Antenna_" + p.first + ":cell_ids",
                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::ANTENNA + i,
                          Antenna::vars::CELL_IDS});
                i++;
            }
        }
    };

    template<class Vehicle, class VehicleEntry, class VehicleState, class Link, class LinkParameters,
            class Stop, class StopParameters, class Generator, class GeneratorParameters,
            class SignalProcessing, class SignalProcessingParameters, class Antenna, class AntennaParameters,
            class Relay, class RelayParameters, class Network,
            typename Time, typename Parameters = artis::common::NoParameters, typename GraphParameters = artis::common::NoParameters>
    class JSONRelayView : public artis::traffic::core::View {
    public:
        JSONRelayView(const Network &network) {
            int i = 0;

            for (const auto &p: network.relays) {
//                selector("Relay_" + p.first + ":vehicle_indexes",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Time, Parameters>::RELAY + i,
//                          Relay::vars::VEHICLE_INDEXES});
//                selector("Relay_" + p.first + ":vehicle_positions",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Time, Parameters>::RELAY + i,
//                          Relay::vars::VEHICLE_POSITIONS});
//                selector("Relay_" + p.first + ":measure_reports_id",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Time, Parameters>::RELAY + i,
//                          Relay::vars::MEASURE_REPORTS_ID});
//                selector("Relay_" + p.first + ":measure_reports_serving_cell",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Time, Parameters>::RELAY + i,
//                          Relay::vars::MEASURE_REPORTS_SERVING_CELL});
//                selector("Relay_" + p.first + ":measure_reports_neighbor_cells",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna, AntennaParameters, Relay, RelayParameters, Time, Parameters>::RELAY + i,
//                          Relay::vars::MEASURE_REPORTS_NEIGHBOR_CELLS});
//                selector("Relay_" + p.first + ":average_message_delays",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna, AntennaParameters, Relay, RelayParameters, Time, Parameters>::RELAY + i,
//                          Relay::vars::AVERAGE_MESSAGE_DELAYS});
//                selector("Relay_" + p.first + ":average_cam_message_delays",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Relay, RelayParameters, Relay, RelayParameters, Time, Parameters>::RELAY + i,
//                          Relay::vars::AVERAGE_CAM_MESSAGE_DELAYS});
//                selector("Relay_" + p.first + ":average_cam_message_end_delays",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna, AntennaParameters, Relay, RelayParameters, Time, Parameters>::RELAY + i,
//                          Relay::vars::AVERAGE_CAM_MESSAGE_END_DELAYS});
//                selector("Relay_" + p.first + ":average_message_delays_distance",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters, Relay, RelayParameters,
//                                 Time, Parameters>::RELAY + i,
//                          Relay::vars::AVERAGE_MESSAGE_DELAYS_DISTANCE});
//                selector("Relay_" + p.first + ":average_message_debits",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Relay, RelayParameters, Relay, RelayParameters, Time, Parameters>::RELAY + i,
//                          Relay::vars::AVERAGE_MESSAGE_DEBITS});
//                selector("Relay_" + p.first + ":average_cam_message_debits",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Relay, RelayParameters, Relay, RelayParameters, Time, Parameters>::RELAY + i,
//                          Relay::vars::AVERAGE_CAM_MESSAGE_DEBITS});
//                selector("Relay_" + p.first + ":average_denm_message_delays",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Relay, RelayParameters, Relay, RelayParameters, Time, Parameters>::RELAY + i,
//                          Relay::vars::AVERAGE_DENM_MESSAGE_DELAYS});
//                selector("Relay_" + p.first + ":average_denm_message_end_delays",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna, AntennaParameters, Relay, RelayParameters, Time, Parameters>::RELAY + i,
//                          Relay::vars::AVERAGE_DENM_MESSAGE_END_DELAYS});
//                selector("Relay_" + p.first + ":average_denm_message_debits",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Relay, RelayParameters, Relay, RelayParameters, Time, Parameters>::RELAY + i,
//                          Relay::vars::AVERAGE_DENM_MESSAGE_DEBITS});
//                selector("Relay_" + p.first + ":average_message_debits_distance",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Relay,RelayParameters,Relay,RelayParameters,Time, Parameters>::RELAY + i,
//                          Relay::vars::AVERAGE_MESSAGE_DEBITS_DISTANCE});
//                selector("Relay_" + p.first + ":average_message_distance",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Relay,RelayParameters,Relay,RelayParameters,Time, Parameters>::RELAY + i,
//                          Relay::vars::AVERAGE_MESSAGE_DISTANCE});
//                selector("Relay_" + p.first + ":average_cam_message_distance",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Relay,RelayParameters,Relay,RelayParameters,Time, Parameters>::RELAY + i,
//                          Relay::vars::AVERAGE_CAM_MESSAGE_DISTANCE});
//                selector("Relay_" + p.first + ":cells_vehicle_count",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Relay,RelayParameters,Relay,RelayParameters,Time, Parameters>::RELAY + i,
//                          Relay::vars::CELLS_VEHICLE_COUNT});
//                selector("Relay_" + p.first + ":cells_message_count",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Relay,RelayParameters,Relay,RelayParameters,Time, Parameters>::RELAY + i,
//                          Relay::vars::CELLS_MESSAGE_COUNT});
//                selector("Relay_" + p.first + ":cells_cam_message_count",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Relay,RelayParameters,Relay,RelayParameters,Time, Parameters>::RELAY + i,
//                          Relay::vars::CELLS_CAM_MESSAGE_COUNT});
//                selector("Relay_" + p.first + ":cells_vehicle",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Relay, RelayParameters, Relay, RelayParameters, Time, Parameters>::RELAY + i,
//                          Relay::vars::CELLS_VEHICLE});
//                selector("Relay_" + p.first + ":charge",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Relay,RelayParameters,Relay,RelayParameters,Time, Parameters>::RELAY + i,
//                          Relay::vars::CHARGE});
//                selector("Relay_" + p.first + ":charge_total",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Time, Parameters>::RELAY + i,
//                          Relay::vars::CHARGE_TOTAL});
//                selector("Relay_" + p.first + ":active_vehicles_cells",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Relay,RelayParameters,Relay,RelayParameters,Time, Parameters>::RELAY + i,
//                          Relay::vars::ACTIVE_VEHICLES_CELLS});
//                selector("Relay_" + p.first + ":transmitting_vehicles_cells",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Relay,RelayParameters,Relay,RelayParameters,Time, Parameters>::RELAY + i,
//                          Relay::vars::TRANSMITTING_VEHICLES_CELLS});
//                selector("Relay_" + p.first + ":cam_message_ids",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Relay,RelayParameters,Relay,RelayParameters,Time, Parameters>::RELAY + i,
//                          Relay::vars::CAM_MESSAGE_IDS});
//                selector("Relay_" + p.first + ":lost_messages_count",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Relay,RelayParameters,Relay,RelayParameters,Time, Parameters>::RELAY + i,
//                          Relay::vars::LOST_MESSAGES_COUNT});
//                selector("Relay_" + p.first + ":average_neighbor_cells_number",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna, AntennaParameters, Relay, RelayParameters, Time, Parameters>::RELAY + i,
//                          Relay::vars::AVERAGE_NEIGHBOR_CELLS_NUMBER});
                selector("Relay_" + p.first + ":vehicle_count",
                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
                                 Antenna, AntennaParameters, Relay, RelayParameters, Time, Parameters>::RELAY + i,
                          Relay::vars::VEHICLE_COUNT});
                selector("Relay_" + p.first + ":average_cam_message_delays_nocell",
                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
                                 Relay, RelayParameters, Relay, RelayParameters, Time, Parameters>::RELAY + i,
                          Relay::vars::AVERAGE_CAM_MESSAGE_DELAYS_NOCELL});
                selector("Relay_" + p.first + ":average_cam_message_end_delays_nocell",
                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
                                 Antenna, AntennaParameters, Relay, RelayParameters, Time, Parameters>::RELAY + i,
                          Relay::vars::AVERAGE_CAM_MESSAGE_END_DELAYS_NOCELL});
                selector("Relay_" + p.first + ":average_cam_message_debits_nocell",
                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
                                 Relay, RelayParameters, Relay, RelayParameters, Time, Parameters>::RELAY + i,
                          Relay::vars::AVERAGE_CAM_MESSAGE_DEBITS_NOCELL});
                selector("Relay_" + p.first + ":average_cam_message_distances_nocell",
                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
                                 Relay, RelayParameters, Relay, RelayParameters, Time, Parameters>::RELAY + i,
                          Relay::vars::AVERAGE_CAM_MESSAGE_DISTANCES_NOCELL});
                selector("Relay_" + p.first + ":average_denm_message_delays_nocell",
                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
                                 Relay, RelayParameters, Relay, RelayParameters, Time, Parameters>::RELAY + i,
                          Relay::vars::AVERAGE_DENM_MESSAGE_DELAYS_NOCELL});
                selector("Relay_" + p.first + ":average_denm_message_end_delays_nocell",
                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
                                 Antenna, AntennaParameters, Relay, RelayParameters, Time, Parameters>::RELAY + i,
                          Relay::vars::AVERAGE_DENM_MESSAGE_END_DELAYS_NOCELL});
                selector("Relay_" + p.first + ":average_denm_message_debits_nocell",
                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
                                 Relay, RelayParameters, Relay, RelayParameters, Time, Parameters>::RELAY + i,
                          Relay::vars::AVERAGE_DENM_MESSAGE_DEBITS_NOCELL});
                selector("Relay_" + p.first + ":cell_ids",
                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
                                 Antenna, AntennaParameters,Relay,RelayParameters,Time, Parameters>::RELAY + i,
                          Relay::vars::CELL_IDS});
                i++;
            }
        }
    };

    template<class Vehicle, class VehicleEntry, class VehicleState, class Link, class LinkParameters,
            class Stop, class StopParameters, class Generator, class GeneratorParameters,
            class SignalProcessing, class SignalProcessingParameters,class Antenna, class AntennaParameters,
            class Relay, class RelayParameters, class Network,
            typename Time, typename Parameters = artis::common::NoParameters, typename GraphParameters = artis::common::NoParameters>
    class JSONSignalProcessingView : public artis::traffic::core::View {
    public:
        JSONSignalProcessingView(const Network &network) {
            int i = 0;

            for (const auto &p: network.signal_processings) {
//                selector("SignalProcessing_" + p.first + ":transmitting_vehicles",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::SIGNAL_PROCESSING + i,
//                          SignalProcessing::vars::TRANSMITTING_VEHICLES});
//                selector("SignalProcessing_" + p.first + ":average_denm_delays",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::SIGNAL_PROCESSING + i,
//                          SignalProcessing::vars::AVERAGE_DENM_DELAYS});
//                selector("SignalProcessing_" + p.first + ":average_denm_debits",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::SIGNAL_PROCESSING + i,
//                          SignalProcessing::vars::AVERAGE_DENM_DEBITS});
//                selector("SignalProcessing_" + p.first + ":average_denm_distance",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::SIGNAL_PROCESSING + i,
//                          SignalProcessing::vars::AVERAGE_DENM_DISTANCE});
//                selector("SignalProcessing_" + p.first + ":cells_denm_count",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::SIGNAL_PROCESSING + i,
//                          SignalProcessing::vars::CELLS_DENM_COUNT});
//                selector("SignalProcessing_" + p.first + ":average_cam_delays",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::SIGNAL_PROCESSING + i,
//                          SignalProcessing::vars::AVERAGE_CAM_DELAYS});
                selector("SignalProcessing_" + p.first + ":average_cam_debits",
                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::SIGNAL_PROCESSING + i,
                          SignalProcessing::vars::AVERAGE_CAM_DEBITS});
//                selector("SignalProcessing_" + p.first + ":average_cam_distance",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::SIGNAL_PROCESSING + i,
//                          SignalProcessing::vars::AVERAGE_CAM_DISTANCE});
//                selector("SignalProcessing_" + p.first + ":cells_cam_count",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::SIGNAL_PROCESSING + i,
//                          SignalProcessing::vars::CELLS_CAM_COUNT});
//                selector("SignalProcessing_" + p.first + ":serving_cell_changes",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::SIGNAL_PROCESSING + i,
//                          SignalProcessing::vars::SERVING_CELL_CHANGES});
//                selector("SignalProcessing_" + p.first + ":denm_vehicle_count",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::SIGNAL_PROCESSING + i,
//                          SignalProcessing::vars::DENM_VEHICLE_COUNT});
                i++;
            }
        }
    };

    template<class Vehicle, class VehicleEntry, class VehicleState, class Link, class LinkParameters,
            class Stop, class StopParameters, class Generator, class GeneratorParameters,
            class SignalProcessing, class SignalProcessingParameters,class Antenna, class AntennaParameters,
            class Relay, class RelayParameters, class Network,
            typename Time, typename Parameters = artis::common::NoParameters, typename GraphParameters = artis::common::NoParameters>
class JSONLinkView : public artis::traffic::core::View {
    public:
        JSONLinkView(const Network &network) {
            int i = 0;

            for (const auto &p: network.links) {
                selector("Link_" + p.first + ":vehicle_number",
                         {artis::traffic::micro::utils::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, Time, Parameters>::LINK + i,
                          Link::vars::VEHICLE_NUMBER});
//                selector("Link_" + p.first + ":vehicle_positions",
//                         {artis::traffic::micro::utils::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, Time, Parameters>::LINK + i,
//                          Link::vars::VEHICLE_POSITIONS});
//                selector("Link_" + p.first + ":vehicle_speed",
//                         {artis::traffic::micro::utils::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, Time, Parameters>::LINK + i,
//                          Link::vars::VEHICLE_SPEEDS});
//                selector("Link_" + p.first + ":vehicle_indexes",
//                         {artis::traffic::micro::utils::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, Time, Parameters>::LINK + i,
//                          Link::vars::VEHICLE_INDEXES});

//                selector("Link_" + p.first + ":measure_reports_serving_cell",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Time, Parameters>::LINK + i,
//                          Link::vars::MEASURE_REPORTS_SERVING_CELL});
//                selector("Link_" + p.first + ":lost_messages_count",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Time, Parameters>::LINK + i,
//                          Link::vars::LOST_MESSAGES_COUNT});
//                selector("Link_" + p.first + ":messages_count",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::LINK + i,
//                          Link::vars::MESSAGES_COUNT});
                selector("Link_" + p.first + ":denm_messages_count",
                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::LINK + i,
                          Link::vars::DENM_MESSAGES_COUNT});
                selector("Link_" + p.first + ":average_cam_message_debits",
                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::LINK + i,
                          Link::vars::AVERAGE_CAM_MESSAGE_DEBITS});
                selector("Link_" + p.first + ":average_cam_message_delays",
                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::LINK + i,
                          Link::vars::AVERAGE_CAM_MESSAGE_DELAYS});
                selector("Link_" + p.first + ":average_denm_message_debits",
                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::LINK + i,
                          Link::vars::AVERAGE_DENM_MESSAGE_DEBITS});
                selector("Link_" + p.first + ":average_denm_message_delays",
                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::LINK + i,
                          Link::vars::AVERAGE_DENM_MESSAGE_DELAYS});
                i++;
            }
        }
    };

    template<class Vehicle, class VehicleEntry, class VehicleState, class Link, class LinkParameters,
            class Stop, class StopParameters, class Generator, class GeneratorParameters,
            class SignalProcessing, class SignalProcessingParameters,class Antenna, class AntennaParameters,
            class Relay, class RelayParameters, class Network,
            typename Time, typename Parameters = artis::common::NoParameters, typename GraphParameters = artis::common::NoParameters>
    class JSONStopView : public artis::traffic::core::View {
    public:
        JSONStopView(const Network &network) {
            int i = 0;

            for (const auto &p: network.stops) {
                selector("Stop_" + p.first + ":vehicle_number",
                         {artis::traffic::micro::utils::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
                                 Stop, StopParameters, Generator, GeneratorParameters, Time, Parameters>::STOP + i,
                          Stop::vars::VEHICLE_NUMBER});
//                selector("Stop_" + p.first + ":vehicle_index",
//                         {artis::traffic::micro::utils::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, Time, Parameters>::STOP + i,
//                          Stop::vars::VEHICLE_INDEX});
//                selector("Stop_" + p.first + ":measure_reports_serving_cell",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::STOP + i,
//                          Stop::vars::MEASURE_REPORTS_SERVING_CELL});
//                selector("Stop_" + p.first + ":lost_messages_count",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Time, Parameters>::STOP + i,
//                          Stop::vars::LOST_MESSAGES_COUNT});
//                selector("Stop_" + p.first + ":messages_count",
//                         {artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, Link, LinkParameters,
//                                 Stop, StopParameters, Generator, GeneratorParameters, SignalProcessing, SignalProcessingParameters,
//                                 Antenna,AntennaParameters,Relay,RelayParameters,Time, Parameters>::STOP + i,
//                          Stop::vars::MESSAGES_COUNT});

                i++;
            }
        }
    };
}

#endif //ARTIS_TRAFFIC_MICRO_5G_JSON_NETWORK_GRAPH_MANAGER_HPP
