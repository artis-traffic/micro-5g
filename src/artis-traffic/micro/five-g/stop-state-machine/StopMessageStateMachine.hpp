/**
 * @file artis-traffic/micro/five-g/stop-state-machine/StopMessageStateMachine.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_STOP_MESSAGE_STATE_MACHINE_HPP
#define ARTIS_TRAFFIC_MICRO_5G_STOP_MESSAGE_STATE_MACHINE_HPP

#include <artis-traffic/micro/five-g/stop-state-machine/StopIDs.hpp>
#include <artis-traffic/micro/five-g/stop-state-machine/StopStateMachine.hpp>
#include <artis-traffic/micro/five-g/stop-state-machine/StopStates.hpp>

#include <artis-traffic/micro/five-g/Data.hpp>

#include <artis-star-addons/utils/StateMachine.hpp>

#include <iostream>

namespace artis::traffic::micro::five_g {

template<class Types, class Parameters, class StateType>
class StopMessageStateMachine : public artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType> {
public:
  StopMessageStateMachine(const typename Types::root_state_machine_type &root, const Parameters &parameters)
    : artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType>(root, {typename Types::vehicle_type()}),
      _coordinates(parameters.coordinates) {}

  void build(const std::shared_ptr<StopMessageStateMachine<Types, Parameters, StateType>> &machine);

// types
  DECLARE_STATE_TRANSITION_TYPES(StopMessageStateMachine)

  const std::vector<Coordinates > _coordinates;
};

template<class Types, class Parameters, class StateType>
struct WaitStopMessageState : StopMessageStateMachine<Types, Parameters, StateType>::State_t {
  WaitStopMessageState(const std::shared_ptr<StopMessageStateMachine<Types, Parameters, StateType>> &machine) :
    StopMessageStateMachine<Types, Parameters, StateType>::State_t(StopStateIDs::WAIT_MESSAGE, machine) {}

  artis::traffic::core::Time ta(const artis::traffic::core::Time &t) const override {
    auto &vehicle = this->_machine->state_()._vehicle;

      const auto &state = this->_machine->root().template state_<core::ProcessVehicleState<Types>>(
              Types::state_machine_IDs_type::PROCESS_VEHICLE);
    if (state._arrived) {
        return std::min(vehicle.data().next_fiveg_time - t, vehicle.data().next_cam_time - t);
    }
    else {
        return artis::common::DoubleTime::infinity;
    }

  }

};

template<class Types, class Parameters, class StateType>
struct StopMessageTransitionNewVehicle : StopMessageStateMachine<Types, Parameters, StateType>::Transition_t {
  StopMessageTransitionNewVehicle(const std::shared_ptr<StopMessageStateMachine<Types, Parameters, StateType>> &machine) :
    StopMessageStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  typename Types::vehicle_type vehicle;

  void action(const artis::traffic::core::Time & /* t */, const artis::common::event::Value &value) override {
    value(vehicle);
    this->_machine->state_()._vehicle = vehicle;
  }

  bool event(const artis::traffic::core::Time & /* t */, int event) override {
    return event == Types::event_IDs_type::IN_VEHICLE_MESSAGE;
  }
};

template<class Types, class Parameters, class StateType>
struct StopMessageTransitionSendStopMessage : StopMessageStateMachine<Types, Parameters, StateType>::Transition_t {
  StopMessageTransitionSendStopMessage(const std::shared_ptr<StopMessageStateMachine<Types, Parameters, StateType>> &machine) :
    StopMessageStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  typename Types::vehicle_type waked_vehicle;

  void action(const artis::traffic::core::Time &t, const artis::common::event::Value & /* value */) override {
    waked_vehicle = this->_machine->state_()._vehicle;
    if (std::abs(t - waked_vehicle.data().next_fiveg_time) < 1e-6) {
        this->_machine->state_()._vehicle.data().next_fiveg_time += waked_vehicle.data().fiveg_timestep;
        if (this->_machine->state_()._vehicle.data().next_fiveg_time - std::ceil(this->_machine->state_()._vehicle.data().next_fiveg_time * 1e6) / 1e6 < 1e-6) {
            this->_machine->state_()._vehicle.data().next_fiveg_time = std::ceil(this->_machine->state_()._vehicle.data().next_fiveg_time * 1e6) / 1e6;
        }
        this->_machine->state_()._vehicle.data().message_count++;
    }
    else if (std::abs(t - waked_vehicle.data().next_cam_time) < 1e-6) {
        this->_machine->state_()._vehicle.data().next_cam_time += waked_vehicle.data().cam_timestep;
        if (this->_machine->state_()._vehicle.data().next_cam_time - std::ceil(this->_machine->state_()._vehicle.data().next_cam_time * 1e6) / 1e6 < 1e-6) {
            this->_machine->state_()._vehicle.data().next_cam_time = std::ceil(this->_machine->state_()._vehicle.data().next_cam_time * 1e6) / 1e6;
        }
        this->_machine->state_()._vehicle.data().message_count++;
    }

  }

  bool no_event() const override { return true; }

  typename StopMessageStateMachine<Types, Parameters, StateType>::Events
  output(const artis::traffic::core::Time & t) const override {
    typename StopMessageStateMachine<Types, Parameters, StateType>::Events events{};

    Coordinates position = this->_machine->_coordinates[0];
    this->_machine->state_()._messages_count++;
        if (std::abs(t - waked_vehicle.data().next_fiveg_time) < 1e-6) {
          Data message_data = Data{"", waked_vehicle.index(), position,
                                                             waked_vehicle.data().serving_cell};
          MeasureReportMessage message = MeasureReportMessage{"", 0, t, false, false, t, -1, 0,
                                                              waked_vehicle.data().message_count, t, message_data, {}};
          events.externals.push_back(
                  typename StopMessageStateMachine<Types, Parameters, StateType>::ExternalEvent{
                          Types::event_IDs_type::STOP_SEND_MESSAGE, message});
      }
      else if(std::abs(t - waked_vehicle.data().next_cam_time) < 1e-6) {
          Data message_data = Data{"", waked_vehicle.index(), position,
                                         waked_vehicle.data().serving_cell};
          CAMSignalMessage message = CAMSignalMessage{"", 0, t, false, false, t, -1, 0,
                                                      waked_vehicle.data().message_count, t, message_data};
          events.externals.push_back(
                  typename StopMessageStateMachine<Types, Parameters, StateType>::ExternalEvent{
                          Types::event_IDs_type::STOP_SEND_CAM_SIGNAL_MESSAGE, message});
      }

    return events;
  }
};

    template<class Types, class Parameters, class StateType>
    struct StopMessageTransitionSendCAM : StopMessageStateMachine<Types, Parameters, StateType>::Transition_t {
        StopMessageTransitionSendCAM(const std::shared_ptr<StopMessageStateMachine<Types, Parameters, StateType>> &machine) :
        StopMessageStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        CAMSignalMessage signal_message;
        CAMMessage message;

        void action(const artis::traffic::core::Time &t, const artis::common::event::Value & value) override {
            value(signal_message);
            message = CAMMessage(signal_message);

        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::STOP_CAM_SIGNAL;
        }

        typename StopMessageStateMachine<Types, Parameters, StateType>::Events
        output(const artis::traffic::core::Time & t) const override {
            typename StopMessageStateMachine<Types, Parameters, StateType>::Events events{};

            events.externals.push_back(
                    typename StopMessageStateMachine<Types, Parameters, StateType>::ExternalEvent{
                            Types::event_IDs_type::STOP_SEND_CAM_MESSAGE, message});

            return events;
        }
    };

template<class Types, class Parameters, class StateType>
struct StopMessageTransitionVehicleOut : StopMessageStateMachine<Types, Parameters, StateType>::Transition_t {
  StopMessageTransitionVehicleOut(const std::shared_ptr<StopMessageStateMachine<Types, Parameters, StateType>> &machine) :
    StopMessageStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

  uint vehicle_index;

  void action(const artis::traffic::core::Time & /* t */, const artis::common::event::Value &value) override {
    value(vehicle_index);
      if (vehicle_index == this->_machine->state_()._vehicle.index()) {
          this->_machine->state_()._vehicle = typename Types::vehicle_type();
      }
  }

  bool event(const artis::traffic::core::Time & /* t */, int event) override {
    return event == Types::event_IDs_type::OUT_VEHICLE;
  }
};

    template<class Types, class Parameters, class StateType>
    struct StopMessageTransitionEventStopMessage : StopMessageStateMachine<Types, Parameters, StateType>::Transition_t {
        StopMessageTransitionEventStopMessage(const std::shared_ptr<StopMessageStateMachine<Types, Parameters, StateType>> &machine) :
                StopMessageStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        std::vector<uint> waked_indexes;
        typename Types::vehicle_type waked_vehicle;

        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
            value(waked_indexes);
            for (auto &index : waked_indexes) {
                if (this->_machine->state_()._vehicle.index() == index) {
                    this->_machine->state_()._vehicle.data().next_fiveg_time = t;
                }
            }

            waked_vehicle = this->_machine->state_()._vehicle;
            this->_machine->state_()._vehicle.data().next_fiveg_time += waked_vehicle.data().fiveg_timestep;
            if (this->_machine->state_()._vehicle.data().next_fiveg_time - std::ceil(this->_machine->state_()._vehicle.data().next_fiveg_time * 1e6) / 1e6 < 1e-6) {
                this->_machine->state_()._vehicle.data().next_fiveg_time = std::ceil(this->_machine->state_()._vehicle.data().next_fiveg_time * 1e6) / 1e6;
            }
            this->_machine->state_()._vehicle.data().message_count++;

        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::EVENT_MESSAGE;
        }

        typename StopMessageStateMachine<Types, Parameters, StateType>::Events
        output(const artis::traffic::core::Time & t) const override {
            typename StopMessageStateMachine<Types, Parameters, StateType>::Events events{};

            Coordinates position = this->_machine->_coordinates[0];
            this->_machine->state_()._messages_count++;
            if (std::abs(t - waked_vehicle.data().next_fiveg_time) < 1e-6) {
                Data message_data = Data{"", waked_vehicle.index(), position,
                                                                   waked_vehicle.data().serving_cell};
                MeasureReportMessage message = MeasureReportMessage{"", 0, t, false, true, t, -1, 0,
                                                                    waked_vehicle.data().message_count, t, message_data, {}};
                events.externals.push_back(
                        typename StopMessageStateMachine<Types, Parameters, StateType>::ExternalEvent{
                                Types::event_IDs_type::STOP_SEND_CAM_SIGNAL_MESSAGE, message});
            }
            else if(std::abs(t - waked_vehicle.data().next_cam_time) < 1e-6) {
                Data message_data = Data{"", waked_vehicle.index(), position,
                                               waked_vehicle.data().serving_cell};
                CAMSignalMessage message = CAMSignalMessage{"", 0, t, false, true, t, -1, 0,
                                                            waked_vehicle.data().message_count, t, message_data};
                events.externals.push_back(
                        typename StopMessageStateMachine<Types, Parameters, StateType>::ExternalEvent{
                                Types::event_IDs_type::STOP_SEND_CAM_SIGNAL_MESSAGE, message});
            }
            return events;
        }
    };

    template<class Types, class Parameters, class StateType>
    struct StopMessageTransitionGetDENM : StopMessageStateMachine<Types, Parameters, StateType>::Transition_t {
        StopMessageTransitionGetDENM(const std::shared_ptr<StopMessageStateMachine<Types, Parameters, StateType>> &machine) :
                StopMessageStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}


        DENMMessage message;
        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
            value(message);
            this->_machine->state_()._denm_messages.push_back(message);
        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::STOP_GET_DENM;
        }

    };

    template<class Types, class Parameters, class StateType>
    struct StopMessageTransitionGetCAM : StopMessageStateMachine<Types, Parameters, StateType>::Transition_t {
        StopMessageTransitionGetCAM(const std::shared_ptr<StopMessageStateMachine<Types, Parameters, StateType>> &machine) :
                StopMessageStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}


        CAMMessage message;
        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
            value(message);
            this->_machine->state_()._cam_messages.push_back(message);
        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::STOP_GET_CAM_MESSAGE;
        }

    };

    template<class Types, class Parameters, class StateType>
    struct StopMessageTransitionEmpty : StopMessageStateMachine<Types, Parameters, StateType>::Transition_t {
        StopMessageTransitionEmpty(const std::shared_ptr<StopMessageStateMachine<Types, Parameters, StateType>> &machine) :
                StopMessageStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}


        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::EMPTY;
        }

    };

template<class Types, class Parameters, class StateType>
void
StopMessageStateMachine<Types, Parameters, StateType>::build(
  const std::shared_ptr<StopMessageStateMachine<Types, Parameters, StateType>> &machine) {
  this->state(new WaitStopMessageState(machine));
  this->initial_state(StopStateIDs::WAIT_MESSAGE);
  this->transition(StopStateIDs::WAIT_MESSAGE, StopStateIDs::WAIT_MESSAGE, new StopMessageTransitionNewVehicle(machine));
  this->transition(StopStateIDs::WAIT_MESSAGE, StopStateIDs::WAIT_MESSAGE, new StopMessageTransitionSendStopMessage(machine));
  this->transition(StopStateIDs::WAIT_MESSAGE, StopStateIDs::WAIT_MESSAGE, new StopMessageTransitionSendCAM(machine));
  this->transition(StopStateIDs::WAIT_MESSAGE, StopStateIDs::WAIT_MESSAGE, new StopMessageTransitionVehicleOut(machine));
  this->transition(StopStateIDs::WAIT_MESSAGE, StopStateIDs::WAIT_MESSAGE, new StopMessageTransitionEventStopMessage(machine));
  this->transition(StopStateIDs::WAIT_MESSAGE, StopStateIDs::WAIT_MESSAGE, new StopMessageTransitionGetDENM(machine));
  this->transition(StopStateIDs::WAIT_MESSAGE, StopStateIDs::WAIT_MESSAGE, new StopMessageTransitionGetCAM(machine));
  this->transition(StopStateIDs::WAIT_MESSAGE, StopStateIDs::WAIT_MESSAGE, new StopMessageTransitionEmpty(machine));
}

}
#endif //ARTIS_TRAFFIC_MICRO_5G_STOP_MESSAGE_STATE_MACHINE_HPP
