/**
 * @file artis-traffic/micro/five-g/link-state-machine/StopIDs.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_STOP_IDS_HPP
#define ARTIS_TRAFFIC_MICRO_5G_STOP_IDS_HPP

#include <artis-traffic/micro/core/stop-state-machine/StopIDs.hpp>

namespace artis::traffic::micro::five_g {

struct StopStateMachineIDs : core::StopStateMachineIDs {
  enum values {
    MESSAGE = core::StopStateMachineIDs::LAST + 1, NEW_MACHINE_2
  };
};

struct StopStateIDs : core::StopStateIDs {
  enum values {
      WAIT_MESSAGE = core::StopStateIDs::LAST + 1
  };
};

struct StopEventIDs : core::StopEventIDs {
  enum values {
      STOP_SEND_MESSAGE = core::StopEventIDs::LAST + 1, IN_VEHICLE_MESSAGE, OUT_VEHICLE, EVENT_MESSAGE, EMPTY,
      STOP_SEND_CAM_SIGNAL_MESSAGE, STOP_SEND_CAM_MESSAGE, STOP_CAM_SIGNAL, STOP_GET_DENM, STOP_GET_CAM_MESSAGE
  };
};

}

#endif //ARTIS_TRAFFIC_MICRO_5G_STOP_IDS_HPP
