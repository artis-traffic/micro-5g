/**
 * @file artis-traffic/micro/five-g/stop-state-machine/StopRootStateMachine.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_STOP_ROOT_STATE_MACHINE_HPP
#define ARTIS_TRAFFIC_MICRO_5G_STOP_ROOT_STATE_MACHINE_HPP

#include <artis-traffic/micro/core/stop-state-machine/StopRootStateMachine.hpp>

#include <artis-traffic/micro/five-g/stop-state-machine/StopIDs.hpp>
#include <artis-traffic/micro/five-g/stop-state-machine/StopStateMachine.hpp>
#include <artis-traffic/micro/five-g/stop-state-machine/StopStates.hpp>

#include <artis-traffic/micro/core/stop-state-machine/ProcessVehicleStateMachine.hpp>
#include <artis-traffic/micro/core/stop-state-machine/StopAnswerStateStateMachine.hpp>
#include <artis-traffic/micro/core/stop-state-machine/StopDownOpenCloseStateMachine.hpp>
#include <artis-traffic/micro/core/stop-state-machine/StopDownOpenCloseStateMachine.hpp>
#include <artis-traffic/micro/core/stop-state-machine/StopStateGoStateMachine.hpp>
#include <artis-traffic/micro/core/stop-state-machine/StopStates.hpp>

#include <artis-traffic/micro/five-g/stop-state-machine/ProcessVehicleStateMachine.hpp>
#include <artis-traffic/micro/five-g/stop-state-machine/StopMessageStateMachine.hpp>

namespace artis::traffic::micro::five_g {

template<class Types, class Parameters>
class StopRootStateMachine
  : public core::StopRootStateMachine<Types, Parameters> {
public:
  StopRootStateMachine() : core::StopRootStateMachine<Types, Parameters>() {}

  ~StopRootStateMachine() override = default;

  void build(const Parameters &parameters) override {
    core::StopRootStateMachine<Types, Parameters>::build(parameters);
    this->template build_machine<typename Types::root_state_machine_type, StopMessageStateMachine<Types, Parameters, StopMessageState<Types>>>(
      Types::state_machine_IDs_type::MESSAGE, parameters);

    this->machine(Types::state_machine_IDs_type::PROCESS_VEHICLE)->switch_transition(
            Types::state_IDs_type::CLOSE_WITH_VEHICLE, Types::state_IDs_type::CLOSE_WITHOUT_VEHICLE,
            0, new ProcessVehicleTransitionSendVehicleNoWaiting<Types, Parameters, core::ProcessVehicleState<Types>>(
                    std::dynamic_pointer_cast<core::ProcessVehicleStateMachine<Types, Parameters, core::ProcessVehicleState<Types>>>(
                            this->machine(Types::state_machine_IDs_type::PROCESS_VEHICLE))));

    this->machine(Types::state_machine_IDs_type::PROCESS_VEHICLE)->switch_transition(
            Types::state_IDs_type::CLOSE_WITH_VEHICLE, Types::state_IDs_type::OPEN,
            0, new ProcessVehicleTransitionSendVehicleWaiting<Types, Parameters, core::ProcessVehicleState<Types>>(
                    std::dynamic_pointer_cast<core::ProcessVehicleStateMachine<Types, Parameters, core::ProcessVehicleState<Types>>>(
                            this->machine(Types::state_machine_IDs_type::PROCESS_VEHICLE))));

  }
};

}

#endif //ARTIS_TRAFFIC_MICRO_5G_STOP_ROOT_STATE_MACHINE_HPP
