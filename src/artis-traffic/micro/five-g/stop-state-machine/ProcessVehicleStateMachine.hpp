/**
 * @file artis-traffic/micro/five-g/stop-state-machine/ProcessVehicleStateMachine.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_PROCESSVEHICLESTATEMACHINE_HPP
#define ARTIS_TRAFFIC_MICRO_5G_PROCESSVEHICLESTATEMACHINE_HPP

#include <artis-traffic/micro/core/stop-state-machine/ProcessVehicleStateMachine.hpp>

namespace artis::traffic::micro::five_g {
    template<class Types, class Parameters, class StateType>
    struct ProcessVehicleTransitionSendVehicleNoWaiting
            : core::ProcessVehicleTransitionSendVehicleNoWaiting<Types, Parameters, StateType> {
        typename Types::vehicle_type vehicle;

        ProcessVehicleTransitionSendVehicleNoWaiting(const std::shared_ptr<core::ProcessVehicleStateMachine<Types, Parameters, StateType>> &machine) :
                core::ProcessVehicleTransitionSendVehicleNoWaiting<Types, Parameters, StateType>(machine) {}

        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
            vehicle = this->_machine->state_()._vehicle;
            core::ProcessVehicleTransitionSendVehicleNoWaiting<Types, Parameters, StateType>::action(t, value);
            const auto &state = this->_machine->root().template state_<StopMessageState<Types>>(Types::state_machine_IDs_type::MESSAGE);

            if (state._vehicle.index() == this->_machine->state_()._vehicle.index()) {
                this->_machine->state_()._vehicle.data().next_fiveg_time = state._vehicle.data().next_fiveg_time;
                this->_machine->state_()._vehicle.data().next_cam_time = state._vehicle.data().next_cam_time;
            }
        }

        typename core::ProcessVehicleStateMachine<Types, Parameters, StateType>::Events
        output(const artis::traffic::core::Time & t) const override {
            typename core::ProcessVehicleStateMachine<Types, Parameters, StateType>::Events events{};

            events = core::ProcessVehicleTransitionSendVehicleNoWaiting<Types, Parameters, StateType>::output(t);

            events.internals.push_back(typename Types::state_machine_type::InternalEvent{
                    {Types::event_IDs_type::OUT_VEHICLE, this->_machine->state_()._vehicle.index()},
                    Types::state_machine_IDs_type::MESSAGE});

            return events;
        }
    };

    template<class Types, class Parameters, class StateType>
    struct ProcessVehicleTransitionSendVehicleWaiting
            : core::ProcessVehicleTransitionSendVehicleWaiting<Types, Parameters, StateType> {
        typename Types::vehicle_type vehicle;

        ProcessVehicleTransitionSendVehicleWaiting(const std::shared_ptr<core::ProcessVehicleStateMachine<Types, Parameters, StateType>> &machine) :
                core::ProcessVehicleTransitionSendVehicleWaiting<Types, Parameters, StateType>(machine) {}

        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
            vehicle = this->_machine->state_()._vehicle;
            core::ProcessVehicleTransitionSendVehicleWaiting<Types, Parameters, StateType>::action(t, value);
            const auto &state = this->_machine->root().template state_<StopMessageState<Types>>(Types::state_machine_IDs_type::MESSAGE);


            if (state._vehicle.index() == this->_machine->state_()._vehicle.index()) {
                this->_machine->state_()._vehicle.data().next_fiveg_time = state._vehicle.data().next_fiveg_time;
            }
        }

        typename core::ProcessVehicleStateMachine<Types, Parameters, StateType>::Events
        output(const artis::traffic::core::Time & t) const override {
            typename core::ProcessVehicleStateMachine<Types, Parameters, StateType>::Events events{};

            events = core::ProcessVehicleTransitionSendVehicleWaiting<Types, Parameters, StateType>::output(t);

            events.internals.push_back(typename Types::state_machine_type::InternalEvent{
                    {Types::event_IDs_type::OUT_VEHICLE, this->_machine->state_()._vehicle.index()},
                    Types::state_machine_IDs_type::MESSAGE});

//            events.externals.push_back(
//                    typename core::ProcessVehicleStateMachine<Types, Parameters, StateType>::ExternalEvent{core::StopEventIDs::SEND_VEHICLE,
//                                                                                                     vehicle});
            return events;
        }
    };
}
#endif //ARTIS_TRAFFIC_MICRO_5G_PROCESSVEHICLESTATEMACHINE_HPP
