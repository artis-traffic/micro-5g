/**
 * @file artis-traffic/micro/five_g/Network.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_NETWORK_HPP
#define ARTIS_TRAFFIC_MICRO_5G_NETWORK_HPP

#include <artis-traffic/micro/utils/Network.hpp>

#include <artis-traffic/micro/five-g/Link.hpp>
#include <artis-traffic/micro/five-g/Stop.hpp>

namespace artis::traffic::micro::five_g {

    template<typename Data>
    struct AntennaEntity : artis::traffic::micro::utils::Entity {
        Data data;
    };

    template<typename Data>
    struct RelayEntity : artis::traffic::micro::utils::Entity {
        Data data;
    };

    template<typename Data>
    struct SignalProcessingEntity : artis::traffic::micro::utils::Entity {
        Data data;
    };

    struct LinkData : artis::traffic::micro::utils::LinkData {
        std::vector<Coordinates> coordinates;

        void parse(const nlohmann::json &data) {
            artis::traffic::micro::utils::LinkData::parse(data);

            for (auto &elem: data["xy_coords"])
                coordinates.push_back(Coordinates{elem[0], elem[1]});
        }

        LinkParameters to_parameters() const {
            return {{max_speed, length, downstream_link_number, concurrent_link_number, concurrent_stop_number,
                     priorities},
                    coordinates};
        }
    };

    struct StopData : artis::traffic::micro::utils::StopData {
        std::vector<Coordinates> coordinates;

        void parse(const nlohmann::json &data) {
            artis::traffic::micro::utils::StopData::parse(data);

            for (auto &elem: data["xy_coords"])
                coordinates.push_back(Coordinates{elem[0], elem[1]});
        }

        StopParameters to_parameters() const {
            return {{exits_links_speeds, concurrent_link_number, concurrent_stop_number,
                     priorities},
                    coordinates};
        }
    };

    struct AntennaData {
        std::string id;
        std::vector<Coordinates> coordinates;
        std::vector<Cell> cells;

        void parse(const nlohmann::json &data) {
            id = data["id"].get<std::string>();
            for (auto &elem: data["xy_coords"])
                coordinates.push_back(Coordinates{elem[0], elem[1]});
            for (auto &elem: data["cells"]) {
                int frequency = elem[0];
                int orientation = elem[1];
                std::string cell_id = id + "/" + std::to_string(frequency) + "/" + std::to_string(orientation);
                cells.push_back(Cell{cell_id, frequency, orientation, 0, 0, 0});
            }
        }
    };

    struct RelayData {
        std::string id;
        std::vector<Coordinates> coordinates;
        std::vector<Cell> cells;

        void parse(const nlohmann::json &data) {
            id = data["id"].get<std::string>();
            for (auto &elem: data["xy_coords"])
                coordinates.push_back(Coordinates{elem[0], elem[1]});
            for (auto &elem: data["cells"]) {
                int frequency = elem[0];
                int orientation = elem[1];
                std::string cell_id = id + "/" + std::to_string(frequency) + "/" + std::to_string(orientation);
                cells.push_back(Cell{cell_id, frequency, orientation, 0, 0, 0});
            }
        }
    };

    struct SignalProcessingData {
        std::string id;
        std::vector<Coordinates> coordinates;
        std::vector<std::vector<Cell> > cells;
    };

    struct Network : artis::traffic::micro::utils::Network<artis::traffic::micro::utils::InputData,
            artis::traffic::micro::utils::OutputData, artis::traffic::micro::utils::NodeData,
            artis::traffic::micro::utils::JunctionData, five_g::StopData, five_g::LinkData> {
        using antenna_type = AntennaEntity<AntennaData>;
        using relay_type = RelayEntity<RelayData>;
        using signal_processing_type = SignalProcessingEntity<SignalProcessingData>;

        std::map<std::string, antenna_type> antennas;
        std::map<std::string, relay_type> relays;
        std::map<std::string, signal_processing_type> signal_processings;
    };

}

#endif //ARTIS_TRAFFIC_MICRO_5G_NETWORK_HPP
