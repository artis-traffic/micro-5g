/**
 * @file artis-traffic/micro/five-g/Link.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_LINK_HPP
#define ARTIS_TRAFFIC_MICRO_5G_LINK_HPP

#include <artis-star-addons/pdevs/StateMachineDynamics.hpp>

#include <artis-traffic/micro/core/Link.hpp>

#include <artis-traffic/micro/five-g/Data.hpp>
#include <artis-traffic/micro/five-g/Vehicle.hpp>

#include <artis-traffic/micro/five-g/link-state-machine/LinkIDs.hpp>
#include <artis-traffic/micro/five-g/link-state-machine/LinkTypes.hpp>
#include <artis-traffic/micro/five-g/link-state-machine/LinkRootStateMachine.hpp>
#include <artis-traffic/micro/five-g/link-state-machine/LinkStateMachine.hpp>

#include <cmath>
#include <algorithm>
#include "link-state-machine/LinkStates.hpp"

namespace artis::traffic::micro::five_g {

struct LinkParameters : artis::traffic::micro::core::LinkParameters {
  std::vector<Coordinates > coordinates;
};

template<class Model, class Parameters, class Types>
class LinkImplementation : public core::LinkImplementation<Model, Parameters, Types> {
public:
  struct inputs : core::LinkImplementation<Model, Parameters, Types>::inputs {
    enum values {
        ANTENNA_MESSAGE = core::LinkImplementation<Model, Parameters, Types>::inputs::LAST + 500,
        CAM_SIGNAL = ANTENNA_MESSAGE + 500,  CAM_MESSAGE = CAM_SIGNAL + 500,
        DENM_SIGNAL = CAM_MESSAGE + 500, DENM_MESSAGE = DENM_SIGNAL + 500,
        VEHICLE_LIST = DENM_MESSAGE + 500
    };
  };

  struct outputs : core::LinkImplementation<Model, Parameters, Types>::outputs {
    enum values {
      ANTENNA_MESSAGE = core::LinkImplementation<Model, Parameters, Types>::outputs::LAST + 500,
      CAM_SIGNAL = ANTENNA_MESSAGE + 500, CAM_MESSAGE = CAM_SIGNAL + 500,
      DENM_SIGNAL = CAM_MESSAGE + 500, DENM_MESSAGE = DENM_SIGNAL + 500,
      VEHICLE_LIST = DENM_MESSAGE + 500
    };
  };

  struct vars : core::LinkImplementation<Model, Parameters, Types>::vars {
    enum values {
        MEASURE_REPORTS_SERVING_CELL = core::LinkImplementation<Model, Parameters, Types>::vars::LAST + 1,
        LOST_MESSAGES_COUNT, MESSAGES_COUNT, DENM_MESSAGES_COUNT, AVERAGE_CAM_MESSAGE_DEBITS,
        AVERAGE_CAM_MESSAGE_DELAYS, AVERAGE_DENM_MESSAGE_DEBITS, AVERAGE_DENM_MESSAGE_DELAYS
    };
  };

  LinkImplementation(const std::string &name,
                     const artis::pdevs::Context<artis::common::DoubleTime, Model, Parameters> &context) :
    core::LinkImplementation<Model, Parameters, Types>(name, context) {
      _messages_count = 0;
      _lost_messages_count = 0;
    // new input/output port
      this->input_port({inputs::ANTENNA_MESSAGE, "antenna_message"});
      this->input_port({inputs::CAM_SIGNAL, "cam_signal"});
      this->input_port({inputs::CAM_MESSAGE, "cam_message"});
      this->input_port({inputs::DENM_SIGNAL, "denm_signal"});
      this->input_port({inputs::DENM_MESSAGE, "denm_message"});
      this->input_port({inputs::VEHICLE_LIST, "vehicle_list"});

      this->output_port({outputs::ANTENNA_MESSAGE, "antenna_message"});
      this->output_port({outputs::CAM_SIGNAL, "cam_signal"});
      this->output_port({outputs::CAM_MESSAGE, "cam_message"});
      this->output_port({outputs::DENM_SIGNAL, "denm_signal"});
      this->output_port({outputs::DENM_MESSAGE, "denm_message"});
      this->output_port({outputs::VEHICLE_LIST, "vehicle_list"});

    // new observables
      this->observables({{vars::MEASURE_REPORTS_SERVING_CELL,   "measure_reports_serving_cell"}});
      this->observables({{vars::LOST_MESSAGES_COUNT,   "lost_messages_count"}});
      this->observables({{vars::MESSAGES_COUNT,   "messages_count"}});
      this->observables({{vars::DENM_MESSAGES_COUNT,   "denm_messages_count"}});
      this->observables({{vars::AVERAGE_CAM_MESSAGE_DEBITS,   "average_cam_message_debits"}});
      this->observables({{vars::AVERAGE_CAM_MESSAGE_DELAYS,   "average_cam_message_delays"}});
      this->observables({{vars::AVERAGE_DENM_MESSAGE_DEBITS,   "average_denm_message_debits"}});
      this->observables({{vars::AVERAGE_DENM_MESSAGE_DELAYS,   "average_denm_message_delays"}});
  }

  ~LinkImplementation() override = default;

  void dext(const artis::traffic::core::Time &t, const artis::traffic::core::Time &e,
            const artis::traffic::core::Bag &bag) override {
    super::dext(t, e, bag);
    std::for_each(bag.begin(), bag.end(),
                  [t, this](const artis::traffic::core::ExternalEvent &event) {
                    if (event.on_port(inputs::IN)) {
                      typename Types::vehicle_type vehicle{};

                      event.data()(vehicle);

//                    std::cout << get_full_name() << " => ADD VEHICLE = " << vehicle.to_string() << " t=" << t << std::endl;

                        if (vehicle.data().next_fiveg_time - t < 1e-6) {
                            vehicle.data().next_fiveg_time = t;
                        }
                        if (vehicle.data().next_cam_time - t < 1e-6) {
                            vehicle.data().next_cam_time = t;
                        }
                      this->root().transition(t, StateMachineIDs::MESSAGE,
                                              typename Types::state_machine_type::ExternalEvent{
                                                Types::event_IDs_type::IN_VEHICLE_MESSAGE,
                                                vehicle});
                    }
                    else if (event.on_port(inputs::ANTENNA_MESSAGE)) {
                        MeasureReportMessage message;
                        event.data()(message);

//                        std::cout<<"LINK RECEIVED MESSAGE "<<message.data.measure_id<<" - "<<
//                        message.to_string()<<std::endl;
                        _measure_messages.push_back(message);

//                        std::cout << this->get_full_name() << " => LINK IN at " << t << " - " << message.to_string()
//                                  << std::endl;

                        auto state = this->root().template state_<micro::core::ProcessState<Types>>(Types::state_machine_IDs_type::PROCESS);
                        bool vehicle_found = false;
                        if (not state._vehicles.empty()) {
                            for (typename Types::vehicle_entry_type entry: state._vehicles) {
                                if (entry.vehicle().index() == message.data.vehicle_index) {
                                    entry.vehicle().data().serving_cell = message.data.serving_cell;
                                    entry.vehicle().data().neighbor_cells = message.neighbor_cells;
                                    vehicle_found = true;
                                }
                            }
                        }
                        if(!vehicle_found) {
                            _lost_messages_count++;
                        }
                        _messages_count++;
                        this->root().transition(t, StateMachineIDs::MESSAGE,
                                                typename Types::state_machine_type::ExternalEvent{
                                                        Types::event_IDs_type::GET_MEASURE_MESSAGE, message});
                    }
                    else if (event.on_port(inputs::CAM_SIGNAL)) {
                        CAMSignalMessage message;
                        event.data()(message);

//                        std::cout<<"LINK RECEIVED CAMSIGNAL t "<<t<<" " <<message.message_id<<" - "<<
//                        message.to_string()<<std::endl;

                        this->root().transition(t, StateMachineIDs::MESSAGE,
                                                typename Types::state_machine_type::ExternalEvent{
                                                        Types::event_IDs_type::CAM_SIGNAL, message});
                    }
                    else if (event.on_port(inputs::CAM_MESSAGE)) {
                        CAMMessage message;
                        event.data()(message);

//                        std::cout<<"LINK " << this->get_full_name() << " RECEIVED CAM t "<<t<<" " <<message.message_id<<" - "<<
//                                 message.to_string()<<std::endl;

                        this->root().transition(t, StateMachineIDs::MESSAGE,
                                                typename Types::state_machine_type::ExternalEvent{
                                                        Types::event_IDs_type::GET_CAM_MESSAGE, message});
                    }
                    else if (event.on_port(inputs::DENM_SIGNAL)) {
                        DENMSignalMessage message;
                        event.data()(message);

//                        std::cout<<"LINK RECEIVED DENMSIGNAL t "<<t<<" " <<message.message_id<<" - "<<
//                        message.to_string()<<std::endl;

                        this->root().transition(t, StateMachineIDs::MESSAGE,
                                                typename Types::state_machine_type::ExternalEvent{
                                                        Types::event_IDs_type::DENM_SIGNAL, message});
                    }
                    else if (event.on_port(inputs::DENM_MESSAGE)) {
                        DENMMessage message;
                        event.data()(message);

//                        std::cout<<"LINK RECEIVED DENM t "<<t<<" " <<message.message_id<<" - "<<
//                                 message.to_string()<<std::endl;

                        this->root().transition(t, StateMachineIDs::MESSAGE,
                                                typename Types::state_machine_type::ExternalEvent{
                                                        Types::event_IDs_type::GET_DENM, message});
                    }
                    else if (event.on_port(inputs::VEHICLE_LIST)) {

//                        std::cout<<"LINK RECEIVED DENM t "<<t<<" " <<message.message_id<<" - "<<
//                                 message.to_string()<<std::endl;

                        this->root().transition(t, StateMachineIDs::MESSAGE,
                                                typename Types::state_machine_type::ExternalEvent{
                                                        Types::event_IDs_type::GET_VEHICLE_LIST});
                    }
                  });
  }

  void start(const artis::traffic::core::Time &t) override {
    super::start(t);
    // TODO
  }

  artis::traffic::core::Bag lambda(const artis::traffic::core::Time &t) const override {
    artis::traffic::core::Bag bag = super::lambda(t);
    const typename Types::root_state_machine_type::external_events_type &events = this->root().outbox();

//  std::cout << "[" << this->get_full_name() << "] lambda at " << t << std::endl;

    std::for_each(events.cbegin(), events.cend(), [t, this, &bag](const auto &e) {
      switch (e.id) {
        case Types::event_IDs_type::SEND_MESSAGE: {
          MeasureReportMessage message;

          e.data(message);

//          std::cout << this->get_full_name() << " => LINK OUT at " << t << " - " << message.to_string()
//                    << std::endl;

            if (message.data.serving_cell.cell_id == "fake") {
                message.cell_change = true;
            }
            const auto &state = this->root().template state_<MessageState<Types>>(Types::state_machine_IDs_type::MESSAGE);
            message.message_id = std::to_string(message.data.vehicle_index) + "/m/" + std::to_string(message.vehicle_message_count);
            message.data.measure_id = std::to_string(message.data.vehicle_index) + "/r/" + std::to_string(message.vehicle_message_count);
          bag.push_back(artis::traffic::core::ExternalEvent(outputs::ANTENNA_MESSAGE, message));
          break;
        }
          case Types::event_IDs_type::SEND_CAM_SIGNAL_MESSAGE: {
              CAMSignalMessage message;

              e.data(message);

//          std::cout << this->get_full_name() << " => LINK CAM SIGNAL at " << t << " - " << message.to_string()
//                    << std::endl;

              if (message.data.serving_cell.cell_id == "fake") {
                  message.cell_change = true;
              }
              message.message_id = std::to_string(message.data.vehicle_index) + "/cams/" + std::to_string(message.vehicle_message_count);
              message.data.measure_id = std::to_string(message.data.vehicle_index) + "/camd/" + std::to_string(message.vehicle_message_count);
              bag.push_back(artis::traffic::core::ExternalEvent(outputs::CAM_SIGNAL, message));
              break;
          }
          case Types::event_IDs_type::SEND_CAM_MESSAGE: {
              CAMMessage message;

              e.data(message);

//              std::cout << this->get_full_name() << " => LINK SEND CAM at " << t << " - " << message.to_string()
//                        << std::endl;

              bag.push_back(artis::traffic::core::ExternalEvent(outputs::CAM_MESSAGE, message));
              break;
          }
          case Types::event_IDs_type::SEND_DENM_SIGNAL_MESSAGE: {
              DENMSignalMessage message;

              e.data(message);

//          std::cout << this->get_full_name() << " => LINK DENM SIGNAL at " << t << " - " << message.to_string()
//                    << std::endl;

              if (message.data.serving_cell.cell_id == "fake") {
                  message.cell_change = true;
              }
              message.message_id = std::to_string(message.data.vehicle_index) + "/denms/" + std::to_string(message.vehicle_message_count);
              message.data.measure_id = std::to_string(message.data.vehicle_index) + "/denmd/" + std::to_string(message.vehicle_message_count);
              bag.push_back(artis::traffic::core::ExternalEvent(outputs::DENM_SIGNAL, message));
              break;
          }
          case Types::event_IDs_type::SEND_DENM_MESSAGE: {
              DENMMessage message;

              e.data(message);

//              std::cout << this->get_full_name() << " => LINK SEND DENM at " << t << " - " << message.to_string()
//                        << std::endl;

              bag.push_back(artis::traffic::core::ExternalEvent(outputs::DENM_MESSAGE, message));
              break;
          }
          case Types::event_IDs_type::SEND_VEHICLE_LIST: {
              std::vector<typename Types::vehicle_type> vehicle_list;

              e.data(vehicle_list);

//              std::cout << this->get_full_name() << " => LINK SEND VLIST at " << t << std::endl;

              bag.push_back(artis::traffic::core::ExternalEvent(outputs::VEHICLE_LIST, vehicle_list));
              break;
          }
      }
    });
    return bag;
  }

  artis::common::event::Value observe(const artis::traffic::core::Time &t,
                                      unsigned int index) const override {
    artis::common::event::Value value = super::observe(t, index);
    const auto &message_state = this->root().template state_<MessageState<Types>>(Types::state_machine_IDs_type::MESSAGE);

    if (value.is_null()) {
      switch (index) {
          case vars::MEASURE_REPORTS_SERVING_CELL: {
              std::vector<Cell> measure_reports_info;

              if (not _measure_messages.empty()) {
                  for (const auto &measure: _measure_messages) {
                      if (measure.next_time >= t - 1) {
                          measure_reports_info.push_back(measure.data.serving_cell);
                      }
                  }
              }
              return measure_reports_info;
          }
          case vars::LOST_MESSAGES_COUNT: {
              return _lost_messages_count;
          }
          case vars::MESSAGES_COUNT: {
              return _messages_count;
          }
          case vars::DENM_MESSAGES_COUNT: {
              return message_state._denm_messages.size();
          }
          case vars::AVERAGE_CAM_MESSAGE_DEBITS: {
              double avg_debit = 0;
              if (message_state._cam_messages.size() > 0) {
                  for (const auto &message: message_state._cam_messages) {
                      avg_debit += message.debit;
                  }
                  return avg_debit / message_state._cam_messages.size();
              }
              else {
                  return 0;
              }
          }
          case vars::AVERAGE_CAM_MESSAGE_DELAYS: {
              double avg_delay = 0;
              if (message_state._cam_messages.size() > 0) {
                  for (const auto &message: message_state._cam_messages) {
                      avg_delay += message.delay;
                  }
                  return avg_delay / message_state._cam_messages.size();
              }
              else {
                  return 0;
              }
          }
          case vars::AVERAGE_DENM_MESSAGE_DEBITS: {
              double avg_debit = 0;
              if (message_state._denm_messages.size() > 0) {
                  for (const auto &message: message_state._denm_messages) {
                      avg_debit += message.debit;
                  }
                  return avg_debit / message_state._denm_messages.size();
              }
              else {
                  return 0;
              }
          }
          case vars::AVERAGE_DENM_MESSAGE_DELAYS: {
              double avg_delay = 0;
              if (message_state._denm_messages.size() > 0) {
                  for (const auto &message: message_state._denm_messages) {
                      avg_delay += message.delay;
                  }
                  return avg_delay / message_state._denm_messages.size();
              }
              else {
                  return 0;
              }
          }
        default:
            return {};
      }
    }
    return value;
  }

private:
  using super = core::LinkImplementation<Model, Parameters, Types>;
  std::vector<MeasureReportMessage> _measure_messages;
  uint _messages_count;
  uint _lost_messages_count;
};

template<class Types, class Parameters>
class Link : public LinkImplementation<Link<Types, Parameters>, Parameters, Types> {
public:
  Link(const std::string &name,
       const artis::pdevs::Context<artis::common::DoubleTime, Link<Types, Parameters>, LinkParameters> &context)
    : LinkImplementation<Link<Types, Parameters>, LinkParameters, Types>(name, context) {
  }

  ~Link() override = default;
};

}

#endif //ARTIS_TRAFFIC_MICRO_5G_LINK_HPP
