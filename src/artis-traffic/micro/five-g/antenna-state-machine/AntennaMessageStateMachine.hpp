/**
 * @file artis-traffic/micro/five-g/antenna-state-machine/MessageStateMachine.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_ANTENNAMESSAGESTATEMACHINE_HPP
#define ARTIS_TRAFFIC_MICRO_5G_ANTENNAMESSAGESTATEMACHINE_HPP

#include <artis-traffic/micro/five-g/antenna-state-machine/AntennaIDs.hpp>
#include <artis-traffic/micro/five-g/antenna-state-machine/AntennaStates.hpp>

#include <artis-traffic/micro/five-g/antenna-state-machine/AntennaStateMachine.hpp>

#include <algorithm>
#include <cmath>
#include <iostream>
#include <memory>

namespace artis::traffic::micro::five_g {
    template<class Types, class Parameters, class StateType>
    class AntennaMessageStateMachine
            : public artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType> {
    public:
        AntennaMessageStateMachine(const typename Types::root_state_machine_type &root,
                           const Parameters &parameters)
                : artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType>(root, {}),
                  _cells(parameters.cells) {}

        ~AntennaMessageStateMachine() override = default;

        void build(const std::shared_ptr<AntennaMessageStateMachine<Types, Parameters, StateType>> &machine);

        typedef typename AntennaMessageStateMachine<Types, Parameters, StateType>::template State<AntennaMessageStateMachine<Types, Parameters, StateType>> State_t;
        typedef typename AntennaMessageStateMachine<Types, Parameters, StateType>::template Transition<AntennaMessageStateMachine<Types, Parameters, StateType>> Transition_t;

        // parameters
        std::vector<Cell> _cells;
    };

    template<class Types, class Parameters, class StateType>
    struct InitAntennaMessageState : AntennaMessageStateMachine<Types, Parameters, StateType>::State_t {
        InitAntennaMessageState(
                const std::shared_ptr<AntennaMessageStateMachine<Types, Parameters, StateType>> &machine) :
        AntennaMessageStateMachine<Types, Parameters, StateType>::State_t(AntennaStateIDs::INIT,
                machine) {}

        artis::traffic::core::Time ta(const artis::traffic::core::Time &t) const override {
            return 0;
        }

    };

    template<class Types, class Parameters, class StateType>
    struct WaitAntennaMessageState : AntennaMessageStateMachine<Types, Parameters, StateType>::State_t {
        WaitAntennaMessageState(
                const std::shared_ptr<AntennaMessageStateMachine<Types, Parameters, StateType>> &machine) :
        AntennaMessageStateMachine<Types, Parameters, StateType>::State_t(AntennaStateIDs::WAIT,
                machine) {}

        artis::traffic::core::Time ta(const artis::traffic::core::Time &t) const override {
            if (not this->_machine->state_()._vehicles.empty()) {
                return std::min_element(this->_machine->state_()._vehicles.begin(), this->_machine->state_()._vehicles.end(),
                                        [](const auto &l, const auto &r) { return l.second.first < r.second.first; }
                )->second.first + this->_machine->state_()._timeout_duration - t;
            }
            return artis::common::DoubleTime::infinity;
        }

    };
    
    template<class Types, class Parameters, class StateType>
    struct AntennaMessageTransitionInit : AntennaMessageStateMachine<Types, Parameters, StateType>::Transition_t {
        AntennaMessageTransitionInit(const std::shared_ptr<AntennaMessageStateMachine<Types, Parameters, StateType>> &machine)
        :
        AntennaMessageStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
            
            std::vector<Cell> cells = this->_machine->_cells;
            this->_machine->state_()._lost_messages_count = 0;
            this->_machine->state_()._timeout_duration = 0.1;
            for(auto cell : cells) {
                this->_machine->state_()._cells_vehicles[cell.cell_id] = {};
            }

        }

        bool no_event() const override { return true; }

    };

    template<class Types, class Parameters, class StateType>
    struct AntennaMessageTransitionUpdateVehicles : AntennaMessageStateMachine<Types, Parameters, StateType>::Transition_t {
        AntennaMessageTransitionUpdateVehicles(const std::shared_ptr<AntennaMessageStateMachine<Types, Parameters, StateType>> &machine)
                :
                AntennaMessageStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {

            if (not this->_machine->state_()._vehicles.empty()) {
                auto it = this->_machine->state_()._vehicles.cbegin();
                while (it != this->_machine->state_()._vehicles.cend()) {
                    if (std::abs(it->second.first + this->_machine->state_()._timeout_duration - t) < 1e-6) {
                        this->_machine->state_()._lost_messages_count++;
                        for (auto cell: this->_machine->_cells) {
                            this->_machine->state_()._cells_vehicles[cell.cell_id].erase(std::remove(this->_machine->state_()._cells_vehicles[cell.cell_id].begin(),
                                                                                                     this->_machine->state_()._cells_vehicles[cell.cell_id].end(),
                                                                            it->first),
                                                                                         this->_machine->state_()._cells_vehicles[cell.cell_id].end());

                        }
                        this->_machine->state_()._vehicles_by_port.erase(it->first);
                        it = this->_machine->state_()._vehicles.erase(it);
                    } else {
                        it++;
                    }
                }
            }

        }

        bool no_event() const override { return true; }

    };

    template<class Types, class Parameters, class StateType>
    struct AntennaMessageTransitionNewMessage : AntennaMessageStateMachine<Types, Parameters, StateType>::Transition_t {
        AntennaMessageTransitionNewMessage(const std::shared_ptr<AntennaMessageStateMachine<Types, Parameters, StateType>> &machine)
                :
                AntennaMessageStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        MeasureReportMessagePair message_pair;
        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
            value(message_pair);
            MeasureReportMessage message = message_pair.message;
            message.end_delay = t - message.end_delay;
            this->_machine->state_()._measure_messages.push_back(message);

//                            std::cout << " => ANTENNA IN => " << message.in_port_index << " t: " << t << " " << message.data.measure_id << std::endl;

            this->_machine->state_()._vehicles[message.data.vehicle_index] = std::make_pair(message.next_time,
                                                                   message.data.serving_cell);
            this->_machine->state_()._vehicles_by_port[message.data.vehicle_index] = message_pair.port_index;

            if (message.data.serving_cell.cell_id == "fake") {
                std::cout << "still fake antenna" << std::endl;
            }

            message.delay = t - message.delay;

            std::vector<uint> cell_vehicle_indexes = this->_machine->state_()._cells_vehicles[message.data.serving_cell.cell_id];
            if (find(cell_vehicle_indexes.begin(), cell_vehicle_indexes.end(),
                     message.data.vehicle_index) != cell_vehicle_indexes.end()) {
            } else {
                this->_machine->state_()._cells_vehicles[message.data.serving_cell.cell_id].push_back(message.data.vehicle_index);
            }

            for (auto cell: this->_machine->_cells) {
                if (message.data.serving_cell.cell_id != cell.cell_id) {

                    this->_machine->state_()._cells_vehicles[cell.cell_id].erase(std::remove(this->_machine->state_()._cells_vehicles[cell.cell_id].begin(),
                                                                                             this->_machine->state_()._cells_vehicles[cell.cell_id].end(),
                                                                    message.data.vehicle_index),
                                                                                 this->_machine->state_()._cells_vehicles[cell.cell_id].end());

                }
            }

            message.cell_change = true;
            this->_machine->state_()._send_measure_messages.push_back(message_pair);

        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::NEW_ANTENNA_MESSAGE;
        }

        typename AntennaMessageStateMachine<Types, Parameters, StateType>::Events
        output(const artis::traffic::core::Time & /* t */) const override {
            typename AntennaMessageStateMachine<Types, Parameters, StateType>::Events events{};
            MeasureReportMessagePair new_message_pair = this->_machine->state_()._send_measure_messages.front();
            this->_machine->state_()._send_measure_messages.pop_front();
            events.externals.push_back(
                    typename AntennaMessageStateMachine<Types, Parameters,
                            StateType>::ExternalEvent{AntennaEventIDs::SEND_ANTENNA_MESSAGE, {new_message_pair}});
            return events;
        }

    };

    template<class Types, class Parameters, class StateType>
    void AntennaMessageStateMachine<Types, Parameters, StateType>::build(
            const std::shared_ptr<AntennaMessageStateMachine<Types, Parameters, StateType>> &machine) {
        this->state(new InitAntennaMessageState(machine));
        this->state(new WaitAntennaMessageState(machine));
        this->initial_state(AntennaStateIDs::INIT);
        this->transition(AntennaStateIDs::INIT, AntennaStateIDs::WAIT,
        new AntennaMessageTransitionInit(machine));
        this->transition(AntennaStateIDs::WAIT, AntennaStateIDs::WAIT,
        new AntennaMessageTransitionUpdateVehicles(machine));
        this->transition(AntennaStateIDs::WAIT, AntennaStateIDs::WAIT,
                         new AntennaMessageTransitionNewMessage(machine));
    }
}
#endif //ARTIS_TRAFFIC_MICRO_5G_ANTENNAMESSAGESTATEMACHINE_HPP
