/**
 * @file artis-traffic/micro/five-g/antenna-state-machine/TransmitStateMachine.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_ANTENNADENMSTATEMACHINE_HPP
#define ARTIS_TRAFFIC_MICRO_5G_ANTENNADENMSTATEMACHINE_HPP

#include <artis-traffic/micro/five-g/antenna-state-machine/AntennaIDs.hpp>
#include <artis-traffic/micro/five-g/antenna-state-machine/AntennaStates.hpp>

#include <artis-traffic/micro/five-g/antenna-state-machine/AntennaStateMachine.hpp>

#include <algorithm>
#include <cmath>
#include <iostream>
#include <memory>

namespace artis::traffic::micro::five_g {
    template<class Types, class Parameters, class StateType>
    class AntennaDENMStateMachine
            : public artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType> {
    public:
        AntennaDENMStateMachine(const typename Types::root_state_machine_type &root,
                               const Parameters &parameters)
                : artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType>(root, {}),
                  _cells(parameters.cells) {}

        ~AntennaDENMStateMachine() override = default;

        void build(const std::shared_ptr<AntennaDENMStateMachine<Types, Parameters, StateType>> &machine);

        typedef typename AntennaDENMStateMachine<Types, Parameters, StateType>::template State<AntennaDENMStateMachine<Types, Parameters, StateType>> State_t;
        typedef typename AntennaDENMStateMachine<Types, Parameters, StateType>::template Transition<AntennaDENMStateMachine<Types, Parameters, StateType>> Transition_t;

        // parameters
        std::vector<Cell> _cells;
    };

    template<class Types, class Parameters, class StateType>
    struct InitAntennaDENMState : AntennaDENMStateMachine<Types, Parameters, StateType>::State_t {
        InitAntennaDENMState(
                const std::shared_ptr<AntennaDENMStateMachine<Types, Parameters, StateType>> &machine) :
                AntennaDENMStateMachine<Types, Parameters, StateType>::State_t(AntennaStateIDs::INIT_DENM,
                                                                              machine) {}

        artis::traffic::core::Time ta(const artis::traffic::core::Time &t) const override {
            return 0;
        }

    };

    template<class Types, class Parameters, class StateType>
    struct WaitAntennaDENMState : AntennaDENMStateMachine<Types, Parameters, StateType>::State_t {
        WaitAntennaDENMState(
                const std::shared_ptr<AntennaDENMStateMachine<Types, Parameters, StateType>> &machine) :
                AntennaDENMStateMachine<Types, Parameters, StateType>::State_t(AntennaStateIDs::WAIT_DENM,
                                                                              machine) {}

        artis::traffic::core::Time ta(const artis::traffic::core::Time &t) const override {
            std::vector<artis::traffic::core::Time> cells_best;
            for (auto cell_message : this->_machine->state_()._signal_messages) {
                if (not cell_message.second.empty()) {
                    cells_best.push_back(std::min_element(cell_message.second.begin(),
                                                          cell_message.second.end(),
                                                          [](const auto &l, const auto &r) {
                                                              return l.message.next_time < r.message.next_time;
                                                          })->message.next_time);
                }
            }

            if (not cells_best.empty()) {
                double next = *std::min_element(cells_best.begin(),
                                                cells_best.end());
                return next - t;
            }
            return artis::common::DoubleTime::infinity;
        }
    };

    template<class Types, class Parameters, class StateType>
    struct AntennaDENMTransitionInit : AntennaDENMStateMachine<Types, Parameters, StateType>::Transition_t {
        AntennaDENMTransitionInit(const std::shared_ptr<AntennaDENMStateMachine<Types, Parameters, StateType>> &machine)
                :
                AntennaDENMStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {

            std::vector<Cell> cells = this->_machine->_cells;
            for (auto cell : cells) {
                this->_machine->state_()._signal_messages[cell.cell_id] = {};
            }

        }

        bool no_event() const override { return true; }

    };

    template<class Types, class Parameters, class StateType>
    struct AntennaDENMTransitionNewSignal : AntennaDENMStateMachine<Types, Parameters, StateType>::Transition_t {
        AntennaDENMTransitionNewSignal(const std::shared_ptr<AntennaDENMStateMachine<Types, Parameters, StateType>> &machine)
                :
                AntennaDENMStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        DENMSignalMessagePair message;

        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
            value(message);

            if (not this->_machine->state_()._signal_messages[message.message.data.serving_cell.cell_id].empty()) {
                message.message.next_time += std::max_element(this->_machine->state_()._signal_messages[message.message.data.serving_cell.cell_id].begin(),
                                                              this->_machine->state_()._signal_messages[message.message.data.serving_cell.cell_id].end(),
                                                              [](const auto &l, const auto &r) {
                                                                  return l.message.next_time < r.message.next_time;
                                                              })->message.next_time - t;
            }
            this->_machine->state_()._signal_messages[message.message.data.serving_cell.cell_id].push_back(message);
        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::NEW_ANTENNA_DENM_SIGNAL;
        }

    };

    template<class Types, class Parameters, class StateType>
    struct AntennaDENMTransitionSendSignal : AntennaDENMStateMachine<Types, Parameters, StateType>::Transition_t {
        AntennaDENMTransitionSendSignal(const std::shared_ptr<AntennaDENMStateMachine<Types, Parameters, StateType>> &machine)
                :
                AntennaDENMStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        std::vector<DENMSignalMessagePair> waked_messages;
        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
            waked_messages.clear();

            for (auto cell_message : this->_machine->state_()._signal_messages) {
                if (not cell_message.second.empty() and std::abs(t - cell_message.second.front().message.next_time) < 1e-6) {
                    DENMSignalMessagePair message_pair = cell_message.second.front();
                    waked_messages.push_back(message_pair);
                }
            }

            for (auto waked_message : waked_messages) {
                this->_machine->state_()._signal_messages[waked_message.message.data.serving_cell.cell_id].pop_front();
            }

        }

        bool no_event() const override { return true; }

        typename AntennaDENMStateMachine<Types, Parameters, StateType>::Events
        output(const artis::traffic::core::Time & t) const override {
            typename AntennaDENMStateMachine<Types, Parameters, StateType>::Events events{};

            for (auto message : waked_messages) {
                events.externals.push_back(
                        typename AntennaDENMStateMachine<Types, Parameters, StateType>::ExternalEvent{
                                Types::event_IDs_type::SEND_ANTENNA_DENM_SIGNAL, message});
            }
            return events;
        }
    };

    template<class Types, class Parameters, class StateType>
    struct AntennaDENMTransitionNewMessage : AntennaDENMStateMachine<Types, Parameters, StateType>::Transition_t {
        AntennaDENMTransitionNewMessage(const std::shared_ptr<AntennaDENMStateMachine<Types, Parameters, StateType>> &machine)
                :
                AntennaDENMStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        DENMMessagePair message;

        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
            value(message);

            message.message.end_delay = t - message.message.end_delay;
            const auto &state = this->_machine->root().template state_<AntennaMessageState>(
                    Types::state_machine_IDs_type::ANTENNA_MESSAGE);
            std::map<uint, uint> vehicles_by_port = state._vehicles_by_port;
            uint vehicle_count = 0;
            for(auto vehicle : vehicles_by_port) {
                if (vehicle.second == message.port_index) {
                    vehicle_count++;
                }
            }
            message.message.vehicle_count = vehicle_count > 0 ? vehicle_count : 1;

            this->_machine->state_()._denm_messages.push_back(message.message);

        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::NEW_ANTENNA_DENM_MESSAGE;
        }

        typename AntennaDENMStateMachine<Types, Parameters, StateType>::Events
        output(const artis::traffic::core::Time & t) const override {
            typename AntennaDENMStateMachine<Types, Parameters, StateType>::Events events{};


            events.externals.push_back(
                    typename AntennaDENMStateMachine<Types, Parameters, StateType>::ExternalEvent{
                            Types::event_IDs_type::SEND_ANTENNA_DENM_MESSAGE, message});
            return events;
        }
    };

    template<class Types, class Parameters, class StateType>
    struct AntennaDENMTransitionBroadcastMessage : AntennaDENMStateMachine<Types, Parameters, StateType>::Transition_t {
        AntennaDENMTransitionBroadcastMessage(const std::shared_ptr<AntennaDENMStateMachine<Types, Parameters, StateType>> &machine)
                :
                AntennaDENMStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        DENMMessagePair message;

        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
            value(message);

            message.message.end_delay = t - message.message.end_delay;
            const auto &state = this->_machine->root().template state_<AntennaMessageState>(
                    Types::state_machine_IDs_type::ANTENNA_MESSAGE);
            std::map<uint, uint> vehicles_by_port = state._vehicles_by_port;
            uint vehicle_count = 0;
            for(auto vehicle : vehicles_by_port) {
                if (vehicle.second == message.port_index) {
                    vehicle_count++;
                }
            }
            message.message.vehicle_count = vehicle_count > 0 ? vehicle_count : 1;

            this->_machine->state_()._denm_messages.push_back(message.message);

        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::BROADCAST_ANTENNA_DENM_MESSAGE;
        }

        typename AntennaDENMStateMachine<Types, Parameters, StateType>::Events
        output(const artis::traffic::core::Time & t) const override {
            typename AntennaDENMStateMachine<Types, Parameters, StateType>::Events events{};


            events.externals.push_back(
                    typename AntennaDENMStateMachine<Types, Parameters, StateType>::ExternalEvent{
                            Types::event_IDs_type::SEND_BROADCAST_ANTENNA_DENM_MESSAGE, message});
            return events;
        }
    };

    template<class Types, class Parameters, class StateType>
    void AntennaDENMStateMachine<Types, Parameters, StateType>::build(
            const std::shared_ptr<AntennaDENMStateMachine<Types, Parameters, StateType>> &machine) {
        this->state(new InitAntennaDENMState(machine));
        this->state(new WaitAntennaDENMState(machine));
        this->initial_state(AntennaStateIDs::INIT_DENM);
        this->transition(AntennaStateIDs::INIT_DENM, AntennaStateIDs::WAIT_DENM,
                         new AntennaDENMTransitionInit(machine));
        this->transition(AntennaStateIDs::WAIT_DENM, AntennaStateIDs::WAIT_DENM,
                         new AntennaDENMTransitionNewSignal(machine));
        this->transition(AntennaStateIDs::WAIT_DENM, AntennaStateIDs::WAIT_DENM,
                         new AntennaDENMTransitionSendSignal(machine));
        this->transition(AntennaStateIDs::WAIT_DENM, AntennaStateIDs::WAIT_DENM,
                         new AntennaDENMTransitionNewMessage(machine));
        this->transition(AntennaStateIDs::WAIT_DENM, AntennaStateIDs::WAIT_DENM,
                         new AntennaDENMTransitionBroadcastMessage(machine));
    }
}
#endif //ARTIS_TRAFFIC_MICRO_5G_ANTENNADENMSTATEMACHINE_HPP
