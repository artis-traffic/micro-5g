/**
 * @file artis-traffic/micro/five-g/antenna-state-machine/AntennaIDs.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_FIVE_G_ANTENNA_IDS_HPP
#define ARTIS_TRAFFIC_FIVE_G_ANTENNA_IDS_HPP

namespace artis::traffic::micro::five_g {

struct AntennaStateMachineIDs {
  enum values {
    ANTENNA_MESSAGE, ANTENNA_ACTIVE, ANTENNA_CAM, ANTENNA_DENM
  };
};

struct AntennaStateIDs {
  enum values {
    INIT, WAIT, INIT_ACTIVE, WAIT_ACTIVE, INIT_CAM, WAIT_CAM, INIT_DENM, WAIT_DENM
  };
};

struct AntennaEventIDs {
  enum values {
    NEW_ANTENNA_MESSAGE, SEND_ANTENNA_MESSAGE, NEW_ANTENNA_ACTIVE, SEND_ANTENNA_ACTIVE,
    NEW_ANTENNA_INACTIVE, SEND_ANTENNA_INACTIVE, LINK_TRANSMITTING, LINK_NOT_TRANSMITTING,
    NEW_ANTENNA_CAM_SIGNAL, SEND_ANTENNA_CAM_SIGNAL, NEW_ANTENNA_CAM_MESSAGE, SEND_ANTENNA_CAM_MESSAGE,
    NEW_ANTENNA_DENM_SIGNAL, SEND_ANTENNA_DENM_SIGNAL, NEW_ANTENNA_DENM_MESSAGE,  SEND_ANTENNA_DENM_MESSAGE,
    BROADCAST_ANTENNA_DENM_MESSAGE, SEND_BROADCAST_ANTENNA_DENM_MESSAGE
  };
};

}

#endif //ARTIS_TRAFFIC_FIVE_G_ANTENNA_IDS_HPP
