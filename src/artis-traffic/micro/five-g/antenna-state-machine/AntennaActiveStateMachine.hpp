/**
 * @file artis-traffic/micro/five-g/antenna-state-machine/ActiveStateMachine.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_ANTENNAACTIVESTATEMACHINE_HPP
#define ARTIS_TRAFFIC_MICRO_5G_ANTENNAACTIVESTATEMACHINE_HPP

#include <artis-traffic/micro/five-g/antenna-state-machine/AntennaIDs.hpp>
#include <artis-traffic/micro/five-g/antenna-state-machine/AntennaStates.hpp>

#include <artis-traffic/micro/five-g/antenna-state-machine/AntennaStateMachine.hpp>

#include <algorithm>
#include <cmath>
#include <iostream>
#include <memory>

namespace artis::traffic::micro::five_g {
    template<class Types, class Parameters, class StateType>
    class AntennaActiveStateMachine
            : public artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType> {
    public:
        AntennaActiveStateMachine(const typename Types::root_state_machine_type &root,
                                   const Parameters &parameters)
                : artis::addons::utils::StateMachine<artis::common::DoubleTime, Types, StateType>(root, {}),
                  _cells(parameters.cells) {}

        ~AntennaActiveStateMachine() override = default;

        void build(const std::shared_ptr<AntennaActiveStateMachine<Types, Parameters, StateType>> &machine);

        typedef typename AntennaActiveStateMachine<Types, Parameters, StateType>::template State<AntennaActiveStateMachine<Types, Parameters, StateType>> State_t;
        typedef typename AntennaActiveStateMachine<Types, Parameters, StateType>::template Transition<AntennaActiveStateMachine<Types, Parameters, StateType>> Transition_t;

        // parameters
        std::vector<Cell> _cells;
    };

    template<class Types, class Parameters, class StateType>
    struct InitAntennaActiveState : AntennaActiveStateMachine<Types, Parameters, StateType>::State_t {
        InitAntennaActiveState(
                const std::shared_ptr<AntennaActiveStateMachine<Types, Parameters, StateType>> &machine) :
        AntennaActiveStateMachine<Types, Parameters, StateType>::State_t(AntennaStateIDs::INIT_ACTIVE,
                machine) {}

        artis::traffic::core::Time ta(const artis::traffic::core::Time &t) const override {
            return 0;
        }

    };

    template<class Types, class Parameters, class StateType>
    struct WaitAntennaActiveState : AntennaActiveStateMachine<Types, Parameters, StateType>::State_t {
        WaitAntennaActiveState(
                const std::shared_ptr<AntennaActiveStateMachine<Types, Parameters, StateType>> &machine) :
        AntennaActiveStateMachine<Types, Parameters, StateType>::State_t(AntennaStateIDs::WAIT_ACTIVE,
                machine) {}

    };

    template<class Types, class Parameters, class StateType>
    struct AntennaActiveTransitionInit : AntennaActiveStateMachine<Types, Parameters, StateType>::Transition_t {
        AntennaActiveTransitionInit(const std::shared_ptr<AntennaActiveStateMachine<Types, Parameters, StateType>> &machine)
        :
        AntennaActiveStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
            
            std::vector<Cell> cells = this->_machine->_cells;
            for (auto cell : cells) {
                this->_machine->state_()._cells_active_vehicles[cell.cell_id] = 0;
                this->_machine->state_()._cells_transmitting_vehicles[cell.cell_id] = 0;
            }

        }

        bool no_event() const override { return true; }

    };

    template<class Types, class Parameters, class StateType>
    struct AntennaActiveTransitionNewActive : AntennaActiveStateMachine<Types, Parameters, StateType>::Transition_t {
        AntennaActiveTransitionNewActive(const std::shared_ptr<AntennaActiveStateMachine<Types, Parameters, StateType>> &machine)
        :
        AntennaActiveStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        std::string serving_cell_id;

        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
            value(serving_cell_id);

            this->_machine->state_()._cells_active_vehicles[serving_cell_id]++;
            this->_machine->state_()._active_cells_id.push_back(serving_cell_id);
        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::NEW_ANTENNA_ACTIVE;
        }

        typename AntennaActiveStateMachine<Types, Parameters, StateType>::Events
        output(const artis::traffic::core::Time & /* t */) const override {
            typename AntennaActiveStateMachine<Types, Parameters, StateType>::Events events{};
            std::vector<std::string> active_cells_id_copy(this->_machine->state_()._active_cells_id);
            this->_machine->state_()._active_cells_id.clear();
            events.externals.push_back(
                    typename AntennaActiveStateMachine<Types, Parameters,
                            StateType>::ExternalEvent{AntennaEventIDs::SEND_ANTENNA_ACTIVE, {active_cells_id_copy}});
            return events;
        }

    };

    template<class Types, class Parameters, class StateType>
    struct AntennaActiveTransitionNewInactive : AntennaActiveStateMachine<Types, Parameters, StateType>::Transition_t {
        AntennaActiveTransitionNewInactive(const std::shared_ptr<AntennaActiveStateMachine<Types, Parameters, StateType>> &machine)
                :
                AntennaActiveStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        std::string serving_cell_id;

        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
            value(serving_cell_id);

            this->_machine->state_()._cells_active_vehicles[serving_cell_id]--;
            this->_machine->state_()._inactive_cells_id.push_back(serving_cell_id);
        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::NEW_ANTENNA_INACTIVE;
        }

        typename AntennaActiveStateMachine<Types, Parameters, StateType>::Events
        output(const artis::traffic::core::Time & /* t */) const override {
            typename AntennaActiveStateMachine<Types, Parameters, StateType>::Events events{};
            std::vector<std::string> inactive_cells_id_copy(this->_machine->state_()._inactive_cells_id);
            this->_machine->state_()._inactive_cells_id.clear();
            events.externals.push_back(
                    typename AntennaActiveStateMachine<Types, Parameters,
                            StateType>::ExternalEvent{AntennaEventIDs::SEND_ANTENNA_INACTIVE, {inactive_cells_id_copy}});
            return events;
        }

    };

    template<class Types, class Parameters, class StateType>
    struct AntennaActiveTransitionLinkTransmitting : AntennaActiveStateMachine<Types, Parameters, StateType>::Transition_t {
        AntennaActiveTransitionLinkTransmitting(const std::shared_ptr<AntennaActiveStateMachine<Types, Parameters, StateType>> &machine)
                :
                AntennaActiveStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        std::string serving_cell_id;

        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
            value(serving_cell_id);

            this->_machine->state_()._cells_transmitting_vehicles[serving_cell_id]++;
        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::LINK_TRANSMITTING;
        }

    };

    template<class Types, class Parameters, class StateType>
    struct AntennaActiveTransitionLinkTransmissionOver : AntennaActiveStateMachine<Types, Parameters, StateType>::Transition_t {
        AntennaActiveTransitionLinkTransmissionOver(const std::shared_ptr<AntennaActiveStateMachine<Types, Parameters, StateType>> &machine)
                :
                AntennaActiveStateMachine<Types, Parameters, StateType>::Transition_t(machine) {}

        std::string serving_cell_id;

        void action(const artis::traffic::core::Time & t, const artis::common::event::Value &value) override {
            value(serving_cell_id);

            this->_machine->state_()._cells_transmitting_vehicles[serving_cell_id]--;
        }

        bool event(const artis::traffic::core::Time & /* t */, int event) override {
            return event == Types::event_IDs_type::LINK_NOT_TRANSMITTING;
        }

    };

    template<class Types, class Parameters, class StateType>
    void AntennaActiveStateMachine<Types, Parameters, StateType>::build(
            const std::shared_ptr<AntennaActiveStateMachine<Types, Parameters, StateType>> &machine) {
        this->state(new InitAntennaActiveState(machine));
        this->state(new WaitAntennaActiveState(machine));
        this->initial_state(AntennaStateIDs::INIT_ACTIVE);
        this->transition(AntennaStateIDs::INIT_ACTIVE, AntennaStateIDs::WAIT_ACTIVE,
        new AntennaActiveTransitionInit(machine));
        this->transition(AntennaStateIDs::WAIT_ACTIVE, AntennaStateIDs::WAIT_ACTIVE,
        new AntennaActiveTransitionNewActive(machine));
        this->transition(AntennaStateIDs::WAIT_ACTIVE, AntennaStateIDs::WAIT_ACTIVE,
        new AntennaActiveTransitionNewInactive(machine));
        this->transition(AntennaStateIDs::WAIT_ACTIVE, AntennaStateIDs::WAIT_ACTIVE,
                         new AntennaActiveTransitionLinkTransmitting(machine));
        this->transition(AntennaStateIDs::WAIT_ACTIVE, AntennaStateIDs::WAIT_ACTIVE,
                         new AntennaActiveTransitionLinkTransmissionOver(machine));
    }
}
#endif //ARTIS_TRAFFIC_MICRO_5G_ANTENNAACTIVESTATEMACHINE_HPP
