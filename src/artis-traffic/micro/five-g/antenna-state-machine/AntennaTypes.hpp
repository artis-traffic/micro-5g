/**
 * @file artis-traffic/micro/five-g/antenna-state-machine/AntennaTypes.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_FIVE_G_ANTENNA_TYPES_HPP
#define ARTIS_TRAFFIC_FIVE_G_ANTENNA_TYPES_HPP

#include <artis-star-addons/utils/StateMachine.hpp>

#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/micro/five-g/antenna-state-machine/AntennaIDs.hpp>

namespace artis::traffic::micro::five_g {

template<class Types, class Parameters>
class AntennaRootStateMachine;

template<class Vehicle, class VehicleEntry, class VehicleState, class Parameters>
struct AntennaTypes {
  using vehicle_type = Vehicle;
  using vehicle_entry_type = VehicleEntry;
  using vehicle_state_type = VehicleState;
  using state_machine_IDs_type = AntennaStateMachineIDs;
  using state_IDs_type = AntennaStateIDs;
  using event_IDs_type = AntennaEventIDs;
  using root_state_machine_type = AntennaRootStateMachine<AntennaTypes, Parameters>;
  using state_machine_type = artis::addons::utils::AbstractStateMachine<artis::common::DoubleTime, AntennaTypes>;
};

}

#endif //ARTIS_TRAFFIC_FIVE_G_ANTENNA_TYPES_HPP
