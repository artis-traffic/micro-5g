/**
 * @file artis-traffic/micro/five-g/relay.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_5G_RELAY_HPP
#define ARTIS_TRAFFIC_MICRO_5G_RELAY_HPP

#include <artis-star-addons/pdevs/StateMachineDynamics.hpp>
#include <artis-star/kernel/pdevs/Dynamics.hpp>
#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/micro/five-g/Data.hpp>
#include <vector>
#include <map>
#include <deque>
#include <sstream>
#include "Antenna.hpp"

#include <artis-traffic/micro/five-g/relay-state-machine/RelayIDs.hpp>
#include <artis-traffic/micro/five-g/relay-state-machine/RelayTypes.hpp>
#include <artis-traffic/micro/five-g/relay-state-machine/RelayRootStateMachine.hpp>
#include <artis-traffic/micro/five-g/relay-state-machine/RelayStateMachine.hpp>

namespace artis::traffic::micro::five_g {

    struct RelayParameters : AntennaParameters {
    };

    template<class Model, class Parameters, class Types>
    class RelayImplementation : public AntennaImplementation<Model, Parameters, Types> {
    public:
        struct inputs : AntennaImplementation<Model, Parameters, Types>::inputs {
            enum values {
                T = AntennaImplementation<Model, Parameters, Types>::inputs::MESSAGE + 50000000
            };
        };

        struct outputs : AntennaImplementation<Model, Parameters, Types>::outputs {
            enum values {
            };
        };

        struct vars : AntennaImplementation<Model, Parameters, Types>::vars {
            enum values {
            };
        };

        RelayImplementation(const std::string &name,
            const artis::pdevs::Context<artis::common::DoubleTime, Model, Parameters> &context) :
        AntennaImplementation<Model, Parameters, Types>(name, context){
//            this->input_port({inputs::MESSAGE, "message"});
//
//            this->output_port({outputs::MESSAGE, "message"});

        }

        virtual ~RelayImplementation() override = default;

        void dext(const artis::traffic::core::Time & t,
                  const artis::traffic::core::Time & e,
                  const artis::traffic::core::Bag & bag) override {
                    AntennaImplementation<Model, Parameters, Types>::dext(t, e, bag);
        }

        artis::traffic::core::Bag lambda(const artis::traffic::core::Time & t) const override {
            artis::traffic::core::Bag bag = AntennaImplementation<Model, Parameters, Types>::lambda(t);
            return bag;
        }

        artis::common::event::Value observe(const artis::traffic::core::Time &t, unsigned int index) const override {
            artis::common::event::Value value = AntennaImplementation<Model, Parameters, Types>::observe(t, index);

            if (value.is_null()) {
                switch (index) {
                    default:
                        return {};
                }
            }
            return value;
        }

    private:

        double _timeout_duration = 20;
        std::vector<Coordinates> _coordinates;
        uint _link_number;
        std::vector<Cell> _cells;


        // state
        std::map<uint, std::pair<artis::traffic::core::Time, Coordinates> > _vehicles;
        std::vector<MeasureReportMessage> _measure_messages;
        std::deque<std::pair<MeasureReportMessage, uint> > _send_measure_messages;
    };

    template<class Types, class Parameters>
    class Relay : public RelayImplementation<Relay<Types, Parameters>, Parameters, Types> {
    public:
        Relay(const std::string &name,
                const artis::pdevs::Context<artis::common::DoubleTime, Relay<Types, Parameters>, RelayParameters> &context)
                : RelayImplementation<Relay<Types, Parameters>, RelayParameters, Types>(name, context) {
        }

        ~Relay() override = default;
    };
}

#endif //ARTIS_TRAFFIC_MICRO_5G_RELAY_HPP
